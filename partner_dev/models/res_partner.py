from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class ResPartner(models.Model):
    _inherit = 'res.partner'

    competitor = fields.Boolean(string='Competitor')
    competitor_material_pricelist_ids = fields.One2many('competitor.material.pricelist', 'partner_id', string='Competitor Pricelist')
    competitor_project_cost_ids = fields.One2many('competitor.project.cost', 'partner_id', string='Project Cost')
    
    @api.model
    def create(self, vals):
        if 'customer' in vals :
            if vals['customer'] and ('search_default_customer' in self._context or 'search_default_supplier' in self._context or 'search_default_competitor' in self._context):
                raise Warning(_("Can not create customer data in this apps."))
        return super(ResPartner, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'customer' in vals :
            if vals['customer'] and ('search_default_customer' in self._context or 'search_default_supplier' in self._context or 'search_default_competitor' in self._context):
                raise Warning(_("Can not change other partner to customer data in this apps."))
        for partner in self :
            if partner.customer and ('search_default_customer' in self._context or 'search_default_supplier' in self._context or 'search_default_competitor' in self._context):
                raise Warning(_("Can not update customer data in this apps."))
        return super(ResPartner, self).write(vals)

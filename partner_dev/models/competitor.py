from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class CompetitorMaterialPricelist(models.Model):
    _name = "competitor.material.pricelist"
    _description = "Material Pricelist of Competitor"
    _rec_name = 'product_id'

    product_id = fields.Many2one('product.product', string='Material', required=True)
    partner_id = fields.Many2one('res.partner', string='Competitor', ondelte='cascade')
    vendor_id = fields.Many2one('res.partner', string='Vendor')
    price = fields.Float(string='Price', digits=dp.get_precision('Price Unit'), required=True)

class CompetitorProjectCost(models.Model):
    _name = "competitor.project.cost"
    _description = "Project Cost of Competitor"
    
    name = fields.Char(string='Project', required=True)
    partner_id = fields.Many2one('res.partner', string='Competitor', ondelte='cascade')
    cost = fields.Float(string='Cost', digits=dp.get_precision('Price Unit'), required=True)

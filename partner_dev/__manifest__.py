# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Partner',
    'version'       : '0.1',
    'summary'       : 'Custom Partner form for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- Competitor information
    """,
    'depends'       : [
        'base',
        'product',
    ],
    'data': [
        'views/res_partner_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
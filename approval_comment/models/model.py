from odoo import api, models, _


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_approve_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_approve',
                'action_comment': 'Approve'
            }
        }

    @api.multi
    def action_approve_manager_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_approve',
                'action_comment': 'Manager Approve'
            }
        }

    @api.multi
    def action_reject_with_comment(self):
        return {
            'name': _('Reject with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_reject',
                'action_comment': 'Reject'
            }
        }

    @api.multi
    def action_reject_manager_with_comment(self):
        return {
            'name': _('Reject with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_reject',
                'action_comment': 'Manager Reject'
            }
        }


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    def button_approve_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'button_to_approve',
                'action_comment': 'Approve'
            }
        }

    @api.multi
    def button_co_approve_with_comment(self):
        return {
            'name': _('Co-approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'button_co_approve',
                'action_comment': 'Co-approve'
            }
        }


class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    @api.multi
    def button_approved_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'button_approved',
                'action_comment': 'Approve'
            }
        }

    @api.multi
    def button_approved_manager_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'button_approved',
                'action_comment': 'Manager Approve'
            }
        }

    @api.multi
    def button_rejected_with_comment(self):
        return {
            'name': _('Reject with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'button_rejected',
                'action_comment': 'Reject'
            }
        }

    @api.multi
    def button_rejected_cancel_with_comment(self):
        return {
            'name': _('Cancel with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'button_rejected',
                'action_comment': 'Cancel'
            }
        }


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def action_approve_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_approve',
                'action_comment': 'Approve'
            }
        }

    @api.multi
    def action_reject_with_comment(self):
        return {
            'name': _('Reject with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_reject',
                'action_comment': 'Reject'
            }
        }


class AdvanceRequest(models.Model):
    _name = 'advance.request'
    _inherit = ['advance.request', 'mail.thread', 'mail.activity.mixin']

    @api.multi
    def action_reject_with_comment(self):
        return {
            'name': _('Reject with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_reject',
                'action_comment': 'Reject'
            }
        }

    @api.multi
    def action_approve_manager_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_approve_manager',
                'action_comment': 'Manager Approve'
            }
        }

    @api.multi
    def action_approve_finance_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'action_approve_finance',
                'action_comment': 'Finance Approve'
            }
        }


class HrExpenseSheet(models.Model):
    _inherit = 'hr.expense.sheet'

    @api.multi
    def approve_expense_sheets_with_comment(self):
        return {
            'name': _('Approve with Comment'),
            'target': 'new',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'wizard.approval.comment',
            'context': {
                'method': 'approve_expense_sheets',
                'action_comment': 'Approve'
            }
        }

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Comment on Every Approval & Rejection',
    'version': '0.1',
    'summary': 'Comment on Every Approval & Rejection',
    'author': 'Arkana',
    'description': """
Features :
1. Quotation sebelum send ke client
2. Purchase Request setelah submit
3. RFQ ke PO
4. Payment
5. Advance Request
6. Expense yg manager approval
""",
    'depends': [
        'account_dev',
        'crm_dev',
        'purchase_request',
        'osi_purchase_workflow_customization',
        'advance_request',
        'hr_expense',
    ],
    'data': [
        'views/view_model.xml',
        'wizard/view_wizard_approval_comment.xml'
    ],
    'demo': [],
    'installable': True
}

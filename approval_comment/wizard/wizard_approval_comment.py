from odoo import fields, api, models
from bs4 import BeautifulSoup


class WizardApprovalComment(models.TransientModel):
    _name = 'wizard.approval.comment'
    _description = 'Approval Comment Wizard'

    comment = fields.Text('Comment', required=True)

    @api.multi
    def action_confirm(self):
        recs = self.env[self._context.get('active_model')].browse(
            self._context.get('active_ids'))
        getattr(recs, self._context.get('method'))()

        soup = BeautifulSoup(self.comment)
        recs.message_post(body="""
        <p>
        <b>Action</b> : %s
        </p>
        <p>
        <b>Comment</b> : %s
        </p>
        """ % (self._context.get('action_comment'), soup.get_text('\n')))

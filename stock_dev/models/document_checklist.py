from odoo import fields, api, models, _
from odoo.exceptions import Warning

class DocumentChecklist(models.Model):
    _inherit = 'document.checklist'

    picking_id = fields.Many2one('stock.picking')

    @api.model
    def create(self, vals):
        if 'picking_id' in vals :
            picking_id = self.env['stock.picking'].browse(vals['picking_id'])
            vals['name'] = picking_id.name
        res = super(DocumentChecklist, self).create(vals)
        return res

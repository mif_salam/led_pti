from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    @api.depends('location_id.usage','location_dest_id.usage')
    def _is_do(self):
        for picking in self :
            picking.is_do = picking.location_id.usage == 'internal' and picking.location_dest_id.usage == 'customer'

    tracking_history_line = fields.One2many('tracking.history', 'picking_id', string='Delivery History')
    qc_date = fields.Datetime(string='QC Date', copy=False)
    qc_uid = fields.Many2one('res.users', string='QC by', copy=False)
    is_do = fields.Boolean(string='Is DO', compute='_is_do', store=True)
    delivery_status = fields.Selection([
        ('waiting_confirmation','Waiting Confirmation'),
        ('ready','Item Ready'),
        ('waiting_carrier','Waiting for Carrier'),
        ('delivery','Delivery Process'),
        ('waiting_customer','Waiting Customer'),
        ('complete','Completed'),
    ], string='Delivery Order Status', copy=False, default='waiting_confirmation')
    doc_ids = fields.One2many('document.checklist', 'picking_id', string='Document Checklist')
    carrier_price = fields.Monetary(string="Shipping Cost")
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id)

    @api.model
    def create(self, vals):
        picking = super(StockPicking, self).create(vals)
        if not picking.is_do :
            picking.write({'delivery_status':'waiting_carrier'})
        return picking

    @api.multi
    def send_to_shipper(self):
        self.ensure_one()
        res = self.carrier_id.send_shipping(self)[0]
        if self.carrier_id.free_over and self.sale_id and self.sale_id._compute_amount_total_without_delivery() >= self.carrier_id.amount:
            res['exact_price'] = 0.0
        #START CUSTOM
        if not self.carrier_price :
            self.carrier_price = res['exact_price']
        #END OF CUSTOM
        if res['tracking_number']:
            self.carrier_tracking_ref = res['tracking_number']
        order_currency = self.sale_id.currency_id or self.company_id.currency_id
        msg = _("Shipment sent to carrier %s for shipping with tracking number %s<br/>Cost: %.2f %s") % (self.carrier_id.name, self.carrier_tracking_ref, self.carrier_price, order_currency.name)
        self.message_post(body=msg)

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        for picking in self :
            if picking.is_do :
                picking.write({
                    'delivery_status': 'delivery'
                })
            else :
                picking.write({
                    'delivery_status': 'complete'
                })
        return res

    @api.multi
    def qc_passed(self):
        for picking in self :
            if picking.is_do :
                picking.write({
                    'delivery_status':'ready',
                })
        self.write({
            'qc_date': fields.Datetime.now(),
            'qc_uid': self._uid,
        })

    @api.multi
    def action_waiting_carrier(self):
        for picking in self :
            if picking.is_do :
                picking.write({
                    'delivery_status':'waiting_carrier',
                })

    @api.multi
    def action_waiting_customer(self):
        for picking in self :
            if picking.is_do :
                picking.write({
                    'delivery_status':'waiting_customer',
                })

    @api.multi
    def action_complete(self):
        for picking in self :
            if picking.is_do :
                picking.write({
                    'delivery_status':'complete',
                })

class TrackingHistory(models.Model):
    _name = "tracking.history"
    _description = "Delivery History"
    _order = "date"

    picking_id = fields.Many2one('stock.picking', string="Picking", ondelete='cascade')
    date = fields.Date(string='Date', required=True, default=fields.Date.context_today)
    note = fields.Text(string='Description', required=True)

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    @api.depends('picking_id.carrier_price','picking_id.weight')
    def _get_carrier_price(self):
        for move in self :
            carrier_price = 0
            if move.picking_id.carrier_price and move.picking_id.weight :
                unit_price = move.picking_id.carrier_price / move.picking_id.weight
                carrier_price = move.weight * unit_price
            move.carrier_price = carrier_price

    carrier_price = fields.Monetary(string="Shipping Cost", compute='_get_carrier_price', store=True)
    currency_id = fields.Many2one('res.currency', 'Currency', related='picking_id.currency_id', store=True)

    @api.multi
    def view_picking(self):
        action = self.env.ref('stock.action_picking_tree_all')
        action = action.read()[0]
        view_id = self.env.ref('stock.view_picking_form')
        action.update({
            'res_id': self.picking_id.id,
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
        })
        return action

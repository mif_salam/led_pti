# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Inventory',
    'version'       : '0.1',
    'summary'       : 'Custom Inventory Module for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- Tracking delivery
- Quality control
    """,
    'depends'       : [
        'base',
        'stock',
        'delivery',
        'document_checklist',
    ],
    'data': [
        'views/stock_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
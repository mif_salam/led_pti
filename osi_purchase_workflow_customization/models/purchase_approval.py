# Copyright (C) 2018 - TODAY, Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models


class PurchaseApproval(models.Model):
    _name = 'purchase.approval'
    _order = 'approval_amount desc'
    _description = 'Purchase Order Approval'
    _rec_name = "employee_id"

    @api.depends('employee_id.company_id.currency_id.rate','currency_id.rate','approval_amount','co_approval_amount')
    @api.multi
    def _get_base_amount(self):
        for approval in self :
            if approval.employee_id.company_id.currency_id != approval.currency_id :
                base_approval_amount = approval.currency_id.compute(approval.approval_amount, approval.employee_id.company_id.currency_id)
                base_co_approval_amount = approval.currency_id.compute(approval.co_approval_amount, approval.employee_id.company_id.currency_id)
            else :
                base_approval_amount = approval.approval_amount
                base_co_approval_amount = approval.co_approval_amount
            approval.base_approval_amount = base_approval_amount
            approval.base_co_approval_amount = base_co_approval_amount

    employee_id = fields.Many2one(
        comodel_name='hr.employee',
        string='Employee',
    )
    job_id = fields.Many2one(comodel_name='hr.job', string='Role')
    approval_amount = fields.Float(string='Approval amount')
    co_approval_amount = fields.Float(string='Co-Approval amount')
    user_id = fields.Many2one(
        related='employee_id.user_id',
        string='Related User'
    )
    po_type = fields.Selection(
        selection=[('inventory', 'Inventory'),
                   ('non-inventory', 'Non Inventory')],
        string='PO Type',
        default='inventory',
    )
    currency_id = fields.Many2one('res.currency', 'Currency', ondelete='restrict', required=True, default=lambda self: self.env.user.company_id.currency_id.id)
    base_approval_amount = fields.Float(string='Base Approval amount', compute='_get_base_amount', store=True)
    base_co_approval_amount = fields.Float(string='Base Co-Approval amount', compute='_get_base_amount', store=True)

    @api.onchange('employee_id')
    def onchange_employee_id(self):
        if self.employee_id:
            self.job_id = self.employee_id.job_id.id

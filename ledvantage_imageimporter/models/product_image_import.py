# -*- coding: utf-8 -*-
import math
import re
import base64
import os.path
from datetime import datetime
from odoo import api, models, exceptions

# product.category
# Edit product category tambah code
# Edit product template tambah supplier info

class ProductImageImporter(models.Model):
    _inherit = "product.template"

    @api.model
    def create(self, vals):
        res = super(ProductImageImporter, self).create(vals)
        return on_saveProduct(self, res)

def on_saveProduct(self,  res):
    _pasang_gambar(res)
    return res

def _pasang_gambar(data):
        pathnya = data.env['ir.config_parameter'].get_param( "productimagepath")
        strcatcode = data.categ_id.x_code
        file_path = pathnya + strcatcode + "/" +  data.default_code + ".JPG"
        ada = os.path.exists(file_path)
        if ada :
            file = open(file_path, "rb")
            out = file.read()
            file.close()
            data.image_medium = base64.b64encode(out)
        else:
            file_path = pathnya + strcatcode + "/" +  data.default_code + ".jpg"
            ada2 = os.path.exists(file_path)
            if ada2 :
                file = open(file_path, "rb")
                out = file.read()
                file.close()
                data.image_medium = base64.b64encode(out)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
LIPS© - LedVantage - Image Importer
=============================

LedVantage - Image Importer

Depends
=======
[stock] addon Odoo

Tech
====
* [Python] - Models
* [XML] - Odoo views

Installation
============
- http://ledvantage.id/
- Install our custom addon

License
=======
GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (LGPLv3)
(http://www.gnu.org/licenses/agpl.html)

Bug Tracker
===========

Contact odoo@ledvantage.id

Authors
-------
* Guritno Dimaz

Maintainer
----------

This module is maintained by LedVantage.

For support and more information, please visit http://ledvantage.id/.
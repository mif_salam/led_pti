# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Project',
    'version'       : '0.1',
    'summary'       : 'Custom Project Module for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- Show project expense
    """,
    'depends'       : [
        'hr_expense',
        'crm_dev',
        'abs_project_so',
        'abs_project_po',
        'advance_request',
        'document_checklist',
        'stock_dev',
        'purchase_request',
        'sale_dev',
    ],
    'data': [
        'views/project_project_view.xml',
        'views/hr_expense_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
from odoo import fields, api, models, _
from odoo.exceptions import Warning

class PurchaseRequest(models.Model):
    _inherit = 'purchase.request'

    @api.multi
    def button_to_approve(self):
        res = super(PurchaseRequest, self).button_to_approve()
        for req in self :
            if req.sale_id.projects_id.state == 'start' :
                req.sale_id.projects_id.write({'state':'purchase_request'})
        return res

class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        res = super(MailComposer, self).send_mail_action()
        if self.model == 'purchase.order' :
            order_id = self.env[self.model].browse(self.res_id)
            if order_id.project_id.state == 'purchase_request' :
                order_id.project_id.write({'state':'purchase_order'})
        return res

class PurchasePickingWizard(models.TransientModel):
    _inherit = 'purchase.picking.wizard'

    @api.multi
    def action_create(self):
        res = super(PurchasePickingWizard, self).action_create()
        purchase_line_ids = self.env[self._context['active_model']].browse(self._context['active_ids'])
        purchase_ids = purchase_line_ids.mapped('order_id')
        for purchase in purchase_ids :
            if purchase.project_id.state == 'purchase_order' :
                purchase.project_id.write({'state':'waiting_product'})
        return res

class SalePickingWizard(models.TransientModel):
    _inherit = 'sale.picking.wizard'

    @api.multi
    def action_create(self):
        res = super(SalePickingWizard, self).action_create()
        sale_line_ids = self.env[self._context['active_model']].browse(self._context['active_ids'])
        sale_ids = sale_line_ids.mapped('order_id')
        for sale in sale_ids :
            if sale.projects_id.state == 'waiting_delivery' :
                sale.projects_id.write({'state':'delivery'})
        return res

class StockMove(models.Model):
    _inherit = 'stock.move'

    @api.multi
    def _action_done(self):
        res = super(StockMove, self)._action_done()
        for move in self :
            if move.purchase_line_id.order_id.project_id.state == 'waiting_product' :
                if all(line.product_qty <= line.qty_received for line in move.purchase_line_id.order_id.order_line):
                    move.purchase_line_id.order_id.project_id.write({'state':'waiting_delivery'})
        return res

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def action_waiting_customer(self):
        res = super(StockPicking, self).action_waiting_customer()
        for picking in self :
            sale_ids = picking.mapped('move_lines.sale_line_id.order_id')
            for sale in sale_ids :
                if sale.projects_id.state == 'delivery' :
                    sale.projects_id.write({'state':'waiting_acceptance'})
        return res

    @api.multi
    def action_complete(self):
        res = super(StockPicking, self).action_complete()
        for picking in self :
            sale_ids = picking.mapped('move_lines.sale_line_id.order_id')
            for sale in sale_ids :
                if sale.projects_id.state == 'waiting_acceptance' :
                    sale.projects_id.write({'state':'waiting_invoice'})
        return res

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        for invoice in self :
            sale_ids = invoice.mapped('invoice_line_ids.sale_line_ids.order_id')
            for sale in sale_ids :
                if sale.projects_id.state == 'waiting_invoice' :
                    sale.projects_id.write({'state':'waiting_payment'})
        return res

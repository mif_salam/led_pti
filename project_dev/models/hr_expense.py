from odoo import fields, api, models, _
from odoo.exceptions import Warning

class HrExpense(models.Model):
    _inherit = 'hr.expense'

    project_id = fields.Many2one('project.project', string='Project')
    
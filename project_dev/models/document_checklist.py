from odoo import fields, api, models, _
from odoo.exceptions import Warning

class DocumentChecklist(models.Model):
    _inherit = 'document.checklist'

    project_id = fields.Many2one('project.project')

    @api.model
    def create(self, vals):
        if 'project_id' in vals :
            project_id = self.env['project.project'].browse(vals['project_id'])
            vals['name'] = project_id.name
        res = super(DocumentChecklist, self).create(vals)
        return res

from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class ProjectProject(models.Model):
    _inherit = 'project.project'

    @api.multi
    @api.depends('sale_order_ids.amount_total','purchase_order_ids.amount_total','expense_ids.amount','currency_id')
    def _get_total_revenue(self):
        for project in self :
            total_revenue = 0
            for sale in project.sale_order_ids :
                total_revenue += sale.currency_id.compute(sale.amount_total, project.currency_id)
            for purchase in project.purchase_order_ids :
                total_revenue -= purchase.currency_id.compute(purchase.amount_total, project.currency_id)
            for expense in project.expense_ids :
                total_revenue -= expense.currency_id.compute(expense.amount, project.currency_id)
            project.total_revenue = total_revenue

    @api.multi
    def _get_move(self):
        for project in self :
            purchase_move_ids = sale_move_ids = self.env['stock.move']
            purchase_line_ids = project.purchase_order_ids.mapped('order_line')
            if purchase_line_ids :
                purchase_move_ids = self.env['stock.move'].search([('purchase_line_id','in',purchase_line_ids.ids)])
            sale_line_ids = project.sale_order_ids.mapped('order_line')
            if sale_line_ids :
                sale_move_ids = self.env['stock.move'].search([('sale_line_id','in',sale_line_ids.ids)])
            project.purchase_move_ids = purchase_move_ids
            project.sale_move_ids = sale_move_ids

    expense_ids = fields.One2many('project.expense', 'project_id', string='Expenses')
    total_revenue = fields.Monetary(string='Total Revenue', compute='_get_total_revenue', digits=dp.get_precision('Price Unit'))
    purchase_move_ids = fields.One2many('stock.move', string='Receiving', compute='_get_move')
    sale_move_ids = fields.One2many('stock.move', string='Delivery Order', compute='_get_move')
    doc_ids = fields.One2many('document.checklist', 'project_id', string='Document Checklist')
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id)
    state = fields.Selection([
        ('start','Project Started'),
        ('purchase_request','Purchase Request Process'),
        ('purchase_order','Purchase Order Process'),
        ('waiting_product','Waiting for Product'),
        ('waiting_delivery','Waiting for Delivery'),
        ('delivery','On Delivery'),
        ('waiting_acceptance','Waiting for Customer Acceptance'),
        ('waiting_invoice','Waiting for Customer Invoice'),
        ('waiting_payment','Waiting for Payment'),
        ('close','Closed'),
        ('cancel','Cancelled'),
    ], string='State', default='start', copy=False)

    @api.model
    def create(self, vals):
        res = super(ProjectProject, self).create(vals)
        # if res.expense_ids and res.advance_request_ids :
        #     raise Warning("Expense dan Advance Request double.")
        return res

    @api.multi
    def button_close(self):
        for project in self :
            warning_message = ''
            for purchase in project.purchase_order_ids :
                if purchase.state != 'done' or any(line.product_qty < line.qty_received or line.product_qty < line.qty_invoiced for line in purchase.order_line):
                    warning_message += "\n - purchase order %s belum selesai"%(purchase.name)
            for sale in project.sale_order_ids :
                if sale.state != 'done' or any(line.product_uom_qty < line.qty_delivered or line.product_uom_qty < line.qty_invoiced for line in sale.order_line):
                    warning_message += "\n - sale order %s belum selesai"%(sale.name)
                for picking in sale.picking_ids.filtered(lambda pick:pick.state != 'cancel'):
                    if picking.delivery_status != 'complete' :
                        warning_message += "\n - proses pengiriman DO %s belum selesai"%(picking.name)
                for inv in sale.invoice_ids.filtered(lambda inv:inv.state != 'cancel'):
                    if inv.state != 'paid' :
                        warning_message += "\n - invoice %s belum selesai"%(inv.number)
            advance_request_ids = self.env['advance.request'].search([
                ('project_id','=',project.id),
                ('state','not in',['done','reject','cancel']),
            ])
            for advance_request_id in advance_request_ids :
                warning_message += "\n - advance request %s belum selesai"%(advance_request_id.name)
            expense_ids = self.env['hr.expense'].search([
                ('project_id','=',project.id),
                ('state','not in',['done','refused']),
            ])
            for expense_id in expense_ids :
                warning_message += "\n - expense %s belum selesai"%(expense_id.name)
            if warning_message :
                raise Warning("Tidak bisa close project dikarnakan:\n%s"%(warning_message))
            project.write({'state':'close'})

    @api.multi
    def write(self, vals):
        res = super(ProjectProject, self).write(vals)
        # for project in self :
        #     if project.expense_ids and project.advance_request_ids :
        #         raise Warning("Expense/Advance Request double atau sudah diinput sebelumnya.")
        return res

    @api.multi
    def unlink(self):
        for project in self :
            if project.sale_order_ids :
                raise Warning(_("Invalid action. You can not delete project."))
        return super(ProjectProject, self).unlink()

    def view_project_form(self):
        self.ensure_one()
        action = self.env.ref('project.open_view_project_all_config', False)
        # action = self.env.ref('project.act_project_project_2_project_task_all', False)
        action = action.read()[0]
        view_id = self.env.ref('project.edit_project')
        action['view_id'] = view_id.id
        action['views'] = [(view_id.id, 'form')]
        action['res_id'] = self.id
        return action

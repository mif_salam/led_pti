# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools
from odoo.addons import decimal_precision as dp

class ProjectExpense(models.Model):
    _name = "project.expense"
    _description = "Project Expense"
    _auto = False

    name = fields.Char(string='Reference')
    amount = fields.Monetary(string='Amount', digits=dp.get_precision('Stock Weight'))
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id)
    project_id = fields.Many2one('project.project', 'Project')
    product_id = fields.Many2one('product.product', 'Product')

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute(""" CREATE VIEW %s AS (
            SELECT 
                row_number() over (order by name) as id,
                product_id,
                name,
                amount,
                currency_id,
                project_id
            FROM (
                SELECT
                    ex.product_id as product_id,
                    concat(ex.name,' ',ex.reference) as name,
                    ex.total_amount as amount,
                    ex.currency_id as currency_id,
                    ex.project_id as project_id
                FROM
                    hr_expense as ex
                WHERE
                    ex.state in ('reported','done') and ex.project_id is not null

                UNION ALL

                SELECT
                    arl.product_id as product_id,
                    concat(ar.name,' ',ar.ref) as name,
                    sum(arl.price_subtotal) as amount,
                    ar.currency_id as currency_id,
                    ar.project_id as project_id
                FROM
                    advance_request_realization as arl
                LEFT JOIN
                    advance_request ar on ar.id = arl.advance_request_id
                WHERE
                    ar.state in ('approve_finance','done') and ar.project_id is not null
                GROUP BY
                    arl.product_id,
                    concat(ar.name,' ',ar.ref),
                    ar.currency_id,
                    ar.project_id

                UNION ALL

                SELECT
                    sm.product_id as product_id,
                    concat(sp.name,' ',so.name) as name,
                    sum(sm.carrier_price) as amount,
                    sm.currency_id as currency_id,
                    so.projects_id as project_id
                FROM
                    stock_move as sm
                LEFT JOIN
                    stock_picking sp on sp.id = sm.picking_id
                LEFT JOIN
                    sale_order_line sol on sol.id = sm.sale_line_id
                LEFT JOIN
                    sale_order so on so.id = sol.order_id
                WHERE
                    sm.state in ('confirmed','assigned','done') and so.projects_id is not null
                GROUP BY
                    sm.product_id,
                    concat(sp.name,' ',so.name),
                    sm.currency_id,
                    so.projects_id

                UNION ALL

                SELECT
                    sm.product_id as product_id,
                    concat(sp.name,' ',po.name) as name,
                    sum(sm.carrier_price) as amount,
                    sm.currency_id as currency_id,
                    po.project_id as project_id
                FROM
                    stock_move as sm
                LEFT JOIN
                    stock_picking sp on sp.id = sm.picking_id
                LEFT JOIN
                    purchase_order_line pol on pol.id = sm.purchase_line_id
                LEFT JOIN
                    purchase_order po on po.id = pol.order_id
                WHERE
                    sm.state in ('confirmed','assigned','done') and po.project_id is not null
                GROUP BY
                    sm.product_id,
                    concat(sp.name,' ',po.name),
                    sm.currency_id,
                    po.project_id
            ) as expense
        )"""%(self._table))

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from odoo.addons import decimal_precision as dp
from odoo import fields, api, models, _
import time
import pytz
from pytz import timezone
import xlsxwriter
import base64
from io import BytesIO

SO_STATE = {
    'draft': 'Draft',
    'waiting': 'Waiting Approval',
    'approved_manager': 'Approved by Manager',
    'approved': 'Approved',
    'sent': 'Sent to Customer',
    'accepted': 'Accepted by Customer',
    'sale': 'Waiting for Delivery',
    'delivery': 'Delivery Process',
    'waiting_invoice': 'Waiting Invoice',
    'waiting_payment': 'Waiting Payment',
    'done': 'Done',
    'cancel': 'Cancel / Rejected / Failed',
    None: '',
    False: '',
    '':'',
}

PO_STATE = {
    'purchase_rfq': 'Purchase Request',
    'draft': 'Draft Order/RFQ',
    'sent': 'Submitted to Supplier',
    'to approve': 'Purchase Approve',
    'co approve': 'Purchase co-approval',
    'purchase': 'Purchase Order',
    'done': 'Done',
    'cancel': 'Cancelled',
    None: '',
    False: '',
    '':'',
}

class ReportOrderWizard(models.TransientModel):
    _name = "report.order.wizard"
    _description = "Report Order Wizard"

    @api.model
    def _get_startdate(self):
        return time.strftime('%Y-%m-1')

    @api.model
    def _get_enddate(self):
        next_month = datetime.now().date().replace(day=28) + timedelta(days=4)
        return next_month - timedelta(days=next_month.day)

    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))

    name = fields.Char(string='Name', default='Report Order')
    data = fields.Binary('File', readonly=True)
    date_start = fields.Date('Date Start', default=_get_startdate)
    date_end = fields.Date('Date End', default=_get_enddate)
    type = fields.Selection([
        ('t0','Status of Customer Inquiry'),
        ('t1','Status of PTI Quotation'),
        ('t1_5','Sales Order Status'),
        ('t2','Purchase Order to Principal'),
        ('t3','PO Status'),
    ], string='Report Type', required=True, default='t0')

    @api.multi
    def action_print_pdf(self):
        if self.type == 't0' :
            pdf_report = 'report_order.action_order_report_t0_pdf'
        elif self.type == 't1' :
            pdf_report = 'report_order.action_order_report_t1_pdf'
        elif self.type == 't1_5' :
            pdf_report = 'report_order.action_order_report_t1_5_pdf'
        elif self.type == 't2' :
            pdf_report = 'report_order.action_order_report_t2_pdf'
        else :
            pdf_report = 'report_order.action_order_report_t3_pdf'
        return self.env.ref(pdf_report).report_action(self)

    @api.multi
    def action_print_excel(self):
        if self.type == 't0' :
            datas = self.get_t0()
            return self.print_excel_report_t0(datas)
        elif self.type == 't1' :
            datas = self.get_t1()
            return self.print_excel_report_t1(datas)
        elif self.type == 't1_5' :
            datas = self.get_t1_5()
            return self.print_excel_report_t1_5(datas)
        elif self.type == 't2' :
            datas = self.get_t2()
            return self.print_excel_report_t2(datas)
        else :
            datas = self.get_t3()
            return self.print_excel_report_t3(datas)

    def get_t0(self):
        query_where = " 1=1 "
        if self.date_start :
            date_start = datetime.strptime(self.date_start, '%Y-%m-%d') - timedelta(days=1)
            date_start = date_start.strftime('%Y-%m-%d 17:00:00')
            query_where += " AND cl.create_date >= '%s'"%(date_start)
        if self.date_end :
            date_end = self.date_end + ' 16:59:59'
            query_where += " AND cl.create_date <= '%s' "%(date_end)

        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        self._cr.execute("""
            SELECT
                row_number() OVER() as num,
                cl.sequence_name as file_number,
                rp.code as customer_code,
                rp.display_name as customer_name,
                cl.customer_inq_ref as customer_inquiry,
                coalesce(cl.planned_revenue, 0) as planned_revenue,
                cl.inquiry_delivery_date inquiry_date,
                cl.create_date + interval '%s' as create_date,
                t.name as team_name,
                method.name as incoterm,
                l.name as location,
                (SELECT 
                    id
                FROM 
                    sale_order
                WHERE
                    opportunity_id = cl.id and state != 'cancel'
                LIMIT 1) as order_id,
                (SELECT 
                    id
                FROM 
                    crm_lead_line
                WHERE
                    lead_id = cl.id
                ORDER BY
                    planned_revenue desc
                LIMIT 1) as lead_line_id
            FROM
                crm_lead cl
            LEFT JOIN
                crm_team t on t.id=cl.team_id
            LEFT JOIN
                res_partner rp ON rp.id=cl.partner_id
            LEFT JOIN
                transaction_method method ON method.id=cl.transaction_method_id
            LEFT JOIN
                delivery_location l ON l.id=cl.delivery_location_id
            WHERE
                %s
        """%(tz,query_where))
        result = self._cr.dictfetchall()
        for res in result :
            if res.get('order_id', False):
                sale_id = self.env['sale.order'].browse(res['order_id'])
                res['quotation_number'] = sale_id.quotation_number
                res['quotation_date'] = (datetime.strptime(sale_id.date_order, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S')
                if sale_id.pricelist_id.currency_id != self.env.user.company_id.currency_id :
                    res['sale_amount'] = sale_id.pricelist_id.currency_id.compute(sale_id.amount_total, self.env.user.company_id.currency_id)
                else :
                    res['sale_amount'] = sale_id.amount_total
            else :
                res['quotation_number'] = ''
                res['quotation_date'] = ''
                res['sale_amount'] = 0
                res['confirmation_date'] = ''
            if res.get('lead_line_id', False):
                lead_line_id = self.env['crm.lead.line'].browse(res['lead_line_id'])
                res['prod_categ'] = lead_line_id.product_id.categ_id.name
            else :
                res['prod_categ'] = ''
        return result

    def get_t1(self):
        query_where = " 1=1 "
        if self.date_start :
            date_start = datetime.strptime(self.date_start, '%Y-%m-%d') - timedelta(days=1)
            date_start = date_start.strftime('%Y-%m-%d 17:00:00')
            query_where += " AND cl.create_date >= '%s'"%(date_start)
        if self.date_end :
            date_end = self.date_end + ' 16:59:59'
            query_where += " AND cl.create_date <= '%s' "%(date_end)

        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        self._cr.execute("""
            SELECT
                row_number() OVER() as num,
                cl.sequence_name as file_number,
                rp.display_name as customer_name,
                rp.code as customer_code,
                cl.id as opportunity_id
            FROM
                crm_lead cl
            LEFT JOIN
                res_partner rp ON rp.id=cl.partner_id
            WHERE
                %s
        """%(query_where))
        datas = self._cr.dictfetchall()
        final_datas = []
        for data in datas :
            opportunity_id = self.env['crm.lead'].browse(data['opportunity_id'])
            sale_ids = opportunity_id.order_ids.filtered(lambda o: o.state != 'cancel')
            if sale_ids :
                new_data = data
                sale_id = sale_ids[:1]
                new_data['quotation_date'] = sale_id.date_order and (datetime.strptime(sale_id.date_order, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S') or ''
                new_data['cust_order'] = sale_id.origin or ''
                new_data['sales_order'] = sale_id.name or ''
                new_data['quotation_number'] = sale_id.quotation_number or ''
                new_data['client_order_ref'] = sale_id.client_order_ref or ''
                new_data['client_order_date_ref'] = sale_id.client_order_date_ref or ''
                new_data['sales_order_date'] = sale_id.confirmation_date and (datetime.strptime(sale_id.confirmation_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S') or ''
                new_data['so_value_usd'] = sale_id.amount_total or 0
                new_data['incoterm'] = sale_id.opportunity_id.transaction_method_id.name or ''
                new_data['location'] = sale_id.opportunity_id.delivery_location_id.name or ''
                new_data['team_name'] = sale_id.opportunity_id.team_id.name or ''
                lead_line_id = self.env['crm.lead.line'].search([
                    ('lead_id','=',sale_id.opportunity_id.id),
                ], order='planned_revenue desc', limit=1)
                if lead_line_id :
                    new_data['prod_categ'] = lead_line_id.product_id.categ_id.name
                else :
                    new_data['prod_categ'] = ''
                final_datas.append(new_data)
        return final_datas

    def get_t2(self):
        query_where = " 1=1 "
        if self.date_start :
            date_start = datetime.strptime(self.date_start, '%Y-%m-%d') - timedelta(days=1)
            date_start = date_start.strftime('%Y-%m-%d 17:00:00')
            query_where += " AND cl.create_date >= '%s'"%(date_start)
        if self.date_end :
            date_end = self.date_end + ' 16:59:59'
            query_where += " AND cl.create_date <= '%s' "%(date_end)

        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        self._cr.execute("""
            SELECT
                row_number() OVER() as num,
                cl.sequence_name as file_number,
                rp.display_name as customer_name,
                rp.code as customer_code,
                cl.id as opportunity_id
            FROM
                crm_lead cl
            LEFT JOIN
                res_partner rp ON rp.id=cl.partner_id
            WHERE
                %s
        """%(query_where))
        datas = self._cr.dictfetchall()
        final_datas = []
        for data in datas :
            opportunity_id = self.env['crm.lead'].browse(data['opportunity_id'])
            sale_ids = opportunity_id.order_ids.filtered(lambda o: o.state != 'cancel')
            if sale_ids :
                sale_id = sale_ids[:1]
                if sale_id.confirmation_date :
                    new_data = data
                    new_data['cust_order'] = sale_id.origin or ''
                    new_data['sales_order'] = sale_id.name or ''
                    new_data['sales_order_date'] = sale_id.confirmation_date and (datetime.strptime(sale_id.confirmation_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S') or ''
                    new_data['so_value_usd'] = sale_id.amount_total
                    purchase_id = self.env['purchase.order'].search([
                        ('sale_id','=',sale_id.id),
                        ('state','!=','cancel'),
                    ], limit=1)
                    new_data['po_number'] = purchase_id and purchase_id.name or ''
                    new_data['po_number'] = purchase_id and purchase_id.name or ''
                    new_data['vendor_code'] = purchase_id and purchase_id.partner_id.code or ''
                    new_data['vendor_name'] = purchase_id and purchase_id.partner_id.name or ''
                    new_data['partner_ref'] = purchase_id and purchase_id.partner_ref or ''
                    new_data['po_date'] = purchase_id and (datetime.strptime(purchase_id.date_order, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S') or ''
                    new_data['po_solar_value_usd'] = purchase_id and purchase_id.amount_total or 0
                    new_data['incoterm'] = opportunity_id.transaction_method_id.name or ''
                    new_data['location'] = opportunity_id.delivery_location_id.name or ''
                    lead_line_id = self.env['crm.lead.line'].search([
                        ('lead_id','=',opportunity_id.id),
                    ], order='planned_revenue desc', limit=1)
                    if lead_line_id :
                        new_data['prod_categ'] = lead_line_id.product_id.categ_id.name
                    else :
                        new_data['prod_categ'] = ''
                    new_data['team_name'] = opportunity_id.team_id.name or ''
                    final_datas.append(new_data)
        return final_datas

    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format({'bold': 1,'align': 'center','valign': 'vcenter','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['orange'],'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['yellow'],'font_color': '#000000'})
        wbf['header_yellow'].set_border()
        
        wbf['header_no'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
                
        wbf['footer'] = workbook.add_format({'align':'left'})
        
        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()
        
        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right() 
        
        wbf['title_doc'] = workbook.add_format({'bold': 1,'align': 'left'})
        wbf['title_doc'].set_font_size(12)
        
        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)
        
        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right() 
        
        wbf['content_float'] = workbook.add_format({'align': 'right','num_format': '#,##0.00'})
        wbf['content_float'].set_right() 
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right() 
        wbf['content_number'].set_left() 
        
        wbf['content_percent'] = workbook.add_format({'align': 'right','num_format': '0.00%'})
        wbf['content_percent'].set_right() 
        wbf['content_percent'].set_left() 
                
        wbf['total_float'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()            
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()         
        
        wbf['total_number'] = workbook.add_format({'align':'right','bg_color': colors['white_orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()            
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()
        
        wbf['total'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'left'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()            
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()         
        
        wbf['total_number_yellow'] = workbook.add_format({'align':'right','bg_color': colors['yellow'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()            
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()
        
        wbf['total_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'center'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()            
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()         
        
        wbf['total_number_orange'] = workbook.add_format({'align':'right','bg_color': colors['orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()            
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()
        
        wbf['total_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'center'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()
        
        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()
        
        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()
        
        return wbf, workbook

    def print_excel_report_t0(self, result):
        data = self.read()[0]
        start_date = data['date_start']
        end_date = data['date_end']
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Status of Customer Inquiry'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 40)
        worksheet.set_column('D1:G1', 20)
        worksheet.set_column('H1:H1', 30)
        worksheet.set_column('I1:K1', 20)
        worksheet.set_column('L1:M1', 30)
        worksheet.set_column('N1:N1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', report_name, wbf['title_doc'])
        if start_date or end_date :
            worksheet.write('A3', 'Date: %s - %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        else :
            worksheet.write('A3', 'Date: All Time', wbf['content_datetime'])
        
        row=5
        
        worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
        worksheet.merge_range('B%s:E%s'%(row,row), 'Customer', wbf['header'])
        worksheet.merge_range('F%s:H%s'%(row,row), 'Inquiry File', wbf['header'])
        worksheet.merge_range('I%s:N%s'%(row,row), 'Quotation', wbf['header'])

        row+=1

        worksheet.write('B%s' %(row), 'Code', wbf['header'])
        worksheet.write('C%s' %(row), 'Name', wbf['header'])
        worksheet.write('D%s' %(row), 'Inquiry No', wbf['header'])
        worksheet.write('E%s' %(row), 'Inquiry Date', wbf['header'])
        worksheet.write('F%s' %(row), 'No', wbf['header'])
        worksheet.write('G%s' %(row), 'Date', wbf['header'])
        worksheet.write('H%s' %(row), 'Estimated Amount (USD)', wbf['header'])
        worksheet.write('I%s' %(row), 'No', wbf['header'])
        worksheet.write('J%s' %(row), 'Date', wbf['header'])
        worksheet.write('K%s' %(row), 'Amount (USD)', wbf['header'])
        worksheet.write('L%s' %(row), 'SBU', wbf['header'])
        worksheet.write('M%s' %(row), 'Product/Service Category', wbf['header'])
        worksheet.write('N%s' %(row), 'Incoterm', wbf['header'])
        
        row+=1
        no=1
        tot_quotation = 0
        tot_no_quotation = 0
        
        for res in result :
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res['customer_code'], wbf['content'])
            worksheet.write('C%s' %row, res['customer_name'], wbf['content'])
            worksheet.write('D%s' %row, res['customer_inquiry'], wbf['content'])
            worksheet.write('E%s' %row, res['inquiry_date'], wbf['content'])
            worksheet.write('F%s' %row, res['file_number'], wbf['content'])
            worksheet.write('G%s' %row, res['create_date'][:19], wbf['content'])
            worksheet.write('H%s' %row, '{0:,.2f}'.format(res['planned_revenue']), wbf['content_float'])
            worksheet.write('I%s' %row, res['quotation_number'], wbf['content'])
            worksheet.write('J%s' %row, res['quotation_date'], wbf['content'])
            worksheet.write('K%s' %row, '{0:,.2f}'.format(res['sale_amount']), wbf['content_float'])
            worksheet.write('L%s' %row, res['team_name'], wbf['content'])
            worksheet.write('M%s' %row, res['prod_categ'], wbf['content'])
            worksheet.write('N%s' %row, '%s %s'%(res['incoterm'] or '', res['location'] or ''), wbf['content'])

            row += 1
            no += 1

            if res['quotation_date'] :
                tot_quotation += 1
            else :
                tot_no_quotation += 1

        worksheet.merge_range('A%s:N%s'%(row,row), 'Quotation       : %s'%(tot_quotation), wbf['total'])
        row += 1
        worksheet.merge_range('A%s:N%s'%(row,row), 'No Quotation : %s'%(tot_no_quotation), wbf['total'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': report_name,
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_excel_report_t1(self, result):
        data = self.read()[0]
        start_date = data['date_start']
        end_date = data['date_end']
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Status of PTI Quotation'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 20)
        worksheet.set_column('D1:D1', 20)
        worksheet.set_column('E1:E1', 20)
        worksheet.set_column('F1:F1', 40)
        worksheet.set_column('G1:G1', 20)
        worksheet.set_column('H1:H1', 20)
        worksheet.set_column('I1:I1', 20)
        worksheet.set_column('J1:J1', 20)
        worksheet.set_column('K1:K1', 20)
        worksheet.set_column('L1:L1', 30)
        worksheet.set_column('M1:M1', 30)
        worksheet.set_column('N1:N1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', report_name, wbf['title_doc'])
        if start_date or end_date :
            worksheet.write('A3', 'Date: %s - %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        else :
            worksheet.write('A3', 'Date: All Time', wbf['content_datetime'])
        
        row=5
        
        worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
        worksheet.merge_range('B%s:E%s'%(row,row), 'Quotation', wbf['header'])
        worksheet.merge_range('F%s:H%s'%(row,row), 'Customer', wbf['header'])
        worksheet.merge_range('I%s:N%s'%(row,row), 'Sales Order', wbf['header'])

        row+=1

        worksheet.write('B%s' %(row), 'No', wbf['header'])
        worksheet.write('C%s' %(row), 'Date', wbf['header'])
        worksheet.write('D%s' %(row), 'Amount (USD)', wbf['header'])
        worksheet.write('E%s' %(row), 'Code', wbf['header'])
        worksheet.write('F%s' %(row), 'Name', wbf['header'])
        worksheet.write('G%s' %(row), 'Order No', wbf['header'])
        worksheet.write('H%s' %(row), 'Order Date', wbf['header'])
        worksheet.write('I%s' %(row), 'No', wbf['header'])
        worksheet.write('J%s' %(row), 'Date', wbf['header'])
        worksheet.write('K%s' %(row), 'Amount (USD)', wbf['header'])
        worksheet.write('L%s' %(row), 'SBU', wbf['header'])
        worksheet.write('M%s' %(row), 'Product/Service Category', wbf['header'])
        worksheet.write('N%s' %(row), 'Incoterm', wbf['header'])
        
        row+=1
        no=1
        tot_order = 0
        tot_no_order = 0
        
        for res in result :
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res['quotation_number'], wbf['content'])
            worksheet.write('C%s' %row, res['quotation_date'], wbf['content'])
            worksheet.write('D%s' %row, '{0:,.2f}'.format(res['so_value_usd']), wbf['content_float'])
            worksheet.write('E%s' %row, res['customer_code'], wbf['content'])
            worksheet.write('F%s' %row, res['customer_name'], wbf['content'])
            worksheet.write('G%s' %row, res['client_order_ref'], wbf['content'])
            worksheet.write('H%s' %row, res['client_order_date_ref'], wbf['content'])
            worksheet.write('I%s' %row, res['sales_order'], wbf['content'])
            worksheet.write('J%s' %row, res['sales_order_date'], wbf['content'])
            worksheet.write('K%s' %row, '{0:,.2f}'.format(res['so_value_usd']), wbf['content_float'])
            worksheet.write('L%s' %row, res['team_name'], wbf['content'])
            worksheet.write('M%s' %row, res['prod_categ'], wbf['content'])
            worksheet.write('N%s' %row, '%s %s'%(res['incoterm'] or '', res['location'] or ''), wbf['content'])

            row += 1
            no += 1

            if res['sales_order_date']:
                tot_order += 1
            else :
                tot_no_order += 1

        worksheet.merge_range('A%s:N%s'%(row,row), 'Ordered              : %s'%(tot_order), wbf['total'])
        row += 1
        worksheet.merge_range('A%s:N%s'%(row,row), 'Not Yet Ordered : %s'%(tot_no_order), wbf['total'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': report_name,
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def get_t1_5(self):
        query_where = " 1=1 "
        if self.date_start :
            date_start = datetime.strptime(self.date_start, '%Y-%m-%d') - timedelta(days=1)
            date_start = date_start.strftime('%Y-%m-%d 17:00:00')
            query_where += " AND so.confirmation_date >= '%s'"%(date_start)
        if self.date_end :
            date_end = self.date_end + ' 16:59:59'
            query_where += " AND so.confirmation_date <= '%s' "%(date_end)

        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        self._cr.execute("""
            SELECT
                so.name as order_name,
                so.confirmation_date + interval '%s' as confirmation_date,
                team.name as sbu,
                rp.name as customer,
                so.client_order_ref,
                method.name as incoterm,
                l.name as location,
                so.id,
                so.amount_total,
                so.state
            FROM
                sale_order so
            LEFT JOIN
                res_partner rp on rp.id=so.partner_id
            LEFT JOIN
                crm_team team on team.id=so.team_id
            LEFT JOIN
                stock_incoterms method on method.id=so.incoterm
            LEFT JOIN
                crm_lead cl on cl.id=so.opportunity_id
            LEFT JOIN
                stock_location l on l.id=cl.delivery_location_id
            WHERE
                so.confirmation_date is not null and so.state in ('sale','delivery','waiting_invoice','waiting_payment','done') and %s
        """%(tz,query_where))
        datas = self._cr.dictfetchall()
        final_datas = []
        for data in datas :
            new_data = data
            order_id = self.env['sale.order'].browse(data['id'])
            if order_id.order_line :
                max_line = order_id.order_line[0]
                tot_qty = 0
                qty_delivered = 0
                min_date = order_id.order_line[0].commitment_date
                max_date = order_id.order_line[0].commitment_date
                for line in order_id.order_line :
                    tot_qty += line.product_uom_qty
                    qty_delivered += line.qty_delivered
                    if line.product_uom_qty > max_line.product_uom_qty :
                        max_line = line
                    if line.commitment_date < min_date :
                        min_date = line.commitment_date
                    if line.commitment_date > min_date :
                        max_date = line.commitment_date
                try :
                    new_data['prod_categ'] = max_line.product_id.categ_id.name_get()[0][1]
                except :
                    new_data['prod_categ'] = max_line.product_id.categ_id.name
                new_data['date_from'] = (datetime.strptime(min_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S')
                new_data['date_to'] = (datetime.strptime(max_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S')
            else :
                new_data['prod_categ'] = ''
                new_data['date_from'] = ''
                new_data['date_to'] = ''

            new_data['qty_ordered'] = tot_qty
            new_data['qty_outstanding'] = tot_qty - qty_delivered
            new_data['qty_delivered'] = qty_delivered
            if tot_qty and qty_delivered :
                new_data['qty_percent'] = qty_delivered/tot_qty*100
            else :
                new_data['qty_percent'] = 0
            invoice_ids = order_id.invoice_ids.filtered(lambda inv: inv.state == 'open')
            inv_amount = sum(inv.amount_total for inv in invoice_ids)
            new_data['amount_outstanding'] = data['amount_total'] - inv_amount
            new_data['amount_invoice'] = inv_amount
            if data['amount_total'] and inv_amount :
                new_data['amount_percent'] = inv_amount/data['amount_total']*100
            else :
                new_data['amount_percent'] = 0
            new_data['state'] = SO_STATE[new_data['state']]
            final_datas.append(new_data)
        return final_datas

    def print_excel_report_t1_5(self, result):
        data = self.read()[0]
        start_date = data['date_start']
        end_date = data['date_end']
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Sales Order Status'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 30)
        worksheet.set_column('D1:D1', 30)
        worksheet.set_column('E1:E1', 30)
        worksheet.set_column('F1:F1', 30)
        worksheet.set_column('G1:G1', 20)
        worksheet.set_column('H1:H1', 20)
        worksheet.set_column('I1:I1', 20)
        worksheet.set_column('J1:J1', 20)
        worksheet.set_column('K1:K1', 20)
        worksheet.set_column('L1:L1', 20)
        worksheet.set_column('M1:M1', 20)
        worksheet.set_column('N1:N1', 20)
        worksheet.set_column('O1:O1', 20)
        worksheet.set_column('P1:P1', 20)
        worksheet.set_column('Q1:Q1', 20)
        worksheet.set_column('R1:R1', 20)
        worksheet.set_column('S1:S1', 20)
        worksheet.set_column('T1:T1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', report_name, wbf['title_doc'])
        if start_date or end_date :
            worksheet.write('A3', 'Date: %s - %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        else :
            worksheet.write('A3', 'Date: All Time', wbf['content_datetime'])
        
        row=5
        
        worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
        worksheet.merge_range('B%s:B%s'%(row,row+1), 'SO No', wbf['header'])
        worksheet.merge_range('C%s:C%s'%(row,row+1), 'Date', wbf['header'])
        worksheet.merge_range('D%s:D%s'%(row,row+1), 'SBU', wbf['header'])
        worksheet.merge_range('E%s:E%s'%(row,row+1), 'Category', wbf['header'])
        worksheet.merge_range('F%s:G%s'%(row,row), 'Customer', wbf['header'])
        worksheet.merge_range('H%s:I%s'%(row,row+1), 'Incoterm', wbf['header'])
        worksheet.merge_range('J%s:K%s'%(row,row), 'Delivery Date', wbf['header'])
        worksheet.merge_range('L%s:O%s'%(row,row), 'Qty', wbf['header'])
        worksheet.merge_range('P%s:S%s'%(row,row), 'Value', wbf['header'])
        worksheet.merge_range('T%s:T%s'%(row,row+1), 'Status', wbf['header'])

        row+=1

        worksheet.write('F%s' %(row), 'Name', wbf['header'])
        worksheet.write('G%s' %(row), 'Ref', wbf['header'])
        worksheet.write('J%s' %(row), 'From', wbf['header'])
        worksheet.write('K%s' %(row), 'To', wbf['header'])
        worksheet.write('L%s' %(row), 'Ordered', wbf['header'])
        worksheet.write('M%s' %(row), 'Outstanding', wbf['header'])
        worksheet.merge_range('N%s:O%s'%(row,row), 'Deliered', wbf['header'])
        worksheet.write('P%s' %(row), 'Quoted', wbf['header'])
        worksheet.write('Q%s' %(row), 'Balance', wbf['header'])
        worksheet.merge_range('R%s:S%s'%(row,row), 'Invoiced', wbf['header'])
        
        row+=1
        no=1
        
        for res in result :
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res['order_name'], wbf['content'])
            worksheet.write('C%s' %row, res['confirmation_date'], wbf['content'])
            worksheet.write('D%s' %row, res['sbu'], wbf['content'])
            worksheet.write('E%s' %row, res['prod_categ'], wbf['content'])
            worksheet.write('F%s' %row, res['customer'], wbf['content'])
            worksheet.write('G%s' %row, res['client_order_ref'], wbf['content'])
            worksheet.write('H%s' %row, res['incoterm'], wbf['content'])
            worksheet.write('I%s' %row, res['location'], wbf['content'])
            worksheet.write('J%s' %row, res['date_from'], wbf['content'])
            worksheet.write('K%s' %row, res['date_to'], wbf['content'])
            worksheet.write('L%s' %row, '{0:,.2f}'.format(res['qty_ordered']), wbf['content_float'])
            worksheet.write('M%s' %row, '{0:,.2f}'.format(res['qty_outstanding']), wbf['content_float'])
            worksheet.write('N%s' %row, '{0:,.2f}'.format(res['qty_delivered']), wbf['content_float'])
            worksheet.write('O%s' %row, '%s%%'%('{0:,.2f}'.format(res['qty_percent'])), wbf['content_float'])
            worksheet.write('P%s' %row, '{0:,.2f}'.format(res['amount_total']), wbf['content_float'])
            worksheet.write('Q%s' %row, '{0:,.2f}'.format(res['amount_outstanding']), wbf['content_float'])
            worksheet.write('R%s' %row, '{0:,.2f}'.format(res['amount_invoice']), wbf['content_float'])
            worksheet.write('S%s' %row, '%s%%'%('{0:,.2f}'.format(res['amount_percent'])), wbf['content_float'])
            worksheet.write('T%s' %row, res['state'], wbf['content'])

            row += 1
            no += 1

        worksheet.merge_range('A%s:T%s'%(row,row), '', wbf['total'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': report_name,
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def print_excel_report_t2(self, result):
        data = self.read()[0]
        start_date = data['date_start']
        end_date = data['date_end']
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Purchase Order to Principal'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 30)
        worksheet.set_column('D1:D1', 20)
        worksheet.set_column('E1:E1', 20)
        worksheet.set_column('F1:F1', 20)
        worksheet.set_column('G1:G1', 20)
        worksheet.set_column('H1:H1', 30)
        worksheet.set_column('I1:I1', 20)
        worksheet.set_column('J1:J1', 20)
        worksheet.set_column('K1:K1', 20)
        worksheet.set_column('L1:L1', 20)
        worksheet.set_column('M1:M1', 20)
        worksheet.set_column('N1:N1', 20)
        worksheet.set_column('O1:O1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', report_name, wbf['title_doc'])
        if start_date or end_date :
            worksheet.write('A3', 'Date: %s - %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        else :
            worksheet.write('A3', 'Date: All Time', wbf['content_datetime'])
        
        row=5
        
        worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
        worksheet.merge_range('B%s:C%s'%(row,row), 'Customer', wbf['header'])
        worksheet.merge_range('D%s:I%s'%(row,row), 'Sales Order', wbf['header'])
        worksheet.merge_range('J%s:L%s'%(row,row), 'Purchase Order', wbf['header'])
        worksheet.merge_range('M%s:O%s'%(row,row), 'Principal/Vendor', wbf['header'])

        row+=1

        worksheet.write('B%s' %(row), 'Code', wbf['header'])
        worksheet.write('C%s' %(row), 'Name', wbf['header'])
        worksheet.write('D%s' %(row), 'No', wbf['header'])
        worksheet.write('E%s' %(row), 'Date', wbf['header'])
        worksheet.write('F%s' %(row), 'Amount (USD)', wbf['header'])
        worksheet.write('G%s' %(row), 'SBU', wbf['header'])
        worksheet.write('H%s' %(row), 'Product/Service Category', wbf['header'])
        worksheet.write('I%s' %(row), 'Incoterm', wbf['header'])
        worksheet.write('J%s' %(row), 'No', wbf['header'])
        worksheet.write('K%s' %(row), 'Date', wbf['header'])
        worksheet.write('L%s' %(row), 'Amount (USD)', wbf['header'])
        worksheet.write('M%s' %(row), 'Code', wbf['header'])
        worksheet.write('N%s' %(row), 'Name', wbf['header'])
        worksheet.write('O%s' %(row), 'Ref No', wbf['header'])
        
        row+=1
        no=1

        tot_order = 0
        tot_no_order = 0
        
        for res in result :
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res['customer_code'], wbf['content'])
            worksheet.write('C%s' %row, res['customer_name'], wbf['content'])
            worksheet.write('D%s' %row, res['sales_order'], wbf['content'])
            worksheet.write('E%s' %row, res['sales_order_date'], wbf['content'])
            worksheet.write('F%s' %row, '{0:,.2f}'.format(res['so_value_usd']), wbf['content_float'])
            worksheet.write('G%s' %row, res['team_name'], wbf['content'])
            worksheet.write('H%s' %row, res['prod_categ'], wbf['content'])
            worksheet.write('I%s' %row, '%s %s'%(res['incoterm'] or '', res['location'] or ''), wbf['content'])
            worksheet.write('J%s' %row, res['po_number'], wbf['content'])
            worksheet.write('K%s' %row, res['po_date'], wbf['content'])
            worksheet.write('L%s' %row, '{0:,.2f}'.format(res['po_solar_value_usd']), wbf['content_float'])
            worksheet.write('M%s' %row, res['vendor_code'], wbf['content'])
            worksheet.write('N%s' %row, res['vendor_name'], wbf['content'])
            worksheet.write('O%s' %row, res['partner_ref'], wbf['content'])

            row += 1
            no += 1

            if res['po_number']:
                tot_order += 1
            else :
                tot_no_order += 1

        worksheet.merge_range('A%s:O%s'%(row,row), 'Ordered to Principal              : %s'%(tot_order), wbf['total'])
        row += 1
        worksheet.merge_range('A%s:O%s'%(row,row), 'Not Yet Ordered to Principal : %s'%(tot_no_order), wbf['total'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': report_name,
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

    def get_t3(self):
        query_where = " 1=1 "
        if self.date_start :
            date_start = datetime.strptime(self.date_start, '%Y-%m-%d') - timedelta(days=1)
            date_start = date_start.strftime('%Y-%m-%d 17:00:00')
            query_where += " AND po.date_order >= '%s'"%(date_start)
        if self.date_end :
            date_end = self.date_end + ' 16:59:59'
            query_where += " AND po.date_order <= '%s' "%(date_end)

        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        self._cr.execute("""
            SELECT
                po.name as order_name,
                po.date_order + interval '%s' as date_order,
                team.name as sbu,
                rp.name as supplier,
                po.partner_ref,
                method.name as incoterm,
                l.name as location,
                po.id,
                po.amount_total,
                po.state
            FROM
                purchase_order po
            LEFT JOIN
                res_partner rp on rp.id=po.partner_id
            LEFT JOIN
                sale_order so on so.id=po.sale_id
            LEFT JOIN
                crm_team team on team.id=so.team_id
            LEFT JOIN
                stock_incoterms method on method.id=po.incoterm_id
            LEFT JOIN
                stock_location l on l.id=po.delivery_location_id
            WHERE
                po.state in ('purchase','done') and %s
        """%(tz,query_where))
        datas = self._cr.dictfetchall()
        final_datas = []
        for data in datas :
            new_data = data
            order_id = self.env['purchase.order'].browse(data['id'])
            if order_id.order_line :
                max_line = order_id.order_line[0]
                tot_qty = 0
                qty_received = 0
                min_date = order_id.order_line[0].date_planned
                max_date = order_id.order_line[0].date_planned
                for line in order_id.order_line :
                    tot_qty += line.product_qty
                    qty_received += line.qty_received
                    if line.product_qty > max_line.product_qty :
                        max_line = line
                    if line.date_planned < min_date :
                        min_date = line.date_planned
                    if line.date_planned > min_date :
                        max_date = line.date_planned
                try :
                    new_data['prod_categ'] = max_line.product_id.categ_id.name_get()[0][1]
                except :
                    new_data['prod_categ'] = max_line.product_id.categ_id.name
                new_data['date_from'] = (datetime.strptime(min_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S')
                new_data['date_to'] = (datetime.strptime(max_date, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S')
            else :
                new_data['prod_categ'] = ''
                new_data['date_from'] = ''
                new_data['date_to'] = ''

            new_data['qty_ordered'] = tot_qty
            new_data['qty_outstanding'] = tot_qty - qty_received
            new_data['qty_received'] = qty_received
            if tot_qty and qty_received :
                new_data['qty_percent'] = qty_received/tot_qty*100
            else :
                new_data['qty_percent'] = 0
            invoice_ids = order_id.invoice_ids.filtered(lambda inv: inv.state == 'open')
            inv_amount = sum(inv.amount_total for inv in invoice_ids)
            new_data['amount_outstanding'] = data['amount_total'] - inv_amount
            new_data['amount_invoice'] = inv_amount
            if data['amount_total'] and inv_amount :
                new_data['amount_percent'] = inv_amount/data['amount_total']*100
            else :
                new_data['amount_percent'] = 0
            new_data['state'] = PO_STATE[new_data['state']]
            final_datas.append(new_data)
        return final_datas

    def print_excel_report_t3(self, result):
        data = self.read()[0]
        start_date = data['date_start']
        end_date = data['date_end']
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Purchase Order Status'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 20)
        worksheet.set_column('C1:C1', 30)
        worksheet.set_column('D1:D1', 30)
        worksheet.set_column('E1:E1', 30)
        worksheet.set_column('F1:F1', 30)
        worksheet.set_column('G1:G1', 20)
        worksheet.set_column('H1:H1', 20)
        worksheet.set_column('I1:I1', 20)
        worksheet.set_column('J1:J1', 20)
        worksheet.set_column('K1:K1', 20)
        worksheet.set_column('L1:L1', 20)
        worksheet.set_column('M1:M1', 20)
        worksheet.set_column('N1:N1', 20)
        worksheet.set_column('O1:O1', 20)
        worksheet.set_column('P1:P1', 20)
        worksheet.set_column('Q1:Q1', 20)
        worksheet.set_column('R1:R1', 20)
        worksheet.set_column('S1:S1', 20)
        worksheet.set_column('T1:T1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', report_name, wbf['title_doc'])
        if start_date or end_date :
            worksheet.write('A3', 'Date: %s - %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        else :
            worksheet.write('A3', 'Date: All Time', wbf['content_datetime'])
        
        row=5
        
        worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
        worksheet.merge_range('B%s:B%s'%(row,row+1), 'Number', wbf['header'])
        worksheet.merge_range('C%s:C%s'%(row,row+1), 'Date', wbf['header'])
        worksheet.merge_range('D%s:D%s'%(row,row+1), 'SBU', wbf['header'])
        worksheet.merge_range('E%s:E%s'%(row,row+1), 'Category', wbf['header'])
        worksheet.merge_range('F%s:G%s'%(row,row), 'Supplier', wbf['header'])
        worksheet.merge_range('H%s:I%s'%(row,row+1), 'Incoterm', wbf['header'])
        worksheet.merge_range('J%s:K%s'%(row,row), 'Delivery Date', wbf['header'])
        worksheet.merge_range('L%s:O%s'%(row,row), 'Qty', wbf['header'])
        worksheet.merge_range('P%s:S%s'%(row,row), 'Value', wbf['header'])
        worksheet.merge_range('T%s:T%s'%(row,row+1), 'Status', wbf['header'])

        row+=1

        worksheet.write('F%s' %(row), 'Name', wbf['header'])
        worksheet.write('G%s' %(row), 'Ref', wbf['header'])
        worksheet.write('J%s' %(row), 'From', wbf['header'])
        worksheet.write('K%s' %(row), 'To', wbf['header'])
        worksheet.write('L%s' %(row), 'Ordered', wbf['header'])
        worksheet.write('M%s' %(row), 'Outstanding', wbf['header'])
        worksheet.merge_range('N%s:O%s'%(row,row), 'Received', wbf['header'])
        worksheet.write('P%s' %(row), 'Quoted', wbf['header'])
        worksheet.write('Q%s' %(row), 'Balance', wbf['header'])
        worksheet.merge_range('R%s:S%s'%(row,row), 'Invoiced', wbf['header'])
        
        row+=1
        no=1
        
        for res in result :
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res['order_name'], wbf['content'])
            worksheet.write('C%s' %row, res['date_order'], wbf['content'])
            worksheet.write('D%s' %row, res['sbu'], wbf['content'])
            worksheet.write('E%s' %row, res['prod_categ'], wbf['content'])
            worksheet.write('F%s' %row, res['supplier'], wbf['content'])
            worksheet.write('G%s' %row, res['partner_ref'], wbf['content'])
            worksheet.write('H%s' %row, res['incoterm'], wbf['content'])
            worksheet.write('I%s' %row, res['location'], wbf['content'])
            worksheet.write('J%s' %row, res['date_from'], wbf['content'])
            worksheet.write('K%s' %row, res['date_to'], wbf['content'])
            worksheet.write('L%s' %row, '{0:,.2f}'.format(res['qty_ordered']), wbf['content_float'])
            worksheet.write('M%s' %row, '{0:,.2f}'.format(res['qty_outstanding']), wbf['content_float'])
            worksheet.write('N%s' %row, '{0:,.2f}'.format(res['qty_received']), wbf['content_float'])
            worksheet.write('O%s' %row, '%s%%'%('{0:,.2f}'.format(res['qty_percent'])), wbf['content_float'])
            worksheet.write('P%s' %row, '{0:,.2f}'.format(res['amount_total']), wbf['content_float'])
            worksheet.write('Q%s' %row, '{0:,.2f}'.format(res['amount_outstanding']), wbf['content_float'])
            worksheet.write('R%s' %row, '{0:,.2f}'.format(res['amount_invoice']), wbf['content_float'])
            worksheet.write('S%s' %row, '%s%%'%('{0:,.2f}'.format(res['amount_percent'])), wbf['content_float'])
            worksheet.write('T%s' %row, res['state'], wbf['content'])

            row += 1
            no += 1

        worksheet.merge_range('A%s:T%s'%(row,row), '', wbf['total'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': report_name,
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Report Order',
    'version'       : '0.1',
    'summary'       : 'Relation between Opportunity, Quotation, Customer PO and PO to Vendor',
    'author'        : 'Arkana',
    'description'   : """
Features :
- Export to pdf
    """,
    'depends'       : [
        'crm_dev',
    ],
    'data': [
        'wizard/report_order_wizard.xml',
        'data/report_paperformat.xml',
        'report/t0.xml',
        'report/t1.xml',
        'report/t1_5.xml',
        'report/t2.xml',
        'report/t3.xml',
    ],
    'demo': [],
    'installable': True
}
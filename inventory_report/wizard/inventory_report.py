import xlsxwriter
import base64
from odoo import fields, models, api
from datetime import datetime, timedelta
from io import BytesIO
from collections import OrderedDict
import pytz
import string
from pytz import timezone

class InventoryReport(models.TransientModel):
    _name = "inventory.report"
    _description = "Inventory Report .xlsx"
    
    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))
    
    data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', readonly=True)
    warehouse_ids = fields.Many2many('stock.warehouse', 'inventory_report_warehouse_rel', 'inventory_report_id',
        'warehouse_id', 'Warehouse')
        
    def print_excel_report(self):
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Inventory Report'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)

        if self.warehouse_ids :
            warehouse_ids = self.warehouse_ids
        else :
            warehouse_ids = self.env['stock.warehouse'].search([])

        for warehouse_id in warehouse_ids :
            #WKS 1
            worksheet = workbook.add_worksheet(warehouse_id.name)
            worksheet.set_column('A1:A1', 5)
            worksheet.set_column('B1:B1', 30)
            worksheet.set_column('C1:J1', 20)
            worksheet.set_column('K1:K1', 40)
            
            #WKS 1
            worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
            worksheet.write('A2', report_name, wbf['title_doc'])
            worksheet.write('A3', 'Date: %s'%(datetime_string), wbf['content_datetime'])
            
            row=5
            
            worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
            worksheet.merge_range('B%s:B%s'%(row,row+1), 'Customer Name', wbf['header'])
            worksheet.merge_range('C%s:E%s'%(row,row), 'Customer', wbf['header'])
            worksheet.merge_range('F%s:F%s'%(row,row+1), 'PTI SO No', wbf['header'])
            worksheet.merge_range('G%s:I%s'%(row,row), 'Currency', wbf['header'])
            worksheet.merge_range('J%s:J%s'%(row,row+1), 'Amount (US$)', wbf['header'])
            worksheet.merge_range('K%s:K%s'%(row,row+1), 'Remarks', wbf['header'])

            row+=1

            worksheet.write('C%s' %(row), 'PO Date', wbf['header'])
            worksheet.write('D%s' %(row), 'RTS Date', wbf['header'])
            worksheet.write('E%s' %(row), 'PO No', wbf['header'])
            worksheet.write('G%s' %(row), 'Cust. PO', wbf['header'])
            worksheet.write('H%s' %(row), 'PO to Solar', wbf['header'])
            worksheet.write('I%s' %(row), 'Invoice to Cust', wbf['header'])
            
            row+=1
            no=1

            location_ids = self.env['stock.location'].search([('id','child_of',warehouse_id.view_location_id.id)])
            domain = [('location_id','in',location_ids.ids)]
            quant_ids = self.env['stock.quant'].search(domain)
            
            for quant in quant_ids :
                cust_name = ''
                po_date = ''
                rts_date = ''
                po_no = ''
                pti_so_no = ''
                cust_po = 0
                solar_po = 0
                invoice = 0
                amount = quant.quantity * quant.product_id.standard_price
                remark = ''

                worksheet.write('A%s' %row, no, wbf['content'])
                worksheet.write('B%s' %row, cust_name, wbf['content'])
                worksheet.write('C%s' %row, po_date, wbf['content'])
                worksheet.write('D%s' %row, rts_date, wbf['content'])
                worksheet.write('E%s' %row, po_no, wbf['content'])
                worksheet.write('F%s' %row, pti_so_no, wbf['content'])
                worksheet.write('G%s' %row, cust_po, wbf['content_float'])
                worksheet.write('H%s' %row, solar_po, wbf['content_float'])
                worksheet.write('I%s' %row, invoice, wbf['content_float'])
                worksheet.write('J%s' %row, amount, wbf['content_float'])
                worksheet.write('K%s' %row, remark, wbf['content'])
                
                row+=1
                no+=1
            
            worksheet.merge_range('A%s:B%s'%(row,row), 'Total', wbf['total'])
            worksheet.write('C%s' %row, '', wbf['total_float'])
            worksheet.write('D%s' %row, '', wbf['total_float'])
            worksheet.write('E%s' %row, '', wbf['total_float'])
            worksheet.write('F%s' %row, '', wbf['total_float'])
            worksheet.write('G%s' %row, '', wbf['total_float'])
            worksheet.write('H%s' %row, '', wbf['total_float'])
            worksheet.write('I%s' %row, '', wbf['total_float'])
            worksheet.write('J%s' %row, '', wbf['total_float'])
            worksheet.write('K%s' %row, '', wbf['total_float'])

            worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': 'Inventory Report',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }
    
    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format({'bold': 1,'align': 'center','valign': 'vcenter','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['orange'],'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['yellow'],'font_color': '#000000'})
        wbf['header_yellow'].set_border()
        
        wbf['header_no'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
                
        wbf['footer'] = workbook.add_format({'align':'left'})
        
        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()
        
        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right() 
        
        wbf['title_doc'] = workbook.add_format({'bold': 1,'align': 'left'})
        wbf['title_doc'].set_font_size(12)
        
        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)
        
        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right() 
        
        wbf['content_float'] = workbook.add_format({'align': 'right','num_format': '#,##0.00'})
        wbf['content_float'].set_right() 
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right() 
        wbf['content_number'].set_left() 
        
        wbf['content_percent'] = workbook.add_format({'align': 'right','num_format': '0.00%'})
        wbf['content_percent'].set_right() 
        wbf['content_percent'].set_left() 
                
        wbf['total_float'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()            
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()         
        
        wbf['total_number'] = workbook.add_format({'align':'right','bg_color': colors['white_orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()            
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()
        
        wbf['total'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'center'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()            
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()         
        
        wbf['total_number_yellow'] = workbook.add_format({'align':'right','bg_color': colors['yellow'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()            
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()
        
        wbf['total_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'center'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()            
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()         
        
        wbf['total_number_orange'] = workbook.add_format({'align':'right','bg_color': colors['orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()            
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()
        
        wbf['total_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'center'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()
        
        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()
        
        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()
        
        return wbf, workbook

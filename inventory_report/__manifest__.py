{
    "name"          : "Inventory Report .xlsx",
    "version"       : "1.0",
    "author"        : "Arkana",
    "website"       : "https://",
    "category"      : "Inventory",
    "summary"       : "Inventory Report .xlsx",
    "description"   : """
        Inventory Report .xlsx
    """,
    "depends"       : [
        "account",
    ],
    "data"          : [
        "wizard/inventory_report.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}
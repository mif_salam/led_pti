from odoo import fields, api, models, _
from odoo.exceptions import Warning

class DocumentChecklist(models.Model):
    _inherit = 'document.checklist'

    invoice_id = fields.Many2one('account.invoice')

    @api.model
    def create(self, vals):
        if 'invoice_id' in vals :
            invoice_id = self.env['account.invoice'].browse(vals['invoice_id'])
            vals['name'] = (invoice_id.sequence_number_next_prefix+invoice_id.sequence_number_next) or invoice_id.number or invoice_id.name or invoice_id.origin
        res = super(DocumentChecklist, self).create(vals)
        return res

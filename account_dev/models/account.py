from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    doc_ids = fields.One2many('document.checklist', 'invoice_id', string='Document Checklist')
    project_id = fields.Many2one('project.project', string="Project")

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.depends('payment_invoice_ids.approve','payment_invoice_ids.residual')
    @api.multi
    def _get_approve_amount(self):
        for payment in self :
            approve_amount = 0
            for inv in payment.payment_invoice_ids :
                if inv.approve :
                    approve_amount += inv.residual
            payment.approve_amount = approve_amount

    payment_invoice_ids = fields.One2many('account.payment.invoice', 'payment_id', string='Detail Invoice')
    approve_amount = fields.Monetary(string='Approved Amount', compute='_get_approve_amount', store=True)
    state = fields.Selection([('draft', 'Draft'), ('waiting','Waiting Approval'), ('approve','Approved'), ('posted', 'Posted'), ('sent', 'Sent'), ('reconciled', 'Reconciled'), ('reject', 'Rejected'), ('cancelled', 'Cancelled')], readonly=True, default='draft', copy=False, string="Status")

    @api.multi
    def approval_request(self):
        for payment in self :
            payment.write({'state':'waiting'})

    @api.multi
    def action_approve(self):
        for payment in self :
            payment.write({'state':'approve'})

    @api.multi
    def action_reject(self):
        for payment in self :
            payment.write({'state':'reject'})

    @api.multi
    def approval_draft(self):
        for payment in self :
            payment.write({'state':'draft'})

    @api.multi
    def post(self):
        for payment in self :
            payment.write({'state':'draft'})
            invoice_ids = payment.payment_invoice_ids.filtered(lambda inv_line: inv_line.approve).mapped('invoice_id')
            if invoice_ids :
                payment.write({'invoice_ids': [(6, 0, invoice_ids.ids)]})
        return super(AccountPayment, self).post()

class AccountPaymentInvoice(models.Model):
    _name = "account.payment.invoice"
    _description = "Account Payment Invoice"
    _rec_name = "invoice_id"

    payment_id = fields.Many2one('account.payment', string='Payment', ondelete='cascade')
    invoice_id = fields.Many2one('account.invoice', string='Invoice', required=True)
    partner_id = fields.Many2one('res.partner', string='Vendor', related='invoice_id.partner_id', store=True)
    project_id = fields.Many2one('project.project', string='Project', related='invoice_id.project_id', store=True)
    currency_id = fields.Many2one('res.currency', string='Currency', related='invoice_id.currency_id', store=True)
    amount_total = fields.Monetary(string='Total', store=True, related='invoice_id.amount_total')
    residual = fields.Monetary(string='Amount Due', related='invoice_id.residual', store=True, help="Remaining amount due.")
    date_due = fields.Date(string='Due Date',
        related='invoice_id.date_due', store=True,
        help="If you use payment terms, the due date will be computed automatically at the generation "
             "of accounting entries. The Payment terms may compute several due dates, for example 50% "
             "now and 50% in one month, but if you want to force a due date, make sure that the payment "
             "term is not set on the invoice. If you keep the Payment terms and the due date empty, it "
             "means direct payment.")
    state = fields.Selection([
            ('draft','Draft'),
            ('open', 'Open'),
            ('paid', 'Paid'),
            ('cancel', 'Cancelled'),
        ], string='Status', index=True, related='invoice_id.state', store=True,
        help=" * The 'Draft' status is used when a user is encoding a new and unconfirmed Invoice.\n"
             " * The 'Open' status is used when user creates invoice, an invoice number is generated. It stays in the open status till the user pays the invoice.\n"
             " * The 'Paid' status is set automatically when the invoice is paid. Its related journal entries may or may not be reconciled.\n"
             " * The 'Cancelled' status is used when user cancel invoice.")
    approve = fields.Boolean(string="Is Approve?")

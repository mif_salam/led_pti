# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Accounting',
    'version'       : '0.1',
    'summary'       : 'Custom Accounting Module for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- Document checklist
    """,
    'depends'       : [
        'base',
        'account',
        'document_checklist',
    ],
    'data': [
        'views/account_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
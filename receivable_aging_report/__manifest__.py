{
    "name"          : "Receivable Aging Report .xlsx",
    "version"       : "1.0",
    "author"        : "Arkana",
    "website"       : "https://",
    "category"      : "Accounting",
    "summary"       : "Receivable Aging Report .xlsx",
    "description"   : """
        Receivable Aging Report .xlsx
    """,
    "depends"       : [
        "account",
    ],
    "data"          : [
        "wizard/receivable_aging_report.xml",
    ],
    "demo"          : [],
    "test"          : [],
    "images"        : [],
    "qweb"          : [],
    "css"           : [],
    "application"   : True,
    "installable"   : True,
    "auto_install"  : False,
}
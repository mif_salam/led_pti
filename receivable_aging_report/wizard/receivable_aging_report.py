import xlsxwriter
import base64
from odoo import fields, models, api
from datetime import datetime, timedelta
from io import BytesIO
from collections import OrderedDict
import pytz
import string
from pytz import timezone

class ReceivableAgingReport(models.TransientModel):
    _name = "receivable.aging.report"
    _description = "Receivable Aging Report .xlsx"
    
    @api.model
    def get_default_date_model(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))
    
    data = fields.Binary('File', readonly=True)
    name = fields.Char('Filename', readonly=True)
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date', default=get_default_date_model)
    status = fields.Selection([
        ('outstanding', 'Outstanding'),
        ('reconciled', 'Reconciled')
    ], 'Status', default='outstanding')
    partner_ids = fields.Many2many('res.partner', 'receivable_aging_report_customer_rel', 'receivable_aging_report_id',
        'customer_id', 'Customer')
        
    def print_excel_report(self):
        data = self.read()[0]
        start_date = data['start_date']
        end_date = data['end_date']
        status = data['status']
        partner_ids = data['partner_ids']
        
        query_where = ' 1=1 '
        if start_date :
            query_where +=" AND aml.date >= '%s'"%str(start_date)
        if end_date :
            query_where +=" AND aml.date <= '%s'"%str(end_date)
        if partner_ids :
            query_where +=" AND aml.partner_id in %s"%str(
                tuple(partner_ids)).replace(',)', ')')
        if status == 'outstanding' :
            query_where +=" AND aml.reconciled = 'f'"
        elif status == 'reconciled' :
            query_where +=" AND aml.reconciled = 't'"
        
        datetime_string = self.get_default_date_model().strftime("%Y-%m-%d %H:%M:%S")
        date_string = self.get_default_date_model().strftime("%Y-%m-%d")
        report_name = 'Receivable Aging Report'
        filename = '%s %s%s'%(report_name,date_string,'.xlsx')
        
        timezone = self.env.user.tz or 'Asia/Jakarta'
        if timezone == 'Asia/Jayapura' :
            tz = '9 hours'
        elif timezone == 'Asia/Pontianak' :
            tz = '8 hours'
        else :
            tz = '7 hours'
        
        query = """
            SELECT 
                aml.invoice_id,
                rp.name as partner_name, 
                '' as po_date, 
                '' as po_ref, 
                '' as sale_num, 
                '' as prod_service, 
                ai.date_invoice,
                ai.number as inv_number,
                aml.amount_residual as amount, 
                current_date - aml.date_maturity as overdue, 
                apt.name as term
            FROM 
                account_move_line aml
            LEFT JOIN 
                account_invoice ai on ai.id = aml.invoice_id
            LEFT JOIN 
                res_partner rp on rp.id = aml.partner_id
            LEFT JOIN 
                account_account a on a.id = aml.account_id
            LEFT JOIN 
                account_payment_term apt on apt.id = ai.payment_term_id
            WHERE 
                ai.type = 'out_invoice' 
                AND a.internal_type = 'receivable' 
                AND %s
            ORDER BY rp.name, ai.number
        """%(query_where)
        
        self._cr.execute(query)
        result = self._cr.dictfetchall()
        
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        wbf, workbook = self.add_workbook_format(workbook)
        
        #WKS 1
        worksheet = workbook.add_worksheet(report_name)
        worksheet.set_column('A1:A1', 5)
        worksheet.set_column('B1:B1', 30)
        worksheet.set_column('C1:E1', 30)
        worksheet.set_column('F1:F1', 40)
        worksheet.set_column('G1:X1', 20)
        
        #WKS 1
        worksheet.write('A1', self.env.user.company_id.name, wbf['company'])
        worksheet.write('A2', report_name, wbf['title_doc'])
        if start_date or end_date :
            worksheet.write('A3', 'Date: %s - %s'%('-' if not start_date else start_date, '-' if not end_date else end_date), wbf['content_datetime'])
        else :
            worksheet.write('A3', 'Date: All Time', wbf['content_datetime'])
        
        row=5
        
        worksheet.merge_range('A%s:A%s'%(row,row+1), 'No', wbf['header'])
        worksheet.merge_range('B%s:B%s'%(row,row+1), 'Customer Name', wbf['header'])
        worksheet.merge_range('C%s:D%s'%(row,row), 'Customer PO', wbf['header'])
        worksheet.merge_range('E%s:E%s'%(row,row+1), 'Sales Order No', wbf['header'])
        worksheet.merge_range('F%s:F%s'%(row,row+1), 'Product & Services', wbf['header'])
        worksheet.merge_range('G%s:H%s'%(row,row), 'PTI Invoice', wbf['header'])
        worksheet.merge_range('I%s:L%s'%(row,row), 'Currency', wbf['header'])
        worksheet.merge_range('M%s:M%s'%(row,row+1), 'Amount', wbf['header'])
        worksheet.merge_range('N%s:S%s'%(row,row), 'A/R AGING', wbf['header'])
        worksheet.merge_range('T%s:T%s'%(row,row+1), 'Terms', wbf['header'])
        worksheet.merge_range('U%s:U%s'%(row,row+1), 'Problem', wbf['header'])
        worksheet.merge_range('V%s:V%s'%(row,row+1), 'Solusi', wbf['header'])
        worksheet.merge_range('W%s:W%s'%(row,row+1), 'PIC', wbf['header'])
        worksheet.merge_range('X%s:X%s'%(row,row+1), 'Target Date', wbf['header'])

        row+=1

        worksheet.write('C%s' %(row), 'PO Date', wbf['header'])
        worksheet.write('D%s' %(row), 'Ref No', wbf['header'])
        worksheet.write('G%s' %(row), 'Invoice Date', wbf['header'])
        worksheet.write('H%s' %(row), 'Ref No', wbf['header'])
        worksheet.write('I%s' %(row), 'INV', wbf['header'])
        worksheet.write('J%s' %(row), 'RCV', wbf['header'])
        worksheet.write('K%s' %(row), 'JISDOR', wbf['header'])
        worksheet.write('L%s' %(row), 'PAY', wbf['header'])
        worksheet.write('N%s' %(row), '0 - 7 Days', wbf['header'])
        worksheet.write('O%s' %(row), '8 - 30 Days', wbf['header'])
        worksheet.write('P%s' %(row), '31 - 60 Days', wbf['header'])
        worksheet.write('Q%s' %(row), '61 - 90 Days', wbf['header'])
        worksheet.write('R%s' %(row), '91 - 365 Days', wbf['header'])
        worksheet.write('S%s' %(row), '> 365 Days', wbf['header'])
        
        row+=1
        no=1

        tot_amount = 0
        day_0_7 = 0
        day_8_30 = 0
        day_31_60 = 0
        day_61_90 = 0
        day_91_365 = 0
        day_366 = 0
        
        for res in result :
            residual = res['amount']
            overdue = res['overdue']
            invoice_id = False
            sale_id = False
            if res['invoice_id']:
                invoice_id = self.env['account.invoice'].browse(res['invoice_id'])
                res['prod_service'] = ', '.join([line.name for line in invoice_id.invoice_line_ids])
                order_ids = invoice_id.invoice_line_ids.mapped('sale_line_ids').mapped('order_id')
                if order_ids :
                    sale_id = order_ids[:1]
                    res['sale_num'] = sale_id.name
                    res['po_date'] = (datetime.strptime(sale_id.date_order, '%Y-%m-%d %H:%M:%S') + timedelta(hours=7)).strftime('%Y-%m-%d %H:%M:%S')
                    res['po_ref'] = sale_id.origin
            
            worksheet.write('A%s' %row, no, wbf['content'])
            worksheet.write('B%s' %row, res['partner_name'], wbf['content'])
            worksheet.write('C%s' %row, res['po_date'], wbf['content'])
            worksheet.write('D%s' %row, res['po_ref'], wbf['content'])
            worksheet.write('E%s' %row, res['sale_num'], wbf['content'])
            worksheet.write('F%s' %row, res['prod_service'], wbf['content'])
            worksheet.write('G%s' %row, res['date_invoice'], wbf['content'])
            worksheet.write('H%s' %row, res['inv_number'], wbf['content'])
            worksheet.write('I%s' %row, '', wbf['content'])
            worksheet.write('J%s' %row, '', wbf['content'])
            worksheet.write('K%s' %row, '', wbf['content'])
            worksheet.write('L%s' %row, '', wbf['content'])
            worksheet.write('M%s' %row, res['amount'], wbf['content_float'])
            worksheet.write('N%s' %row, residual if overdue <= 7 else 0, wbf['content_float'])
            worksheet.write('O%s' %row, residual if overdue >= 8 and overdue <= 30 else 0, wbf['content_float'])
            worksheet.write('P%s' %row, residual if overdue >= 31 and overdue <= 60 else 0, wbf['content_float'])
            worksheet.write('Q%s' %row, residual if overdue >= 61 and overdue <= 90 else 0, wbf['content_float'])
            worksheet.write('R%s' %row, residual if overdue >= 91 and overdue <= 365 else 0, wbf['content_float'])
            worksheet.write('S%s' %row, residual if overdue > 365 else 0, wbf['content_float'])
            worksheet.write('T%s' %row, res['term'], wbf['content'])
            worksheet.write('U%s' %row, '', wbf['content'])
            worksheet.write('V%s' %row, '', wbf['content'])
            worksheet.write('W%s' %row, '', wbf['content'])
            worksheet.write('X%s' %row, '', wbf['content'])


            tot_amount += res['amount']
            if overdue <= 7 :
                day_0_7 += residual
            if overdue >= 8 and overdue <= 30 :
                day_8_30 += residual
            if overdue >= 31 and overdue <= 60 :
                day_31_60 += residual
            if overdue >= 61 and overdue <= 90 :
                day_61_90 += residual
            if overdue >= 91 and overdue <= 365 :
                day_91_365 += residual
            if overdue > 365 :
                day_366 += residual
            
            row+=1
            no+=1
        
        worksheet.merge_range('A%s:B%s'%(row,row), 'Grand Total (USD)', wbf['total'])
        worksheet.write('C%s' %row, '', wbf['total_float'])
        worksheet.write('D%s' %row, '', wbf['total_float'])
        worksheet.write('E%s' %row, '', wbf['total_float'])
        worksheet.write('F%s' %row, '', wbf['total_float'])
        worksheet.write('G%s' %row, '', wbf['total_float'])
        worksheet.write('H%s' %row, '', wbf['total_float'])
        worksheet.write('I%s' %row, '', wbf['total_float'])
        worksheet.write('J%s' %row, '', wbf['total_float'])
        worksheet.write('K%s' %row, '', wbf['total_float'])
        worksheet.write('L%s' %row, '', wbf['total_float'])
        worksheet.write('M%s' %row, tot_amount, wbf['total_float'])
        worksheet.write('N%s' %row, day_0_7, wbf['total_float'])
        worksheet.write('O%s' %row, day_8_30, wbf['total_float'])
        worksheet.write('P%s' %row, day_31_60, wbf['total_float'])
        worksheet.write('Q%s' %row, day_61_90, wbf['total_float'])
        worksheet.write('R%s' %row, day_91_365, wbf['total_float'])
        worksheet.write('S%s' %row, day_366, wbf['total_float'])
        worksheet.write('T%s' %row, '', wbf['total_float'])
        worksheet.write('U%s' %row, '', wbf['total_float'])
        worksheet.write('V%s' %row, '', wbf['total_float'])
        worksheet.write('W%s' %row, '', wbf['total_float'])
        worksheet.write('X%s' %row, '', wbf['total_float'])

        row += 1

        idr_id = self.env.ref('base.IDR')
        usd_id = self.env.user.company_id.currency_id
        tot_amount = usd_id.compute(tot_amount, idr_id)
        day_0_7 = usd_id.compute(day_0_7, idr_id)
        day_8_30 = usd_id.compute(day_8_30, idr_id)
        day_31_60 = usd_id.compute(day_31_60, idr_id)
        day_61_90 = usd_id.compute(day_61_90, idr_id)
        day_91_365 = usd_id.compute(day_91_365, idr_id)
        day_366 = usd_id.compute(day_366, idr_id)

        worksheet.merge_range('A%s:B%s'%(row,row), 'Grand Total (IDR)', wbf['total'])
        worksheet.write('C%s' %row, '', wbf['total_float'])
        worksheet.write('D%s' %row, '', wbf['total_float'])
        worksheet.write('E%s' %row, '', wbf['total_float'])
        worksheet.write('F%s' %row, '', wbf['total_float'])
        worksheet.write('G%s' %row, '', wbf['total_float'])
        worksheet.write('H%s' %row, '', wbf['total_float'])
        worksheet.write('I%s' %row, '', wbf['total_float'])
        worksheet.write('J%s' %row, '', wbf['total_float'])
        worksheet.write('K%s' %row, '', wbf['total_float'])
        worksheet.write('L%s' %row, '', wbf['total_float'])
        worksheet.write('M%s' %row, tot_amount, wbf['total_float'])
        worksheet.write('N%s' %row, day_0_7, wbf['total_float'])
        worksheet.write('O%s' %row, day_8_30, wbf['total_float'])
        worksheet.write('P%s' %row, day_31_60, wbf['total_float'])
        worksheet.write('Q%s' %row, day_61_90, wbf['total_float'])
        worksheet.write('R%s' %row, day_91_365, wbf['total_float'])
        worksheet.write('S%s' %row, day_366, wbf['total_float'])
        worksheet.write('T%s' %row, '', wbf['total_float'])
        worksheet.write('U%s' %row, '', wbf['total_float'])
        worksheet.write('V%s' %row, '', wbf['total_float'])
        worksheet.write('W%s' %row, '', wbf['total_float'])
        worksheet.write('X%s' %row, '', wbf['total_float'])
        worksheet.write('A%s'%(row+2), '%s %s'%(datetime_string, self.env.user.name), wbf['footer'])
        
        workbook.close()
        out=base64.encodestring(fp.getvalue())
        self.write({'data':out, 'name':filename})
        fp.close()
        
        self.write({'data':out})
        url = "web/content/?model="+self._name+"&id="+str(self.id)+"&field=data&download=true&filename="+filename
        return {
            'name': 'Receivable Aging Report',
            'type': 'ir.actions.act_url',
            'url': url,
            'target': 'new',
        }
    
    def add_workbook_format(self, workbook):
        colors = {
            'white_orange': '#FFFFDB',
            'orange': '#FFC300',
            'red': '#FF0000',
            'yellow': '#F6FA03',
        }

        wbf = {}
        wbf['header'] = workbook.add_format({'bold': 1,'align': 'center','valign': 'vcenter','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header'].set_border()

        wbf['header_orange'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['orange'],'font_color': '#000000'})
        wbf['header_orange'].set_border()

        wbf['header_yellow'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': colors['yellow'],'font_color': '#000000'})
        wbf['header_yellow'].set_border()
        
        wbf['header_no'] = workbook.add_format({'bold': 1,'align': 'center','bg_color': '#FFFFDB','font_color': '#000000'})
        wbf['header_no'].set_border()
        wbf['header_no'].set_align('vcenter')
                
        wbf['footer'] = workbook.add_format({'align':'left'})
        
        wbf['content_datetime'] = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})
        wbf['content_datetime'].set_left()
        wbf['content_datetime'].set_right()
        
        wbf['content_date'] = workbook.add_format({'num_format': 'yyyy-mm-dd'})
        wbf['content_date'].set_left()
        wbf['content_date'].set_right() 
        
        wbf['title_doc'] = workbook.add_format({'bold': 1,'align': 'left'})
        wbf['title_doc'].set_font_size(12)
        
        wbf['company'] = workbook.add_format({'align': 'left'})
        wbf['company'].set_font_size(11)
        
        wbf['content'] = workbook.add_format()
        wbf['content'].set_left()
        wbf['content'].set_right() 
        
        wbf['content_float'] = workbook.add_format({'align': 'right','num_format': '#,##0.00'})
        wbf['content_float'].set_right() 
        wbf['content_float'].set_left()

        wbf['content_number'] = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
        wbf['content_number'].set_right() 
        wbf['content_number'].set_left() 
        
        wbf['content_percent'] = workbook.add_format({'align': 'right','num_format': '0.00%'})
        wbf['content_percent'].set_right() 
        wbf['content_percent'].set_left() 
                
        wbf['total_float'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float'].set_top()
        wbf['total_float'].set_bottom()            
        wbf['total_float'].set_left()
        wbf['total_float'].set_right()         
        
        wbf['total_number'] = workbook.add_format({'align':'right','bg_color': colors['white_orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number'].set_top()
        wbf['total_number'].set_bottom()            
        wbf['total_number'].set_left()
        wbf['total_number'].set_right()
        
        wbf['total'] = workbook.add_format({'bold':1, 'bg_color':colors['white_orange'], 'align':'center'})
        wbf['total'].set_left()
        wbf['total'].set_right()
        wbf['total'].set_top()
        wbf['total'].set_bottom()

        wbf['total_float_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_yellow'].set_top()
        wbf['total_float_yellow'].set_bottom()            
        wbf['total_float_yellow'].set_left()
        wbf['total_float_yellow'].set_right()         
        
        wbf['total_number_yellow'] = workbook.add_format({'align':'right','bg_color': colors['yellow'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_yellow'].set_top()
        wbf['total_number_yellow'].set_bottom()            
        wbf['total_number_yellow'].set_left()
        wbf['total_number_yellow'].set_right()
        
        wbf['total_yellow'] = workbook.add_format({'bold':1, 'bg_color':colors['yellow'], 'align':'center'})
        wbf['total_yellow'].set_left()
        wbf['total_yellow'].set_right()
        wbf['total_yellow'].set_top()
        wbf['total_yellow'].set_bottom()

        wbf['total_float_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'right', 'num_format':'#,##0.00'})
        wbf['total_float_orange'].set_top()
        wbf['total_float_orange'].set_bottom()            
        wbf['total_float_orange'].set_left()
        wbf['total_float_orange'].set_right()         
        
        wbf['total_number_orange'] = workbook.add_format({'align':'right','bg_color': colors['orange'],'bold':1, 'num_format': '#,##0'})
        wbf['total_number_orange'].set_top()
        wbf['total_number_orange'].set_bottom()            
        wbf['total_number_orange'].set_left()
        wbf['total_number_orange'].set_right()
        
        wbf['total_orange'] = workbook.add_format({'bold':1, 'bg_color':colors['orange'], 'align':'center'})
        wbf['total_orange'].set_left()
        wbf['total_orange'].set_right()
        wbf['total_orange'].set_top()
        wbf['total_orange'].set_bottom()
        
        wbf['header_detail_space'] = workbook.add_format({})
        wbf['header_detail_space'].set_left()
        wbf['header_detail_space'].set_right()
        wbf['header_detail_space'].set_top()
        wbf['header_detail_space'].set_bottom()
        
        wbf['header_detail'] = workbook.add_format({'bg_color': '#E0FFC2'})
        wbf['header_detail'].set_left()
        wbf['header_detail'].set_right()
        wbf['header_detail'].set_top()
        wbf['header_detail'].set_bottom()
        
        return wbf, workbook

# -*- coding: utf-8 -*-
{
    'name': "crm_lead",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/11.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'crm', 'sale', 'sale_crm', 'crm_lead_product', 'sales_team', 'partner_credit_limit', 'abs_project_so', 'partner_dev'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'data/email_template.xml',
        'data/transaction_type.xml',
        'data/res_groups.xml',
        'data/crm_stage.xml',
        'data/ir_sequence.xml',
        'data/mail_template_data.xml',
        'views/views.xml',
        'views/templates.xml',
        'data/ir_sequence_data.xml',
        'data/transaction_method.xml',
        'data/ir_cron.xml',
        'views/mcf_views.xml',
        'views/type_views.xml',
        'views/sale_view.xml',
        'views/res_partner_view.xml',
        'views/crm_team_view.xml',
        'wizard/failed_reason_wizard.xml',
        'report/sale_report.xml',
        'security/ir.model.access.csv',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
from odoo import fields, api, models, _

class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        res = super(MailComposer, self).send_mail_action()
        if self.model == 'sale.order' and not self._context.get('proforma', False):
            order_id = self.env['sale.order'].browse(self.res_id)
            order_id.write({'state':'sent'})
            stage_approval = self.env.ref('crm.stage_lead8')
            if order_id.opportunity_id and order_id.opportunity_id.stage_id == stage_approval :
                stage_waiting_client = self.env.ref('crm.stage_lead9')
                if stage_waiting_client :
                    order_id.opportunity_id.write({
                        'stage_id' : stage_waiting_client.id,
                        'automatic_stage_trigger' : True,
                    })
        return res

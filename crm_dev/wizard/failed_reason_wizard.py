from odoo import fields, api, models, _
from odoo.exceptions import Warning, UserError
from odoo.addons import decimal_precision as dp

class FailedReasonWizard(models.TransientModel):
    _name = "failed.reason.wizard"
    _description = "Failed Reason"

    name = fields.Text(string='Reason', required=True)

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        order_id = self.env['sale.order'].browse(self._context['active_id'])
        order_id.write({
            'state': 'cancel',
            'failed_reason': self.name
        })
        
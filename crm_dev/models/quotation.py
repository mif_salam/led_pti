from odoo import models, fields, api
from odoo.exceptions import Warning

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting Approval'),
        ('approved_manager', 'Approved by Manager'),
        ('approved', 'Approved'),
        ('sent', 'Sent to Customer'),
        ('accepted', 'Accepted by Customer'),
        ('sale', 'Waiting for Delivery'),
        ('delivery', 'Delivery Process'),
        ('waiting_invoice', 'Waiting Invoice'),
        ('waiting_payment', 'Waiting Payment'),
        ('done', 'Done'),
        ('cancel', 'Cancel / Rejected / Failed'),
    ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
    sent_doc = fields.Binary(string='Sent Doc')
    sent_doc_name = fields.Char(string='Sent Doc Name')
    customer_po = fields.Binary(string='Customer PO')
    customer_po_name = fields.Char(string='Customer PO Name')
    failed_reason = fields.Text(string='Failed Reason')
    quotation_number = fields.Char(string='Quotation Number', copy=False)
    client_order_date_ref = fields.Date(string='Customer Order Date')
    proforma_number = fields.Char(string='Proforma Invoice Number', copy=False)

    @api.model
    def create(self, vals):
        quotation_number = self.env['ir.sequence'].next_by_code('sale.order.qu')
        vals['name'] = quotation_number
        vals['quotation_number'] = quotation_number
        vals['proforma_number'] = self.env['ir.sequence'].next_by_code('sale.order.proforma')
        sale = super(SaleOrder, self).create(vals)
        stage_preparation = self.env.ref('crm.stage_lead7')
        if sale.opportunity_id and sale.opportunity_id.stage_id == stage_preparation :
            stage = self.env.ref('crm.stage_lead8')
            if stage:
                sale.opportunity_id.write({
                    'stage_id' : stage.id,
                    'automatic_stage_trigger' : True,
                })
        return sale

    @api.multi
    def write(self, vals):
        res = super(SaleOrder, self).write(vals)
        for sale in self :
            if 'sent_doc' in vals :
                sale.write({'state':'accepted'})
            if 'customer_po' in vals :
                sale.action_confirm()
        return res

    @api.multi
    def action_confirm(self):
        for sale in self :
            new_name = self.env['ir.sequence'].next_by_code('sale.order')
            if new_name :
                sale.write({'name':new_name})
            if sale.opportunity_id :
                stage = self.env.ref('crm.stage_lead4')
                if stage:
                    sale.opportunity_id.write({
                        'stage_id' : stage.id,
                        'automatic_stage_trigger' : True,
                    })
        return super(SaleOrder, self).action_confirm()

    @api.multi
    def action_failed_form(self):
        return {
            'name': 'Failed Reason',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'failed.reason.wizard',
            'target': 'new',
            'context': {},
        }

    @api.multi
    def action_request(self):
        for sale in self :
            if sale.state != 'draft' :
                continue
            if sale.opportunity_id :
                if not sale.opportunity_id.team_id.user_id :
                    raise Warning("Silahkan input PTI Division beserta Channel Leadernya, untuk opportunity %s"%(sale.opportunity_id.name))
                else :
                    sale.message_post(
                        body='Sale order %s sedang menunggu approval manager' % (sale.name),
                        partner_ids=sale.opportunity_id.team_id.user_id.partner_id.ids
                    )
            sale.write({'state':'waiting'})

    @api.multi
    def action_approve(self):
        for sale in self :
            if sale.state not in ('waiting','approved_manager'):
                continue
            if sale.state == 'waiting' :
                if sale.opportunity_id :
                    if not sale.opportunity_id.team_id.user_id :
                        raise Warning("Silahkan input PTI Division beserta Channel Leadernya, untuk opportunity %s"%(sale.opportunity_id.name))
                    if sale.opportunity_id.team_id.user_id != self.env.user :
                        raise Warning("Hanya manager divisi %s yang bisa approve."%(sale.opportunity_id.team_id.name))
                    buy_sell_id = self.env.ref('crm_dev.buy_sell')
                    if self.opportunity_id.type_id.id == buy_sell_id.id :
                        usd_id = self.env.ref('base.USD')
                        if sale.currency_id != usd_id :
                            amount = sale.currency_id.compute(sale.amount_total, usd_id)
                        else :
                            amount = sale.amount_total
                        if amount <= 20000 :
                            sale.write({'state':'approved'})
                        else :
                            sale.write({'state':'approved_manager'})
                            group_id = self.env.ref('crm_dev.group_director')
                            partner_ids = group_id.mapped('users.partner_id.id')
                            sale.message_post(
                                body='Sale order %s sedang menunggu approval director' % (sale.name),
                                partner_ids=partner_ids
                            )
                    else :
                        sale.write({'state':'approved_manager'})
                        group_id = self.env.ref('crm_dev.group_director')
                        partner_ids = group_id.mapped('users.partner_id.id')
                        sale.message_post(
                            body='Sale order %s sedang menunggu approval director' % (sale.name),
                            partner_ids=partner_ids
                        )
                else :
                    sale.write({'state':'approved'})
            else :
                sale.write({'state':'approved'})

    @api.multi
    def action_reject(self):
        for sale in self :
            if sale.state not in ('waiting','approved_manager'):
                continue
            if sale.state == 'waiting' :
                if sale.opportunity_id :
                    if not sale.opportunity_id.team_id.user_id :
                        raise Warning("Silahkan input PTI Division beserta Channel Leadernya, untuk opportunity %s"%(sale.opportunity_id.name))
                    if sale.opportunity_id.team_id.user_id != self.env.user :
                        raise Warning("Hanya manager divisi %s yang bisa reject."%(sale.opportunity_id.team_id.name))
            sale.action_cancel()

    @api.multi
    def print_quotation(self):
        return self.env.ref('sale.action_report_saleorder').report_action(self)

    @api.onchange('opportunity_id')
    def opportunity_assign_value(self):
        if 'default_opportunity_id' in self._context :
            opportunity_id = self.env['crm.lead'].browse(self._context['default_opportunity_id'])
            order_line_vals = []
            lead_time = 0
            if opportunity_id.transaction_method_id.lead :
                lead_time += opportunity_id.transaction_method_id.lead
            if opportunity_id.delivery_location_id.lead :
                lead_time += opportunity_id.delivery_location_id.lead
            for line in opportunity_id.lead_line_ids :
                order_line_vals.append((0,0,{
                    'product_id': line.product_id.id,
                    'name': line.product_id.name_get()[0][1],
                    'product_uom_qty': 1,
                    'product_uom': line.product_id.uom_id.id,
                    'price_unit': line.mcf_revenue,
                    'purchase_price': sum(abs(mcf_line.amount_signed) for mcf_line in line.mcf_line_ids.filtered(lambda l: l.subject_id.type == 'cost')),
                    'customer_lead': lead_time,
                }))
            self.order_line = order_line_vals

    @api.multi
    def action_quotation_send(self):
        res = super(SaleOrder, self).action_quotation_send()
        if self._context.get('proforma', False) and not self.proforma_number :
            self.proforma_number = self.env['ir.sequence'].next_by_code('sale.order.proforma')
        return res

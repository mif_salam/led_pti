# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from odoo.exceptions import ValidationError

class McfSubject(models.Model):
    _name = 'crm.mcf.subject'
    _order = 'code'
    _rec_name = 'complete_name'

    @api.multi
    @api.depends('name', 'parent_id')
    def _compute_complete_name(self):
        for rec in self :
            rec.complete_name = '%s/%s'%(rec.parent_id.name or '', rec.name or '')

    name = fields.Char('Name')
    code = fields.Char('Code')
    type = fields.Selection([
            ('revenue', 'Revenue'),
            ('cost', 'Cost'),
        ], string='Type', default='cost')
    parent_id = fields.Many2one('crm.mcf.subject.category', 'Parent')
    complete_name = fields.Char("Full Name", compute='_compute_complete_name', store=True)

class McfSubjectCategory(models.Model):
    _name = 'crm.mcf.subject.category'

    name = fields.Char('Name')
    child_ids = fields.One2many('crm.mcf.subject', 'parent_id', 'Childs')

class McfLine(models.Model):
    _name = 'crm.mcf.line'

    @api.multi
    @api.depends('amount', 'subject_id')
    def _compute_amount_signed(self):
        for line in self:
            if line.subject_id.type == 'revenue':
                line.amount_signed = line.amount
            else:
                line.amount_signed = -1 * line.amount

    @api.multi
    @api.depends('lead_line_id.mcf_revenue')
    def _compute_percentage(self):
        for line in self:
            if line.lead_line_id.mcf_revenue <= 0:
                return True
            line.percentage_to_revenue = 100 * line.amount / line.lead_line_id.mcf_revenue
    
    lead_line_id = fields.Many2one('crm.lead.line')
    subject_id = fields.Many2one('crm.mcf.subject', 'Subject')
    code = fields.Char('Code', related='subject_id.code', readonly=True)
    amount = fields.Float('Budget')
    percentage_to_revenue = fields.Float('%', compute='_compute_percentage', compute_sudo=True, store=True)
    amount_signed = fields.Float('Total', compute='_compute_amount_signed', compute_sudo=True, store=True)

class CrmLeadLine(models.Model):
    _inherit = "crm.lead.line"

    @api.multi
    @api.depends('mcf_line_ids', 'mcf_line_ids.amount')
    def _compute_mcf(self):
        for rec in self:
            net = 0
            rev = 0
            for line in rec.mcf_line_ids:
                if line.subject_id.type == 'revenue':
                    rev += line.amount_signed
                net += line.amount_signed
            rec.mcf_revenue = rev
            rec.mcf_net = net
            rec.price_unit = rev

    mcf_line_ids = fields.One2many('crm.mcf.line', 'lead_line_id', 'MCF Details')
    mcf_revenue = fields.Float(compute='_compute_mcf',
        string='Revenue',
        compute_sudo=True,
        store=True)
    mcf_net = fields.Float(compute='_compute_mcf',
        string='Net Profit',
        compute_sudo=True,
        store=True)

    @api.one
    def generate_new_mcf_lines(self):
        existing_subject_ids = []
        for line in self.mcf_line_ids:
            existing_subject_ids.append(line.subject_id.id)
        subjects = self.env['crm.mcf.subject'].sudo().search([('id', 'not in', existing_subject_ids)])
        new_lines = []
        for subject in subjects:
            new_lines.append((0, 0, {
                'subject_id': subject.id,
            }))
        self.mcf_line_ids = new_lines

        stage_profiling = self.env.ref('crm.stage_lead3')
        if self.lead_id and self.lead_id.stage_id == stage_profiling :
            stage = self.env.ref('crm.stage_lead5')
            if stage:
                self.lead_id.write({
                    'stage_id' : stage.id,
                    'automatic_stage_trigger' : True,
                }).with_context({'automatic_stage_trigger': True})

    @api.multi
    def write(self, vals):
        return super(CrmLeadLine, self).write(vals)
from odoo import fields, api, models, _

class CrmTeam(models.Model):
    _inherit = 'crm.team'

    day_close = fields.Integer(string='Automatic Close After')
    
from odoo import fields, api, models, _
from odoo.exceptions import Warning

STAGE_LISTS = [
    ['stage_lead1','New'],
    ['stage_lead2','Customer Follow-up'],
    ['stage_lead3','Customer Profiling Check'],
    ['stage_lead5','MCF Process'],
    ['stage_lead6','Waiting for Review'],
    ['stage_lead7','Quotation Preparation'],
    ['stage_lead8','Quotation Approval'],
    ['stage_lead9','Waiting for Client'],
    ['stage_lead4','Won'],
    ['stage_lead10','No Bid'],
    ['stage_lead11','Closed'],
]

class CrmStage(models.Model):
    _inherit = 'crm.stage'

    @api.model
    def _change_external_id(self):
        for STAGE_LIST in STAGE_LISTS :
            external, stage = STAGE_LIST
            stage_id = self.search([('name','=',stage)], limit=1)
            print("\n stage_id",stage_id)
            if stage_id :
                model_data_id = self.env['ir.model.data'].search([
                    ('model','=','crm.stage'),
                    ('res_id','=',stage_id.id),
                ], limit=1)
                if model_data_id :
                    model_data_id.write({
                        'name': external,
                        'module': 'crm',
                    })

    def get_external_id(self):
        model_data_id = self.env['ir.model.data'].search([
            ('model','=','crm.stage'),
            ('module','=','crm'),
            ('res_id','=',self.id)
        ], limit=1)
        return model_data_id and model_data_id.name or ''

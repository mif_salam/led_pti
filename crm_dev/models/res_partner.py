from odoo import fields, api, models, _
from odoo.exceptions import Warning

class ResPartner(models.Model):
    _inherit = 'res.partner'

    code = fields.Char(string='Code')
    
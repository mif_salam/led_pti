# -*- coding: utf-8 -*-

from . import crm_stage
from . import crm_lead
from . import quotation
from . import crm_mcf
from . import res_partner
from . import crm_team
# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
from odoo.addons import decimal_precision as dp

class TransactionType(models.Model):
    _name = 'crm.transaction.type'

    name = fields.Char('Name')

class TransactionSubType(models.Model):
    _name = 'crm.transaction.subtype'

    name = fields.Char('Name')
    type_id = fields.Many2one('crm.transaction.type', 'Type')

class CrmLead(models.Model):
    _inherit = "crm.lead"

    type_id = fields.Many2one('crm.transaction.type', 'Type')
    subtype_id = fields.Many2one('crm.transaction.subtype', 'Subtype')
    buyer_id = fields.Many2one('res.partner', 'Buyer / End User', domain=[('customer', '=', True)])
    inquiry_date_1 = fields.Date('Inquiry Date')
    buyer = fields.Boolean('Buyer / End User ?')
    partner_pic = fields.Char('Customer PIC Name')
    partner_email = fields.Char('Customer PIC Email')
    #transaction_type = fields.Selection([('BSS','1.1 Buy & Sell - Small'),('BSM','1.2 Buy & Sell - Medium'),('BSL','1.3 Buy & Sell - Large'),('BSXL','1.4 Buy & Sell - Extra Large'),('SNew','2.1 Solutions - New'),('SMod','2.2 Solutions - Modification / Upgrade'),('SvcInstal','3.1 Services - Installation'),('SvcComm','3.2 Services - Commissioning'),('SvcStart','3.3 Services - Start Up'),('SvcCert','3.4 Services - Certification'),('SvcCall','3.5 Services - Call Out'),('SvcOM','3.6 Services - O & M'),('Rental','4. Rental'),('Agency','5. Agency ')],'Type',default='BSS')
    customer_inq_ref = fields.Char('Customer Ref No')
    inquiry_delivery_date = fields.Date('Expected Delivery Date')

    decline_letter = fields.Binary('Decline Letter')
    decline_letter_name = fields.Char('Decline Letter Name')
    acceptance_letter = fields.Binary('Acceptance Letter')
    acceptance_letter_name = fields.Char('Acceptance Letter Name')
    stage_name = fields.Char('Stage Name', related='stage_id.name')
    email_sent = fields.Boolean('Email Sent')
    transaction_method_id = fields.Many2one('transaction.method', 'Transaction Method')
    delivery_location_id = fields.Many2one('delivery.location', 'Delivery Location')
    sale_amount_total = fields.Monetary(compute='_compute_sale_amount_total', string="Sum of Orders", help="Untaxed Total of Confirmed Orders", currency_field='company_currency')
    sale_number = fields.Integer(compute='_compute_sale_amount_total', string="Number of Quotations")
    available_credit = fields.Float(string='Available Credit', related='partner_id.available_credit', store=True, digits=dp.get_precision('Price Unit'))
    credit_limit = fields.Float(string='Credit Limit', related='partner_id.credit_limit', store=True, digits=dp.get_precision('Price Unit'))
    close_date = fields.Datetime(string='Closed Date', copy=False, readonly=True)

    @api.depends('order_ids')
    def _compute_sale_amount_total(self):
        for lead in self:
            total = 0.0
            nbr = 0
            company_currency = lead.company_currency or self.env.user.company_id.currency_id
            for order in lead.order_ids:
                if order.state not in ('sale', 'done', 'cancel'):
                    nbr += 1
                if order.state in ('sale', 'done'):
                    total += order.currency_id.compute(order.amount_untaxed, company_currency)
            lead.sale_amount_total = total
            lead.sale_number = nbr


    @api.one
    def action_bid(self):
        stage_review = self.env.ref('crm.stage_lead6')
        if self.stage_id == stage_review :
            stage = self.env.ref('crm.stage_lead7')
            if stage:
                self.write({
                    'stage_id' : stage.id,
                    'automatic_stage_trigger' : True,
                })
    @api.one
    def action_no_bid(self):
        stage_review = self.env.ref('crm.stage_lead6')
        if self.stage_id == stage_review :
            stage = self.env.ref('crm.stage_lead10')
            if stage:
                self.write({
                    'stage_id' : stage.id,
                    'automatic_stage_trigger' : True,
                })

    @api.multi
    def write(self, vals):
        # TODO: Still harcode the stage name. Better to change it to external ID
        if vals.get('stage_id') and not (vals.get('automatic_stage_trigger', False)):
            stage = self.env['crm.stage'].browse(vals['stage_id'])
            external_id = stage.get_external_id()
            if stage.name == 'Customer Follow-up':
                raise ValidationError('Silakan membuat meeting dan schedule untuk masuk ke dalam stage '+stage.name)
            # TODO: Masih NgeBug yang MCF Process
            # if stage.name == 'MCF Process':
            #     raise ValidationError('Silakan membuat detail MCF setiap produk untuk masuk ke dalam stage '+stage.name)
            if stage.name == 'No Bid (End)':
                raise ValidationError('Silahkan menunggu Division Manager melakukan review terhadap inquiry')
            if stage.name == 'Quotation Preparation':
                raise ValidationError('Silahkan menunggu Division Manager melakukan review terhadap inquiry')
            if stage.name == 'Quotation Approval':
                raise ValidationError('Silahkan mensubmit Quotation untuk approval')
            if stage.name == 'Quotation on Client':
                raise ValidationError('Silahkan mengirim Quotation kepada client')

            # Must go to the next stage
            next_stage = self.stage_id.search([('sequence', '>', self.stage_id.sequence)], limit=1)
            if next_stage.id == vals['stage_id'] or external_id == 'stage_lead11' :
                if external_id == 'stage_lead11' :
                    vals['close_date'] = fields.Datetime.now()
                return super(CrmLead, self).write(vals)
            else :
                raise ValidationError('Hanya bisa mengubah inquiry ke stage selanjutnya')
        return super(CrmLead, self).write(vals)

    @api.model
    def create(self, vals):
        if 'sequence_name' in vals:
            vals['sequence_name'] = self.env['ir.sequence'].next_by_code('crm.lead.inquiry.sequence')

        res = super(CrmLead, self).create(vals)
        return res

    @api.multi
    def action_send(self):
        template = self.env.ref('crm_dev.inquiry_email_template')
        template.send_mail(self.id, force_send=True)
        self.write({'email_sent': True})

    def automatic_close_lead(self):
        stage_ids = []
        stage_close = self.env.ref('crm.stage_lead11')
        stage_ids.append(stage_close.id)
        stage_no_bid = self.env.ref('crm.stage_lead10')
        stage_ids.append(stage_no_bid.id)
        lead_ids = self.search([
            ('team_id.day_close','!=',0),
            ('type','=','lead'),
            ('stage_id','not in',stage_ids),
        ])
        for lead in lead_ids :
            max_date = datetime.strptime(lead.write_date, '%Y-%m-%d %H:%M:%S') + timedelta(days=lead.team_id.day_close)
            if max_date <= datetime.now():
                lead.write({'stage_id':stage_close.id})
    
class CalendarEvent(models.Model):
    _inherit = 'calendar.event'

    @api.model
    def create(self, vals):
        event = super(CalendarEvent, self).create(vals)
        stage_new = self.env.ref('crm.stage_lead1')
        if event.opportunity_id and event.opportunity_id.stage_id == stage_new :
            stage = self.env.ref('crm.stage_lead2')
            if stage:
                event.opportunity_id.write({
                    'stage_id' : stage.id,
                    'automatic_stage_trigger' : True,
                })
        return event

class TransactionMethod(models.Model):
    _name = "transaction.method"
    _description = "Transaction Method"

    name = fields.Char(string='Transaction Method', required=True)
    lead = fields.Integer('Lead Time', required=True, default=0)

    @api.multi
    def name_get(self):
        result = []
        for me_id in self :
            result.append((me_id.id, "%s (%d days)"%(me_id.name, me_id.lead)))
        return result

class DeliveryLocation(models.Model):
    _name = "delivery.location"
    _description = "Delivery Location"

    name = fields.Char(string='Delivery Location', required=True)
    lead = fields.Integer('Lead Time', required=True, default=0)

    @api.multi
    def name_get(self):
        result = []
        for me_id in self :
            result.append((me_id.id, "%s (%d days)"%(me_id.name, me_id.lead)))
        return result

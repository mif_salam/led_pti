# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 DevIntelle Consulting Service Pvt.Ltd. (<http://devintellecs.com>).
#
##############################################################################
{
    "name": "Employee Advance Expenses Request",
    "category": 'Generic Modules/Human Resources',
    "summary": """
                 odoo Apps will manage Employee Advance Expenses request flow
        """,
    "description": """
        odoo Apps will manage Employee Advance Expenses request flow
        
        Advance Expense, hr Expenses, Employee Expenses, employee advance Expenses, employee request, employee advance expense request, 
        hr expense request, hr advance epense request,  
        
Employee Advance Expenses Request
Advance expenses request
Advance expense request odoo
Employee advance expenses request odoo
Create advance expenses request 
Create employee advance expenses request
Employee Expense Multiple Request
Employee expense multiple request odoo
Create employee expense multiple request
Create employee expense multiple request odoo
Expense request
Expense request odoo
Create expense request
Expense Advance Request
Create expense request odoo
Expense advance request odoo
Expense advance request employee
Expense advance request employee odoo
Create Expenses/Expense Advances
Expenses/Expense Advances/Expense Advance Requests
Expenses/Expense Advances/Advance to Approve
Expenses/Expense Advances/Advance to Pay
Create Expenses/Expense Advances/Expense Advance Requests
Create Expenses/Expense Advances/Advance to Approve
Create Expenses/Expense Advances/Advance to Pay
Expenses/expense advance
Expenses/expense advance odoo
create advance request for expenses        
        
        
    """,
    "sequence": 1,
    "author": "DevIntelle Consulting Service Pvt.Ltd",
    "website": "http://www.devintellecs.com",
    "version": '1.0',
    "depends": ['hr_expense'],
    "data": [
        'security/ir.model.access.csv',
        'security/expenses_security.xml',
        'views/ir_sequence_data.xml',
        'views/dev_advance_expenses.xml',
        'views/hr_expense.xml',
    ],
    "installable": True,
    "application": True,
    "auto_install": False,
    'images': ['images/main_screenshot.png'],
    'price':25.0,
    'currency':'EUR',   
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

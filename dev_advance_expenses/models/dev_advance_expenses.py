# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 DevIntelle Consulting Service Pvt.Ltd (<http://devintellecs.com>).
#
##############################################################################
#
from odoo import models, fields, api, _
import odoo.addons.decimal_precision as dp
from datetime import datetime, timedelta
from odoo.exceptions import ValidationError
import sys
from datetime import datetime
from time import mktime, strptime


class dev_advance_expenses(models.Model):
    _name = 'dev.advance.expenses'
    _inherit = 'mail.thread'
    _order='name desc'
    
    
    @api.depends('adv_exp_line_ids')
    def get_total_amount(self):
        for exp in self:
            amount = 0
            for line in exp.adv_exp_line_ids:
                amount += line.subtotal
            exp.amount = amount
    
    name = fields.Char('Name', default='/', copy=False)
    employee_id = fields.Many2one('hr.employee','Employee',required="1")
    date = fields.Date('Request Date', required="1",default=fields.Datetime.now, copy=False)
    amount = fields.Float(compute='get_total_amount',string='Request Amount', required="1",copy=False, readonly="1")
    confirm_date = fields.Date('Confirm Date',)
    comfirm_manager = fields.Many2one('res.users','Confirm Manager',track_visibility='onchange')
    reason = fields.Text('Reason', placeholder='Reason')
    user_id = fields.Many2one('res.users','User',default=lambda self: self.env.user,copy=False)
    state= fields.Selection([('draft','Draft'),('request','Wating For Hr Confirm'),('hr_confirm','HR Confirm'),('reject','Rejected'),('submit','Expenses Submit'),('cancel','Cancel')], string='State', default='draft',track_visibility='onchange')
    
    adv_exp_line_ids = fields.One2many('dev.advance.exp.line','adv_exp_id',string='Expense Lines')
    
    @api.depends()
    def count_hr_expense(self):
        for exp in self:
            expense_ids = self.env['hr.expense'].search([('advance_exp_id','=',self.ids[0])])
            if expense_ids:
                exp.count_expense = len(expense_ids)
            else:
                exp.count_expense = 0
    
    count_expense = fields.Integer(compute='count_hr_expense',string='Expenses')
    
    @api.multi
    def _make_url(self,model='dev.advance.expenses'):
        base_url = self.env['ir.config_parameter'].get_param('web.base.url', default='http://localhost:8069')
        if base_url:
            base_url += '/web/login?db=%s&login=%s&key=%s#id=%s&model=%s' % (self._cr.dbname, '', '',self.id, model)
        return base_url
    
    @api.multi
    def copy(self, default=None):
        if default is None:
            default = {}
        default['name'] = '/'
        return super(dev_advance_expenses, self).copy(default=default)

    @api.multi
    def set_to_draft(self):
        self.state = 'draft'
        
    @api.multi
    def set_to_cancel(self):
        self.state = 'cancel'

    
    @api.multi
    def unlink(self):
        for obj in self:
            if obj.state != 'draft':
                raise ValidationError(_('Expenses Delete in Draft State !!!'))
        
        return super(dev_advance_expenses,self).unlink()
                
    @api.model
    def create(self, vals):
        if vals.get('name', '/') == '/':
            vals['name'] = self.env['ir.sequence'].next_by_code(
                'dev.advance.expenses') or '/'
        return super(dev_advance_expenses, self).create(vals)
        
    
    def send_request(self):
        mail_mail = self.env['mail.mail']
        group_id = self.env['ir.model.data'].get_object_reference('hr_expense', 'group_hr_expense_manager')[1] 
        group_ids = self.env['res.groups'].browse(group_id)
        partner_pool = self.env['res.partner']
        partner_ids=[]
        for user in group_ids.users:
            partner_ids.append(user.partner_id.id)
        subject  =  self.employee_id.name + '-' + 'Advance Expenses Confirmation'
        if partner_ids:
            url = self._make_url()  
            for partner in partner_pool.browse(partner_ids):
                if partner.email:
                    body = '''
                        Dear ''' " <b>%s</b>," % (partner.name) + '''
                        <p></p>
                        <p> Employee ''' "<b>%s</b>" % self.employee_id.name + '''  require your Confirmation for Advance Expenses.</p> 
                        <p></p>
                        <p><b>Reason:</b> '''"%s" % self.reason+'''</p>
                        <p></p>
                        <p>Please action it accordingly</p> 
                        <p> </p>
                        <p>You can access Advance Salary Request  from  below url </p>
                        <p>''' "%s" % url +''' </p> 
                        
                        <p>Regards, </p> 
                        <p>''' "<b>%s</b>" % self.env.user.name +''' </p> 
                        ''' 
                    mail_values = {
                        'email_from': self.env.user.email,
                        'email_to': partner.email,
                        'subject': subject,
                        'body_html': body,
                        'state': 'outgoing',
                        'type': 'email',
                    }
                    mail_id = mail_mail.create(mail_values)
                    mail_id.send(True)
        self.state = 'request'
        return True
        
    def confirm_request(self):
        import datetime
        mail_mail = self.env['mail.mail']
        partner_id = False
        if self.employee_id:
            if self.employee_id.user_id and self.employee_id.user_id.partner_id:
                partner_id = self.employee_id.user_id.partner_id or False
        subject  =  self.employee_id.name + '-' + 'Advance Expenses Confirmed'
        if partner_id:
            url = self._make_url()  
            if partner_id.email:
                body = '''
                    Dear ''' " <b>%s</b>," % (partner_id.name) + '''
                    <p></p>
                    <p> Your Advance Expenses Request ''' "<b>%s</b>" % self.name + ''' is confirmed.</p> 
                    <p></p>
                    <p>Please action it accordingly</p> 
                    <p> </p>
                    <p>You can access Advance Expenses Request from below url </p>
                    <p>''' "%s" % url +''' </p> 
                    
                    <p>Regards, </p> 
                    <p>''' "<b>%s</b>" % self.env.user.name +''' </p> 
                    ''' 
                mail_values = {
                    'email_from': self.env.user.partner_id.email,
                    'email_to': partner_id.email,
                    'subject': subject,
                    'body_html': body,
                    'state': 'outgoing',
                    'type': 'email',
                }
                mail_id = mail_mail.create(mail_values)
                mail_id.send(True)
        self.state = 'hr_confirm'
        self.confirm_date = datetime.datetime.now().strftime('%Y-%m-%d')
        self.comfirm_manager = self.env.user.id
        return True
        
    def reject_request(self):
        mail_mail = self.env['mail.mail']
        subject  =  'Advance Expenses Request is reject'
        partner= self.user_id.partner_id
        if partner:
            url = self._make_url()  
            if partner.email:
                body = '''
                    Dear ''' " <b>%s</b>," % (partner.name) + '''
                    <p></p>
                    <p> Your Advance Expenses Request ''' "<b>%s</b>" % self.name + ''' is Reject.</p> 
                    <p></p>
                    <p>Please action it accordingly</p> 
                    <p> </p>
                    <p>You can access Advance Expenses Request from below url </p>
                    <p>''' "%s" % url +''' </p> 
                    
                    <p>Regards, </p> 
                    <p>''' "<b>%s</b>" % self.env.user.name +''' </p> 
                    ''' 
                mail_values = {
                    'email_from': self.env.user.partner_id.email,
                    'email_to': partner.email,
                    'subject': subject,
                    'body_html': body,
                    'state': 'outgoing',
                    'type': 'email',
                }
                mail_id = mail_mail.create(mail_values)
                mail_id.send(True)
        self.state = 'reject'
        return True
    
    @api.multi
    def submit_expenses(self):
        for line in self.adv_exp_line_ids:
            vals={
                'name':line.name or '',
                'product_id':line.product_id and line.product_id.id or False,
                'unit_amount':line.unit_price or 0.0,
                'quantity':line.qty or 1.0,
                'product_uom_id':line.product_uom_id and line.product_uom_id.id or False,
                'payment_mode':'own_account',
                'employee_id':self.employee_id and self.employee_id.id or False,
                'description':self.reason or '',
                'advance_exp_id':self.ids[0],
                'state':'draft',
            }
            new_id=self.env['hr.expense'].create(vals)
        self.state = 'submit'
            
    
class dev_advance_exp_line(models.Model):
    _name='dev.advance.exp.line'
    
    product_id = fields.Many2one('product.product',string='Product', required="1", domain="[('can_be_expensed','=',True)]")
    name = fields.Char('Description', required="1")
    unit_price =fields.Float('Price', required="1")
    qty = fields.Float(string='Quantity', required="1",default=1)
    product_uom_id = fields.Many2one('product.uom',string='UOM', required="1")
    subtotal = fields.Float(compute='get_subtotal',string='Subtotal')
    adv_exp_id = fields.Many2one('dev.advance.expenses',string='Advance Expenses')
    
    
    @api.onchange('product_id')
    def onchange_product_id(self):
        if self.product_id:
            self.name = self.product_id.name
            self.unit_price = self.product_id.lst_price
            self.product_uom_id = self.product_id.uom_id.id or False,
    
    @api.depends('qty','unit_price')
    def get_subtotal(self):
        for line in self:
            if line.qty and line.unit_price:
                line.subtotal = line.qty * line.unit_price
            else:
                line.subtotal = 0.00

class hr_expense(models.Model):
    _inherit = 'hr.expense'
    
    advance_exp_id = fields.Many2one('dev.advance.expenses',string='Advance Expense')
        
        
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
    
    
        

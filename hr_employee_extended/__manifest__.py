# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'HR Employee Extra Features',
    'version' : '1.0',
    'author':'Craftsync Technologies',
    'category': 'HR',
    'maintainer': 'Craftsync Technologies',
    'description': """This App is extend HR Features""",
    'summary': """This App has Extra Features in HR""",
    'website': 'https://www.craftsync.com/',
    'license': 'OPL-1',
    'support':'info@craftsync.com',
    'depends' : ['hr_contract'],
    'data': [

       'data/employee_family_relation.xml',
       'views/religion.xml',
       'views/document.xml',
       'views/perfomance.xml',
	     'views/employee.xml',
       'views/visa_insurance.xml',
       'views/family.xml',
       'views/injury.xml',
       'views/language.xml',
       'views/promotion.xml',
       'views/request_complaint.xml',
       'views/training.xml',
       'views/habit_hobby_disablity.xml',
       'security/ir.model.access.csv',
    ],    
    'installable': True,
    'application': True,
    'auto_install': False,
    'images': ['static/description/main_screen.png'],
    'price': 49.00,
    'currency': 'EUR',

}

from odoo import api, models, fields, _
from datetime import datetime
from dateutil import relativedelta,parser


class HREmployee(models.Model):
    _inherit = 'hr.employee'

    @api.multi
    @api.depends('birthday')
    def compute_age(self):
        for res in self:
            if res.birthday:
                calculated_date = relativedelta.relativedelta(datetime.today(), parser.parse(res.birthday))
                res.age = float(calculated_date.years)
            else:
                res.age = 0.0

    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('cohabitant', 'Legal Cohabitant'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Marital Status', default='single', groups=False)
    joining_date = fields.Date(string="Joining Date")
    employment_date = fields.Date(string="Employment Date")
    confirmation_date = fields.Date(string="Confirmation Date")
    marriage_date = fields.Date(string="Marriage Date")
    blood_group = fields.Selection([('o', 'O'), ('o', 'O'),
                                    ('a', 'A'), ('a', 'A'),
                                    ('b', 'B'), ('b', 'B'),
                                    ('ab', 'AB'), ('ab', 'AB')],
                                   string="Blood Group")
    positive_negative = fields.Selection([('-', '-ve'), ('+', '+ve')],string="Positive or Negative")
    age = fields.Float(compute=compute_age, string="Age")
    skype = fields.Char(string="Skype")
    habit_ids = fields.Many2many('employee.habits', string="Habits")
    hobbies_ids = fields.Many2many('employee.hobbies', string="Hobbies")
    pf_account_no = fields.Char(string="PF Account No.")
    height = fields.Float(string="Height")
    weight = fields.Float(string="Weight")
    height_uom_id = fields.Many2one('product.uom', string="UOM Height")
    weight_uom_id = fields.Many2one('product.uom', string="UOM Weight")
    disability_ids = fields.Many2many('employee.disability',
                                      string="Disability")
    work_country_id = fields.Many2one('res.country', string="Work Country")
    family_detail_ids = fields.One2many('employee.family', 'employee_id',
                                        string="Family Details")
    document_ids = fields.One2many('employee.document', 'employee_id',
                                   string="Documents")
    visa_ids = fields.One2many('employee.visa', 'employee_id', string="Visa Details")
    insurance_ids = fields.One2many('employee.insurance', 'employee_id',
                                    string="Insurance Details")
    religion_id = fields.Many2one('employee.religion', 'Religion')
    previous_country_id = fields.Many2one('res.country',
                                          string='Previous Nationality')
    facilities_ids = fields.Many2many('employee.facilities', string='Facilities By Company')
    speciality = fields.Char(string="Speciality")
    passport_country = fields.Many2one('res.country', string="Passport Country")
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ])
    marital = fields.Selection([
        ('single', 'Single'),
        ('married', 'Married'),
        ('cohabitant', 'Legal Cohabitant'),
        ('widower', 'Widower'),
        ('divorced', 'Divorced')
    ], string='Marital Status')
    religion_id = fields.Many2one("employee.religion", string="Religion")
    passport_issue_date = fields.Date(string="Passport Issue Date")
    passport_expiry_date = fields.Date(string="Passport Expiry Date")
    promotion_ids = fields.One2many("employee.promotion", "employee_id", string="Promotions")
    injury_ids = fields.One2many("employee.injury", "employee_id", string="Work Injury")
    award_ids = fields.One2many("employee.award", "employee_id", string="Awarding")
    complaint_ids = fields.One2many("employee.complaint", "employee_id", string="Complaints")
    request_ids = fields.One2many("employee.request", "employee_id", string="Requests")
    training_ids = fields.One2many("employee.training", "employee_id", string="Training")
    emp_lang_ids = fields.One2many("employee.language", "employee_id", string="Language")
    is_married = fields.Boolean('Is Married', default=False, compute='_compute_marital_stauts', store=True)
    history_ids = fields.One2many("employee.history", "employee_id", string="History")
    performance_ids = fields.One2many("employee.performance", "employee_id", string="Performance")
    is_part_time = fields.Boolean(string='Is Part Time?')
    reference_id = fields.Many2one('hr.employee',string='Reference By')

    @api.depends('marital')
    def _compute_marital_stauts(self):
        for rec in self:
            if rec.marital:
                if rec.marital  ==  'married':
                    rec.is_married = True
                
                else:
                    rec.is_married = False
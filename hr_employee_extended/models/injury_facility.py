from odoo import api, models, fields


class Injury(models.Model):
    _name = "employee.injury"
    _description = "Employee Injury"

    name = fields.Char(string="Name")
    employee_id = fields.Many2one("hr.employee", string="Employee")
    date = fields.Date(string="Date")
    ttype = fields.Char(string="Type")
    location = fields.Char(string="Location")
    description = fields.Char(string="Problem")
    is_during_work = fields.Boolean(string="During Work?")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.employee_id.company_id)

class Facilities(models.Model):
    _name = "employee.facilities"
    _description = "Employee Facilities"

    name = fields.Char(string="Name")
    description = fields.Char(string="Description")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)

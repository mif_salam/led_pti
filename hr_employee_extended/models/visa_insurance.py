from odoo import models, fields


class Visa(models.Model):
    _name = 'employee.visa'
    _description = "Employee Visa"

    name = fields.Char(string="Description", required=True)
    employee_id = fields.Many2one('hr.employee', string="Employee")
    from_date = fields.Date(string="From Date")
    to_date = fields.Date(string="To Date")
    country_id = fields.Many2one('res.country', string="Country")
    status = fields.Selection([('request', 'To Request'),
                               ('processing', 'In Process'),
                               ('approve', 'Approve'),
                               ('reject', 'Reject'),
                               ('expired', 'Expired')], default='request',
                              string="Status")
    no_of_entry = fields.Char(string="No. of Entries")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)
    type = fields.Selection([('employee', 'Employee'), ('family', 'Family')],
                            string="Type", default='employee')
    family_member_id = fields.Many2one('employee.family', string="Family Member")
    family_relation_id = fields.Many2one('employee.family.relation',
                                         string="Relation",
                                         related='family_member_id.family_relation_id')
    visa_type = fields.Many2one("employee.visa.type",string='Visa Type')

class VisaType(models.Model):
    _name = 'employee.visa.type'
    _description = "Employee Visa Type"

    name = fields.Char('Name', required=True)

class InsuranceType(models.Model):
    _name = 'employee.insurance.type'
    _description = "Employee Insurance Type"

    name = fields.Char('Name', required=True)


class Insurance(models.Model):
    _name = 'employee.insurance'
    _description = "Employee Insurance"

    name = fields.Char('Description')
    type = fields.Selection([
        ('employee', 'Employee'),
        ('family', 'Family')],
        string="Type", default='employee')
    employee_id = fields.Many2one('hr.employee', string="Employee")
    
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    insurance_type = fields.Many2one('employee.insurance.type', 'Insurance Type')
    amount = fields.Float('Amount', help='For insurance amount')
    status = fields.Selection([('request', 'To Request'),
                               ('running', 'Running'),
                               ('approve', 'Approve'),
                               ('reject', 'Reject'),
                               ('expired', 'Expired')], default='request',
                              string="Status")
    company_id = fields.Many2one('res.company',
                                 employee=lambda self: self.employee_id.company_id)
    currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')
    family_id = fields.Many2one('employee.family', string='Family Member')
    family_relation_id = fields.Many2one('employee.family.relation', 'Relation',related='family_id.family_relation_id')

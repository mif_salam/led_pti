from odoo import  models, fields

class PromotionType(models.Model):
    _name = "employee.promotion.type"
    _description = "Promotion Type"

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)


class PromotionCategory(models.Model):
    _name = "employee.promotion.category"
    _description = "Promotion Category"

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)



class Promotion(models.Model):
    _name = "employee.promotion"
    _description = "Employee Promotion"

    name = fields.Char(string="Name")
    employee_id = fields.Many2one("hr.employee", string="Employee")
    date = fields.Date(string="Date")
    previous_salary = fields.Float(string="Previous Salary")
    new_salary = fields.Float(string="New Salary")
    previous_designation = fields.Many2one('hr.job', string="Previous Job Position")
    new_designation = fields.Many2one('hr.job', string="New Job Position")
    promotion_type = fields.Many2one("employee.promotion.type", string="Promotion Type")
    appraisal = fields.Many2one("employee.appraisal", string="Appraisal")
    contract_id = fields.Many2one('hr.contract', string='Contract')
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')
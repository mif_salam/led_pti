
from odoo import models, fields

class Habits(models.Model):
    _name = 'employee.habits'
    _description = "Employee Habits"

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")

class Hobbies(models.Model):
    _name = 'employee.hobbies'
    _description = "Employee Hobbies"

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")

class Disability(models.Model):
    _name = 'employee.disability'
    _description = "Employee Disability"

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")

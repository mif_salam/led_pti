from odoo import  models, fields, api


class Performance(models.Model):
    _name = "employee.performance"
    _description = "Employee Performance"

    name = fields.Char(string="Name", default="New", copy=False)
    employee_id = fields.Many2one("hr.employee", string="Employee")
    start_date = fields.Date(string="Review Start Date")
    end_date = fields.Date(string="Review End Date")
    appraisal = fields.Many2one("employee.appraisal", string="Appraisal")
    reviewer = fields.Many2one('hr.employee', string="Review By")
    description = fields.Text(string="Description")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            if 'company_id' in vals:
                name = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code('employee.performance') or 'New'
            else:
                name = self.env['ir.sequence'].next_by_code('employee.performance') or 'New'            
            
            vals['name'] = name
        return super(Performance, self).create(vals)


class Appraisal(models.Model):
    _name = 'employee.appraisal'
    _description = "Employee Appraisal"

    name = fields.Char(string="Name")
    description = fields.Text(string="Description")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)
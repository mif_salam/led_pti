from odoo import api, models, fields
from dateutil.relativedelta import relativedelta

class EmployeeFamily(models.Model):
    _name = 'employee.family'
    _description = "Employee Family Details"

    name = fields.Char(string="Name", required=True)
    employee_id = fields.Many2one('hr.employee', string="Employee")
    family_relation_id = fields.Many2one('employee.family.relation',
                                         string="Relation")
    contact_no = fields.Char(string="Contact No.")
    partner_address_id = fields.Text(string="Address")
    nationality = fields.Many2one("res.country", string="Nationality")
    birth_date = fields.Date(string="Birth date")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.employee_id.company_id)
   
    age_of_child = fields.Float('Child Age')

    @api.onchange('birth_date', 'family_relation_id')
    def onchange_birth_date(self):
        if self.birth_date and self.family_relation_id.id == self.env.ref('hr_employee_extended.employee_relation_child').id:
            rd = relativedelta(fields.Date.today(), self.birth_date)
            self.child_age = rd.years


class EmployeeFamilyRelation(models.Model):
    _name = 'employee.family.relation'
    _description = "Employee Family Relation"

    name = fields.Char(string="Name")

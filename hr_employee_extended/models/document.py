from odoo import models, fields


class HrDocument(models.Model):
    _name = 'employee.document'
    _description = "Employee Documents"

    name = fields.Char(string="Name", required=True)
    employee_id = fields.Many2one('hr.employee', string="Employee")
    file = fields.Binary(string="File")
    file_name = fields.Char('File Name')
    document_type_id = fields.Many2one('document.type', string='Document Type')
    is_a_certificate = fields.Boolean(string="Certificate?")
    is_educational_degree = fields.Boolean(string="Educational Degree")
    is_professional_certificate = fields.Boolean(string="Professional Certificate")
    certificate_year = fields.Integer(string="Degree\Certificate Year")
    description = fields.Char(string="Description")
    issuer_country = fields.Many2one("res.country", string="Issuer Country")
    certificate_institute = fields.Char(string="Degree\Certificate Institute")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.employee_id.company_id)


class DocumentType(models.Model):
    _name = 'document.type'
    _description = "Documents Type"

    name = fields.Char(string="Name", required=True)
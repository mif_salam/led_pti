from odoo import models, fields


class Religion(models.Model):
    _name = 'employee.religion'
    _description = "Employee Religion"

    name = fields.Char(string="Name", required=True)
    description = fields.Text(string="Description")
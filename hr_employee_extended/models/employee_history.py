from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class EmployeeHistory(models.Model):
    _name = 'employee.history'
    _description = 'Employee History'

    employee_id = fields.Many2one("hr.employee", string="Employee")
    name = fields.Char(string='Previous Company')
    joining_date = fields.Date(string='Joining Date')
    end_date = fields.Date(string='End Date')
    reason_for_left = fields.Char(string='Reason For Left', help="Reason For Left Previous Company")
    salary = fields.Float(string='Salary')
    experience = fields.Integer(string='Experience')
    time = fields.Selection([
        ('days', 'Days'),
        ('months', 'Months'),
        ('years', 'Years'),
    ], string='Time', default='years')
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one('res.currency', string='Currency', related='company_id.currency_id')

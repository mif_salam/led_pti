from odoo import models, fields, api


class Request(models.Model):
    _name = "employee.request"
    _description = "Employee Request"

    name = fields.Char(string="Name", default='New', copy=False)
    employee_id = fields.Many2one("hr.employee", string="Employee")
    request_date = fields.Date(string="Request Date")
    status = fields.Selection([('request', 'To Request'),
                               ('running', 'Running'),
                               ('approve', 'Approve'),
                               ('reject', 'Reject'),
                               ('expired', 'Expired')], default='request',
                              string="Status")
    request_for = fields.Char(string='Request For')
    purpose = fields.Char(string="Purpose")
    action_by_company = fields.Char(string="Action Taken From Company")
    has_financial_effect = fields.Boolean(string="Has Financial Effect?")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            if 'company_id' in vals:
                name = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code('employee.request') or 'New'
            else:
                name = self.env['ir.sequence'].next_by_code('employee.request') or 'New'            
            
            vals['name'] = name
        return super(Request, self).create(vals)


class Complaint(models.Model):
    _name = "employee.complaint"
    _description = "Employee Complaint"

    name = fields.Char(string="Name", default='New', copy=False)
    employee_id = fields.Many2one("hr.employee", string="Employee")
    complaint_for = fields.Char(string='Complaint For')
    status = fields.Selection([('request', 'To Request'),
                               ('running', 'Running'),
                               ('solved', 'Solved'),
                               ('reject', 'Reject'),
                               ('expired', 'Expired')], default='request',
                              string="Status")
    action_by_company = fields.Char(string="Action Taken From Company")
    solution = fields.Char(string="Solution")
    has_effect_on_work = fields.Boolean(string="Has Effect On Work Effiency?")
    complaint_type = fields.Char(string="Complaint Type")
    company_id = fields.Many2one('res.company',
                                 default=lambda self: self.env.user.company_id)

    @api.model
    def create(self, vals):
        if vals.get('name', 'New') == 'New':
            if 'company_id' in vals:
                name = self.env['ir.sequence'].with_context(force_company=vals['company_id']).next_by_code('employee.complaint') or 'New'
            else:
                name = self.env['ir.sequence'].next_by_code('employee.complaint') or 'New'            
            
            vals['name'] = name
        return super(Complaint, self).create(vals)


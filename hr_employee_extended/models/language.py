from odoo import models, fields


class Language(models.Model):
    _name = 'employee.language'
    _description = "Employee Language"

    employee_id = fields.Many2one('hr.employee', string="Employee")
    language_id = fields.Many2one('res.lang', string="Language")
    can_read = fields.Boolean('Can Read ?')
    can_write = fields.Boolean('Can Write ?')
    can_speak = fields.Boolean('Can Speak ?')



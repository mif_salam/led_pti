from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.addons import decimal_precision as dp
from odoo.exceptions import Warning, ValidationError, UserError
import pytz
from pytz import timezone

STATE = [
    ('draft','Draft'),
    ('confirm','Confirmed'),
    ('done','Done'),
    ('cancel','Cancelled'),
]

class PurchaseOrderTransporter(models.Model):
    _name = "purchase.order.transporter"
    _description = "Purchase Order Transporter"
    _order = "id desc"
    
    @api.multi
    def get_default_date_multi(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))

    name = fields.Char(string='Name', required=True, default='New', copy=False)
    date = fields.Datetime('Date', default=datetime.now(), required=True)
    state = fields.Selection(STATE, string="State", default='draft')
    carrier_id = fields.Many2one("res.partner", string="Transporter", domain=[('supplier','=',True)])
    weight_total = fields.Float(string='Total Weight', digits=dp.get_precision('Stock Weight'))
    purchase_order_transporter_line = fields.One2many('purchase.order.transporter.line', 'order_id', string='Purchase Order Transporter')
    invoice_id = fields.Many2one("account.invoice", string="Invoice")
    ref = fields.Char(string='Ref Number')
    note = fields.Text(string='Note')
    carrier_price_total = fields.Monetary(string="Total Value", digits=dp.get_precision('Price Unit'))
    currency_id = fields.Many2one('res.currency', 'Currency')

    @api.model
    def create(self, vals):
        if vals.get('New', False) or 'name' not in vals :
            vals['name'] = self.env.ref('purchase_order_transporter.seq_po_transporter').next_by_id()
        res = super(PurchaseOrderTransporter, self).create(vals)
        return res

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft' :
                raise UserError(_('Can not delete data that is not Draft !'))
        return super(PurchaseOrderTransporter, self).unlink()

    @api.multi
    def action_confirm(self):
        for request in self :
            if not request.purchase_order_transporter_line :
                raise Warning(_("Please input detail."))
            picking_price = {}
            invoice_line_vals = []
            invoice_line = self.env['account.invoice.line']
            domain = [
                ('type', '=', 'purchase'),
            ]
            journal_id = self.env['account.journal'].search(domain, limit=1)
            if not journal_id :
                raise Warning(_("Vendor bill journal not define, please create one."))
            for line in request.purchase_order_transporter_line :
                picking_price[line.picking_id] = picking_price.get(line.picking_id, 0) + line.carrier_price
                invoice_line_vals.append((0,0,{
                    'name': line.name+': '+line.product_id.name,
                    'origin': request.name,
                    'uom_id': line.weight_uom_id.id,
                    'product_id': line.product_id.id,
                    'account_id': invoice_line.with_context({'journal_id': journal_id.id, 'type': 'in_invoice'})._default_account(),
                    'price_unit': line.carrier_price/line.weight if line.weight else 0,
                    'quantity': line.weight,
                    'discount': 0.0,
                }))
            invoice_id = self.env['account.invoice'].create({
                'partner_id': request.carrier_id.id,
                'date_invoice': request.date,
                'reference': request.ref,
                'currency_id': request.currency_id.id,
                'journal_id': journal_id.id,
                'user_id': self.env.user.id,
                'account_id': request.carrier_id.property_account_payable_id.id,
                'type': 'in_invoice',
                'invoice_line_ids': invoice_line_vals,
            })
            for picking, price in picking_price.items():
                picking.write({
                    'carrier_tracking_ref': request.ref,
                    'currency_id': request.currency_id.id,
                    'carrier_price': price,
                })
            invoice_id.action_invoice_open()
            request.write({
                'state': 'confirm',
                'invoice_id': invoice_id.id,
            })

    @api.multi
    def action_draft(self):
        for request in self :
            request.write({'state':'draft'})

    @api.multi
    def action_done(self):
        for request in self :
            request.write({'state':'done'})

    @api.multi
    def action_cancel(self):
        for request in self :
            request.write({'state':'cancel'})

    @api.multi
    def action_change(self):
        wizard_line = []
        picking_ids = self.purchase_order_transporter_line.mapped('picking_id')
        for picking_id in picking_ids :
            wizard_line.append((0,0,{
                'picking_id': picking_id.id,
                'weight': picking_id.weight,
                'weight_uom_id': picking_id.weight_uom_id.id,
            }))
        return {
            'name': 'Update PO Transporter',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.order.transporter.wizard',
            'target': 'new',
            'context': {
                'default_carrier_id': self.carrier_id.id,
                'default_ref': self.ref,
                'default_carrier_price': self.carrier_price_total,
                'default_currency_id': self.currency_id.id,
                'default_order_id': self.id,
                'default_wizard_line': wizard_line,
            },
        }

class PurchaseOrderTransporterLine(models.Model):
    _name = "purchase.order.transporter.line"
    _description = "Purchase Order Transporter Line"
    _rec_name = 'picking_id'

    order_id = fields.Many2one('purchase.order.transporter', 'Purchase Order Transporter', ondelete='cascade', index=True)
    move_id = fields.Many2one('stock.move', 'Move')
    picking_id = fields.Many2one('stock.picking', 'Transfer Reference', related='move_id.picking_id')
    product_id = fields.Many2one('product.product', 'Product', related='move_id.product_id')
    name = fields.Text(string='PO/SO Reference')
    qty = fields.Float('Qty', digits=dp.get_precision('Product Unit of Measure'), related='move_id.product_uom_qty')
    weight = fields.Float(string='Weight', related='move_id.weight', digits=dp.get_precision('Stock Weight'), store=True)
    weight_uom_id = fields.Many2one('product.uom', 'UoM', related='move_id.weight_uom_id', store=True)
    carrier_price = fields.Monetary(string="Shipping Cost", digits=dp.get_precision('Price Unit'))
    currency_id = fields.Many2one('res.currency', 'Currency')

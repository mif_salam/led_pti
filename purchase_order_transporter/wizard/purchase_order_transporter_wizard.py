from odoo import fields, api, models, _
from odoo.exceptions import Warning, UserError
from odoo.addons import decimal_precision as dp

class PurchaseOrderTransporterWizard(models.TransientModel):
    _name = "purchase.order.transporter.wizard"
    _description = "Purchase Order Transporter Wizard"

    @api.multi
    @api.depends('wizard_line.weight')
    def _compute_weight(self):
        for me_id in self :
            weight_total = 0
            for line in me_id.wizard_line :
                weight_total += line.weight
            me_id.weight_total = weight_total

    carrier_id = fields.Many2one("res.partner", string="Transporter", domain=[('supplier','=',True)])
    ref = fields.Char(string='Ref Number')
    weight_total = fields.Float(string='Total Weight', compute='_compute_weight', digits=dp.get_precision('Stock Weight'), store=True)
    wizard_line = fields.One2many('purchase.order.transporter.line.wizard', 'wizard_id', string='Purchase Order Transporter Wizard')
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id, required=True)
    carrier_price = fields.Monetary(string="Total Value", digits=dp.get_precision('Stock Weight'), required=True)
    order_id = fields.Many2one('purchase.order.transporter', 'Purchase Order Transporter')

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        if not self.wizard_line :
            raise Warning(_("Please input detail."))
        if self.order_id :
            order_id = self.order_id
            order_id.write({
                'carrier_id': self.carrier_id.id,
                'ref': self.ref,
                'carrier_price_total': self.carrier_price,
                'weight_total': self.weight_total,
                'currency_id': self.currency_id.id,
            })
            order_id.purchase_order_transporter_line.unlink()
            for line in self.wizard_line :
                for move in line.picking_id.move_lines :
                    carrier_price = 0
                    if line.wizard_id.carrier_price and line.wizard_id.weight_total :
                        unit_price = line.wizard_id.carrier_price / line.wizard_id.weight_total
                        carrier_price = move.weight * unit_price
                    order_id.purchase_order_transporter_line.create({
                        'order_id': order_id.id,
                        'move_id': move.id,
                        'name': move.purchase_line_id.order_id.name or move.sale_line_id.order_id.name,
                        'carrier_price': carrier_price,
                        'currency_id': self.currency_id.id,
                    })
        else :
            line_vals = []
            for line in self.wizard_line :
                for move in line.picking_id.move_lines :
                    carrier_price = 0
                    if line.wizard_id.carrier_price and line.wizard_id.weight_total :
                        unit_price = line.wizard_id.carrier_price / line.wizard_id.weight_total
                        carrier_price = move.weight * unit_price
                    line_vals.append((0, 0, {
                        'move_id': move.id,
                        'name': move.purchase_line_id.order_id.name or move.sale_line_id.order_id.name,
                        'currency_id': self.currency_id.id,
                        'carrier_price': carrier_price,
                    }))
            order_id = self.env['purchase.order.transporter'].create({
                'carrier_id': self.carrier_id.id,
                'ref': self.ref,
                'weight_total': self.weight_total,
                'carrier_price_total': self.carrier_price,
                'currency_id': self.currency_id.id,
                'purchase_order_transporter_line': line_vals,
            })
            action = self.env.ref('purchase_order_transporter.purchase_order_transporter_action')
            action = action.read()[0]
            view_id = self.env.ref('purchase_order_transporter.purchase_order_transporter_form_view')
            action.update({
                'res_id': order_id.id,
                'views': [(view_id.id, 'form')],
                'view_id': view_id.id,
            })
            return action

class PurchaseOrderTransporterLineWizard(models.TransientModel):
    _name = "purchase.order.transporter.line.wizard"
    _description = "Purchase Order Transporter Line Wizard"
    _rec_name = "picking_id"

    wizard_id = fields.Many2one('purchase.order.transporter.wizard', 'Wizard')
    picking_id = fields.Many2one('stock.picking', 'Transfer Reference', required=True)
    weight = fields.Float(string='Weight', digits=dp.get_precision('Stock Weight'))
    weight_uom_id = fields.Many2one('product.uom', 'UoM')

    @api.onchange('picking_id','weight','weight_uom_id')
    def change_picking(self):
        if self.picking_id :
            self.weight = self.picking_id.weight
            self.weight_uom_id = self.picking_id.weight_uom_id.id

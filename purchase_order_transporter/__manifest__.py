# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Purchase Order Transporter',
    'version'       : '0.1',
    'summary'       : 'PO to transporter and breakdown cost per item',
    'author'        : 'Arkana',
    'description'   : """
Features :

- 
    """,
    'depends'       : [
        'stock_dev',
        'purchase_dev',
        'sale_dev',
    ],
    'data': [
        'views/purchase_order_transporter_view.xml',
        'wizard/purchase_order_transporter_wizard.xml',
        'data/ir_sequence.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Document Checklist',
    'version'       : '0.1',
    'summary'       : 'Document Checklist Module for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- Document checklist in some form
    """,
    'depends'       : [
        'base',
        'project',
    ],
    'data': [
        'views/document_checklist_view.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
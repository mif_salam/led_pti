from odoo import fields, api, models, _
from odoo.exceptions import Warning

class DocumentChecklistType(models.Model):
    _name = "document.checklist.type"
    _description = "Document Checklist Type"

    name = fields.Char(string='Document Name', required=True)
    type = fields.Char(string='Type')

    @api.multi
    def name_get(self):
        result = []
        for me_id in self :
            if me_id.type :
                result.append((me_id.id, "%s - %s"%(me_id.name, me_id.type)))
            else :
                result.append((me_id.id, me_id.name))
        return result

class DocumentChecklist(models.Model):
    _name = "document.checklist"
    _description = "Document Checklist"

    document_checklist_type_id = fields.Many2one('document.checklist.type', string='Document Checklist Type', required=True)
    data = fields.Binary(string='File', required=True)
    data_name = fields.Char(string='Filename')
    name = fields.Char(string='Reference')
    date = fields.Date(string='Date', default=fields.Date.context_today)

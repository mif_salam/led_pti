# See LICENSE file for full copyright and licensing details.

from odoo import fields, models, api, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class ResPartner(models.Model):
    _inherit = 'res.partner'

    @api.multi
    @api.depends('credit_limit','used_credit')
    def _get_available_credit(self):
        for partner in self :
            partner.available_credit = partner.credit_limit - partner.used_credit

    over_credit = fields.Boolean('Allow Over Credit?')
    available_credit = fields.Float(string='Available Credit', compute='_get_available_credit', store=True, digits=dp.get_precision('Price Unit'))
    used_credit = fields.Float(string='Used Credit', digits=dp.get_precision('Price Unit'))

    #custom by miftah
    @api.multi
    def check_limit(self, amount):
        self.ensure_one()
        if self.over_credit or self.available_credit >= amount :
            self.used_credit += amount
        else :
            if self.company_id.currency_id.position == 'before' :
                available_credit = '%s. %d'%(self.company_id.currency_id.symbol,self.available_credit)
            else :
                available_credit = '%d%s'%(self.available_credit,self.company_id.currency_id.symbol)
            raise Warning(_("Over credit. You can not confirm sale order or cancel payment.\n\nAvailable credit limit for %s = %s"%(self.display_name,available_credit)))
    
    @api.model
    def _set_initial_used_credit(self):
        partner_ids = self.search([('customer','=',True)])
        print("\n partner_ids",partner_ids)
        for partner in partner_ids :
            print("\n partner.used_credit",partner.used_credit)
            # if partner.used_credit :
            #     return False
            used_credit = 0
            order_ids = partner.sale_order_ids.filtered(lambda s: s.state in ('sale','done'))
            payment_ids = self.env['account.payment'].search([
                ('state','in',['posted','reconciled']),
                ('partner_id','=',partner.id),
            ])
            for order in order_ids :
                if order.pricelist_id.currency_id != self.env.user.company_id.currency_id :
                    sale_amount = order.pricelist_id.currency_id.compute(order.amount_total, self.env.user.company_id.currency_id)
                else :
                    sale_amount = order.amount_total
                used_credit += sale_amount
            for payment in payment_ids :
                if payment.currency_id != self.env.user.company_id.currency_id :
                    payment_amount = payment.currency_id.compute(payment.amount, self.env.user.company_id.currency_id)
                else :
                    payment_amount = payment.amount
                used_credit -= payment_amount
            partner.used_credit = used_credit

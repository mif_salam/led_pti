from odoo import fields, api, models, _
from odoo.exceptions import Warning

class AccountPayment(models.Model):
    _inherit = 'account.payment'

    @api.multi
    def post(self):
        res = super(AccountPayment, self).post()
        for payment in self :
            if payment.currency_id != self.env.user.company_id.currency_id :
                payment_amount = payment.currency_id.compute(payment.amount, self.env.user.company_id.currency_id)
            else :
                payment_amount = payment.amount
            payment.partner_id.write({'used_credit': payment.partner_id.used_credit-payment_amount})
        return res

    @api.multi
    def cancel(self):
        res = super(AccountPayment, self).cancel()
        for payment in self :
            if payment.currency_id != self.env.user.company_id.currency_id :
                payment_amount = payment.currency_id.compute(payment.amount, self.env.user.company_id.currency_id)
            else :
                payment_amount = payment.amount
            payment.partner_id.check_limit(payment_amount)
        return res

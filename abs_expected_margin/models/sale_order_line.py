# -*- coding: utf-8 -*-
#################################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2018-today Ascetic Business Solution <www.asceticbs.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################

from odoo import api,fields,models,_
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'   # Inherit this class because we have to add field
   
    current_margin = fields.Integer('Current Margin',help='show the current profit of product')
    expected_margin = fields.Integer('Expected Margin',help='please enter value if u want particular margin')
    current_margin_per = fields.Float('Current Margin %',help='current profit in percentage')
    expected_margin_per = fields.Float('Expected Margin %',help='expected profit in percentage')

    #define function on product_id change
    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        result = super(SaleOrderLine,self).product_id_change()	
        product_id = self.product_id
        sale_price = product_id.lst_price
        cost_price = product_id.standard_price
        self.current_margin = sale_price - cost_price
        current_margin = self.current_margin
        if cost_price > 0:        
            per = current_margin / cost_price
            self.current_margin_per = per * 100
        return result

    # Define function on expected margin changed
    @api.multi
    @api.onchange('expected_margin')
    def expected_margin_change(self):
        result = super(SaleOrderLine,self).product_id_change()	
        product_id = self.product_id
        cost_price = product_id.standard_price
        self.price_unit = self.expected_margin + cost_price
        expected_margin = self.expected_margin
        price_unit = self.price_unit	        
        if cost_price > 0:
            per = expected_margin / cost_price
            self.expected_margin_per = per * 100
        return result

# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from datetime import date, datetime, timedelta
from odoo.exceptions import ValidationError

class PtiSalesOrderCustom(models.Model):
    _inherit = 'sale.order'

    project_id = fields.Many2one('project.project', string="Project",  copy=False, ondelete='cascade')

    @api.model
    def create(self,vals):
        sequence = self.env['ir.sequence'].next_by_code('sale.order') or _('New')
        
        #Create project from here!
        Project = self.env['project.project']
        projects = Project.create(self._create_lead_project_data(sequence))

        vals['name'] = sequence
        vals['project_id'] = projects.id

        res = super(PtiSalesOrderCustom, self).create(vals)
        return res

    def _create_lead_project_data(self , sequence):
        return {
            'name': sequence,
            'alias_name' : sequence,
        }

class PtiPurchaseOrderCustom(models.Model):
    _inherit = 'purchase.order'

    project_id = fields.Many2one('project.project', string="Project", required= True, copy=False, ondelete='cascade')

class PtiExpenseCustom(models.Model):
    _inherit = 'hr.expense'

    project_id = fields.Many2one('project.project', string="Project", required= True, copy=False, ondelete='cascade')
    exp_description = fields.Char("Description")

    @api.model
    def create(self,vals):
        if vals.get('name', _('New')) == _('New'):
            sequence = self.env['ir.sequence'].next_by_code('hr.expense') or _('New')
            vals['name'] = sequence
            
        res = super(PtiExpenseCustom, self).create(vals)
        return res

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = '[' + record.name + ']' + ' ' + record.exp_description or ''
            result.append((record.id, name))
        return result
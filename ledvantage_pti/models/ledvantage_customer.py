# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
from datetime import date, datetime, timedelta
from odoo.exceptions import ValidationError

class PtiResPartnerCustom(models.Model):
    _inherit = 'res.partner'

    int_id = fields.Integer(string="Integration ID")
    cust_type = fields.Char(string="Customer Type")
    cust_elev_value = fields.Char(string="Elevation Value")
    cust_elev_unit = fields.Char(string="Elevation Unit")
    transaction_type = fields.Char(string="Transaction Type")
    customer_code3 = fields.Char(string="Code 3")
    code_group_company1 = fields.Char(string="Code Group Company")
    custom_3 = fields.Char(string="Custom 3")
    top = fields.Char(string="Term Of Payment")

    custom_parent_id = fields.Many2one('res.partner', string="Parent Company", index=True, copy=False)
    site_line = fields.One2many('ledvantage.sitepartner', 'partner_id', string="Sites")

    @api.one
    def my_button(self):
        print("##########################################")

class PtiPartnerSitesModel(models.Model):
    _name = 'ledvantage.sitepartner'
    
    name = fields.Char(string="Name", required=True)
    cust_city = fields.Char(string="City")
    cust_loc_type = fields.Char(string="Location Type")
    int_id = fields.Integer(string="Integration ID", required=True)
    cust_state = fields.Char(string="State")
    cust_site_address = fields.Char(string="Cust. Address")
    site_pic_phone = fields.Char(string="Pic Phone")
    site_address = fields.Char(string="Site Address")
    pkg_desc = fields.Char(string="Package Description")

    partner_id = fields.Many2one('res.partner', string="Customer", required=True, ondelete='cascade', index=True, copy=False)

LIPS© - LedVantage Inventory Sistem & Purchase Order Management Module
=============================

LIPS© menyediakan module yang mampu mengintegrasikan semua proses pembelian barang 
(quotation, purchase order)  dan melakukan pencatatan barang tersebut hingga masuk 
ke warehouse (inventory sistem) hingga barang tersebut dijual (Sales Management) 
dan system kami akan kami rancang khusus untuk kebutuhan client
yaitu system pembelian kembali (buy back). 

Depends
=======
[stock] addon Odoo

Tech
====
* [Python] - Models
* [XML] - Odoo views

Installation
============
- http://ledvantage.id/
- Install our custom addon

License
=======
GNU LESSER GENERAL PUBLIC LICENSE, Version 3 (LGPLv3)
(http://www.gnu.org/licenses/agpl.html)

Bug Tracker
===========

Contact odoo@ledvantage.id

Authors
-------
* Guritno Dimaz

Maintainer
----------

This module is maintained by LedVantage.

For support and more information, please visit http://ledvantage.id/.
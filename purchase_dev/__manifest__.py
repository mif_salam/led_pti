# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Purchase Order',
    'version'       : '0.1',
    'summary'       : 'Custom Purchase Module for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- Disable create button on tree and form
    """,
    'depends'       : [
        'purchase',
        'crm_dev',
        'stock_dev',
        'osi_purchase_workflow_customization',
        'document_checklist',
    ],
    'data': [
        'views/purchase_order_view.xml',
        'views/cost_category_view.xml',
        'views/stock_view.xml',
        'wizard/purchase_picking_wizard.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
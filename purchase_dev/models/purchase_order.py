from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    sale_id = fields.Many2one('sale.order', string="Sale Order", domain=[('state','in',['sale','delivery','waiting_invoice','waiting_payment','done'])])
    vendor_date = fields.Date(string='Vendor Ref Date')
    cost_category_id = fields.Many2one('cost.category', string="Cost Category")
    delivery_location_id = fields.Many2one('delivery.location', 'Delivery Location')
    state = fields.Selection([
        ('purchase rfq', 'Purchase Request'),
        ('draft', 'Draft Order/RFQ'),
        ('sent', 'Submitted to Supplier'),
        ('to approve', 'Purchase Approve'),
        ('co approve', 'Purchase Co-approval'),
        ('purchase', 'Purchase Order'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Status', readonly=True, index=True, copy=False,
        default='purchase rfq', track_visibility='onchange',
    )
    vendor_doc = fields.Binary(string='Vendor Acceptance Document', copy=False)
    vendor_doc_name = fields.Char(string='Vendor Doc Name', copy=False)
    doc_ids = fields.One2many('document.checklist', 'purchase_order_id', string='Document Checklist')

    @api.multi
    def print_quotation(self):
        return self.env.ref('purchase.report_purchase_quotation').report_action(self)

    @api.multi
    def write(self, vals):
        res = super(PurchaseOrder, self).write(vals)
        if 'vendor_doc' in vals :
            self.button_confirm()
        return res

    @api.multi
    def set_to_draft(self):
        for purchase in self :
            purchase.button_cancel()
            purchase.button_draft()

    @api.multi
    def button_cancel(self):
        res = super(PurchaseOrder, self).button_cancel()
        for purchase in self :
            if purchase.sale_id.opportunity_id :
                if not purchase.sale_id.opportunity_id.team_id.user_id :
                    raise Warning("Silahkan input PTI Division beserta Channel Leadernya, untuk opportunity %s"%(purchase.sale_id.opportunity_id.name))
                if purchase.sale_id.opportunity_id.team_id.user_id != self.env.user :
                    raise Warning("Hanya manager divisi %s yang bisa approve."%(purchase.sale_id.opportunity_id.team_id.name))
        return res

    @api.multi
    def _create_picking(self):
        if 'force_create' in self._context :
            return super(PurchaseOrder, self)._create_picking()
        else :
            return False

class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    @api.depends('move_ids.state','qty_received')
    @api.multi
    def _get_picking_qty(self):
        for line in self :
            move_ids = line.move_ids.filtered(lambda move: move.state not in ('draft','cancel','done'))
            line.picking_qty = sum(move.product_uom_qty for move in move_ids)
            line.outstanding_qty = line.product_qty - line.qty_received - line.picking_qty

    picking_qty = fields.Float(compute='_get_picking_qty', string="Picking Qty", digits=dp.get_precision('Product Unit of Measure'), store=True)
    outstanding_qty = fields.Float(compute='_get_picking_qty', string="Outstanding Qty", digits=dp.get_precision('Product Unit of Measure'), store=True)


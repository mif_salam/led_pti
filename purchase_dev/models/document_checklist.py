from odoo import fields, api, models, _
from odoo.exceptions import Warning

class DocumentChecklist(models.Model):
    _inherit = 'document.checklist'

    purchase_order_id = fields.Many2one('purchase.order')

    @api.model
    def create(self, vals):
        if 'purchase_order_id' in vals :
            purchase_order_id = self.env['purchase.order'].browse(vals['purchase_order_id'])
            vals['name'] = purchase_order_id.name
        res = super(DocumentChecklist, self).create(vals)
        return res

from odoo import fields, api, models, _

class CostCategory(models.Model):
    _name = "cost.category"
    _description = "Cost Category"

    name = fields.Char(string='Cost Category', required=True)
    
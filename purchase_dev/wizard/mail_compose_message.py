from odoo import fields, api, models, _

class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    @api.multi
    def send_mail_action(self):
        res = super(MailComposer, self).send_mail_action()
        if self.model == 'purchase.order' :
            order_id = self.env[self._context['active_model']].browse(self.res_id)
            order_id.write({'state':'sent'})
        return res

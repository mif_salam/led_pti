# -*- coding: utf-8 -*-
import math
import re
from datetime import datetime
from odoo import api, models, exceptions

# product.category
# Edit product category tambah code
# Edit product template tambah supplier info

class ProductAutoDescription(models.Model):
    _inherit = "product.template"

    @api.model
    def create(self, vals):
        res = super(ProductAutoDescription, self).create(vals)
        return on_saveProduct(res)

    # @api.model
    # def write(self, ids, vals):
    #     res = super(ProductAutoDescription, self).write(vals) #res stores the result of the write function'
    #     return res

def on_saveProduct(res):
    #Check for stockable product
    if res.type != "product":
        return res

    # Get supplier code
    sup_code = get_supplier_code(res.seller_ids)
    # Get auto generated code
    auto_num = str (res.id).zfill(5)
    # Get auto generated code
    today = datetime.today()
    bulan = str(today.month).zfill(2)
    tahun = str(today.year)
    # Get category code
    str_display_name = generate_category_code(res)
    str_display_name_combine = sup_code + str_display_name + tahun[-2:]  + bulan + auto_num
    res.display_name = str_display_name_combine
    res.name = str_display_name_combine
    res.barcode = str_display_name_combine

    str_desc = generate_description(res)
    str_combine = str_display_name + ' ' + str_desc
    res.description = str_combine
    res.description_picking = str_combine
    res.description_pickingin = str_combine
    res.description_pickingout = str_combine
    res.description_purchase = str_combine
    res.description_sale = str_combine
    return res

def get_supplier_code(data):
    if len(data) <= 0 :
        raise exceptions.ValidationError('Vendor Harus diisi!')

    if len(data) > 1 :
        raise exceptions.ValidationError('Vendor tidak boleh lebih dari 1!')

    str = ""
    for val_attr in data:
        str += val_attr.name.ref

    if str == "" :
        raise exceptions.ValidationError('Purchases-> Vendor -> Misc -> Internal Reference harus diisi!')

    return str

def generate_category_code(data):
    str = data.categ_id.x_code
    if str == "" or str == False  :
        raise exceptions.ValidationError('Product Category harus di pilih, dengan format XXX:(Nama Category)!')
    return str.strip()

def generate_description(data):
    attribute_names = ""
    for line in data.attribute_line_ids:
        for val_attr in line.value_ids:
            attribute_names += " " + val_attr.name

    if attribute_names == "" :
        raise exceptions.ValidationError('Product Attribute harus di isi!')

    return attribute_names

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
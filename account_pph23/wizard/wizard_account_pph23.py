from odoo import fields, api, models
from dateutil.relativedelta import relativedelta


class WizardAccountPph23(models.TransientModel):
    _name = 'wizard.account.pph23'
    _description = 'Export PPh23'

    @api.model
    def _default_date_from(self):
        return (fields.date.today() - relativedelta(months=1)).strftime('%Y-%m-01')

    @api.model
    def _default_date_to(self):
        today = fields.date.today()
        return (fields.date(today.year, today.month, 1) - relativedelta(days=1)).strftime('%Y-%m-%d')

    date_from = fields.Date('Date From', required=True,
                            default=_default_date_from)
    date_to = fields.Date('Date To', required=True,
                          default=_default_date_to)
    type = fields.Selection([
        ('out_invoice', 'Customer Invoices'),
        ('in_invoice', 'Vendor Bill')], string='Type', default='out_invoice', required=True)

    @api.multi
    def action_export(self):
        return self.env.ref('account_pph23.account_pph23_csv').report_action(self.ids, config=False)

    @api.multi
    def get_report_data(self):
        domain = [
            ('pph23_date', '>=', self.date_from),
            ('pph23_date', '<=', self.date_to),
            ('type', '=', self.type),
            ('is_pph23', '=', True),
            ('state', 'in', ('open', 'paid'))
        ]
        res = []
        for inv in self.env['account.invoice'].search(domain):
            res.append({
                'Kode Form Bukti Potong': inv.pph23_invoice_tax_id.tax_id.pph23_form_code,
                'Masa Pajak': fields.Date.from_string(inv.pph23_date).strftime("%m"),
                'Tahun Pajak': fields.Date.from_string(inv.pph23_date).strftime("%Y"),
                'Pembetulan': '0',
                'NPWP WP yang Dipotong': inv.partner_id.vat,
                'Nama WP yang Dipotong': inv.partner_id.name,
                'Alamat WP yang Dipotong': inv.partner_id.street,
                'Nomor Bukti Potong': inv.pph23_transaction_code,
                'Tanggal Bukti Potong': inv.pph23_date,
                inv.pph23_invoice_tax_id.tax_id.pph23_type: abs(inv.pph23_invoice_tax_id.tax_id.amount),
                'Nilai Bruto 1': abs(inv.pph23_invoice_tax_id.base),
                'PPh Yang Dipotong  1': abs(inv.pph23_invoice_tax_id.amount),
                'Jumlah Nilai Bruto ': abs(inv.pph23_invoice_tax_id.base),
                'Jumlah PPh Yang Dipotong': abs(inv.pph23_invoice_tax_id.amount)
            })
        return res

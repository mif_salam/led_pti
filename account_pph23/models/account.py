from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    @api.depends('tax_line_ids.tax_id')
    def _is_pph23(self):
        for invoice in self:
            if any(t.tax_id.pph23_type for t in invoice.tax_line_ids):
                invoice.is_pph23 = True
                for t in invoice.tax_line_ids:
                    if t.tax_id.pph23_type:
                        invoice.pph23_invoice_tax_id = t.id
            else:
                invoice.is_pph23 = False

    pph23_transaction_code = fields.Char('Nomor Bukti Potong PPh 23')
    pph23_date = fields.Date('Tanggal Bukti Potong PPh 23')

    is_pph23 = fields.Boolean('Is PPh 23 Transaction',
                              compute='_is_pph23', store=True)

    pph23_invoice_tax_id = fields.Many2one(
        'account.invoice.tax', string='PPh 23 Tax Type', compute='_is_pph23', store=True)


class AccountTax(models.Model):
    _inherit = 'account.tax'

    pph23_type = fields.Selection([
        ('Tarif 1', 'Tarif 1'),
        ('Tarif 2', 'Tarif 2'),
        ('Tarif 3', 'Tarif 3'),
        ('Tarif 4', 'Tarif 4'),
        ('Tarif 5', 'Tarif 5'),
        ('Tarif 6a/Tarif 6', 'Tarif 6a/Tarif 6'),
        ('Tarif 6b/Tarif 7', 'Tarif 6b/Tarif 7'),
        ('Tarif 6c/Tarif 8', 'Tarif 6c/Tarif 8'),
        ('Tarif 9', 'Tarif 9'),
        ('Tarif 10', 'Tarif 10'),
        ('Tarif 11', 'Tarif 11'),
        ('Tarif 12', 'Tarif 12'),
        ('Tarif 13', 'Tarif 13'),
        ('Tarif 6d1', 'Tarif 6d1'),
        ('Tarif 6d2', 'Tarif 6d2'),
        ('Tarif 6d3', 'Tarif 6d3'),
        ('Tarif 6d4', 'Tarif 6d4'),
        ('Tarif 6d5', 'Tarif 6d5'),
        ('Tarif 6d6', 'Tarif 6d6'),
    ], string="Tipe PPh 23")
    pph23_form_code = fields.Char('Kode Form Bukti Potong PPh 23')

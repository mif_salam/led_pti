# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Accounting',
    'version': '0.1',
    'summary': 'Custom Accounting PPH23 Export Module ',
    'author': 'Arkana',
    'description': """
Features :
- Export PPH23 CSV Format
    """,
    'depends': [
        'account',
        'report_csv',
    ],
    'data': [
        'views/view_account.xml',
        'wizard/view_wizard_account_pph23.xml',

        'report/report.xml',
    ],
    'demo': [],
    'installable': True
}

odoo.define('mail.utils_override', function (require) {
    "use strict";


    var bus = require('bus.bus').bus;
    var session = require('web.session');
    var web_client = require('web.web_client');
    var original_mail_utils = require('mail.utils');
    var _beep = (function () {
    if (typeof(Audio) === "undefined") {
        return function () {};
    }
    var audio;
    return function () {
        if (!audio) {
            audio = new Audio();
            var ext = audio.canPlayType("audio/ogg; codecs=vorbis") ? ".ogg" : ".mp3";
            audio.src = session.url("/mail/static/src/audio/ting" + ext);
        }
        audio.play();
    };
})();

    function _send_native_notification(title, content) {
     let model = title.split("|")[0]
           let model_id = title.split("|")[1]
           let model_receiverID = title.split("|")[2]
    var notification = new Notification(title, {
        body: content,
         requireInteraction: false,
        icon: "/mail/static/src/img/odoo_o.png"});

    var url = window.location.href
    var arr = url.split("/");
    var result = arr[0] + "//" + arr[2]

    notification.onclick = function () {
        window.location = result + "/web#id="+model_id+"view_type=form&model="+model;
        if (this.cancel) {
            this.cancel();
        } else if (this.close) {
            this.close();
        }
    };
}


    original_mail_utils.send_notification =
       function (widget,title, content) {
    if (window.Notification && Notification.permission === "granted" || true ) {
        if (bus.is_master || true) {
            _send_native_notification(title, content);
        }
    } else {
        web_client.do_notify(title, content);
        if (bus.is_master) {
            _beep();
        }
    }
}


});

odoo.define('web_kanban.Record_override', function (require) {
    "use strict";
    // remove width limit
    var kanban_view = require('web.KanbanRecord');
    var core = require("web.core");
    var new_kanban = kanban_view.include({
 renderElement: function () {
        this._super();
        let _self = this;

        if(this.model === 'desktop_notifications.dashboard') {
            _self.$el.removeClass("o_kanban_record");
            _self.$el.css("width", "1600px");
        }
    },


    });
    core.form_widget_registry.add('kanban_custom', new_kanban);
});

odoo.define('mail_chat_manager_override', function(require) {
"use strict";
let original_chat_manager = require('mail.chat_manager');

original_chat_manager.make_message = (function(_super) {
    return function() {
        let orig = _super.apply(this, arguments);
        let link_to_object = "none";
        let notification_header = 'none';
        let data = arguments[0];
        if(data.subject) {

            let data_array = data.subject.split("|");
            if (data_array.length == 4 && data_array[0] == 'notification_automatic_xx') {
                let model = data.subject.split("|")[1];
                let model_id = data.subject.split("|")[2];

                if(model === 'crm.lead_from'){
                notification_header = "Lead from contact form";
                link_to_object = "web#id="+model_id+"&view_type=form&model=crm.lead";
                }

                if(model === 'crm.lead'){
                notification_header = data.subject.split("|")[3];
                link_to_object = "web#id="+model_id+"&view_type=form&model=crm.lead";
                }
            }
        }
        let extend_msg = {
             notification_header : notification_header,
            link_to_object : link_to_object,
        };
        $.extend(orig, extend_msg);
        return orig;
    };

})(original_chat_manager.make_message);

});





from odoo import models, fields, api
import datetime
import logging
_logger = logging.getLogger(__name__)

class DashBoard(models.Model):
    _name = 'crm_notifications.dashboard'


    @api.one
    def _get_count(self):
        today = 0
        week = 0
        month = 0
        total = 0

        lead_status_today = 0
        lead_status_week = 0
        lead_status_month = 0
        lead_status_total = 0

        saleperson_today = 0
        saleperson_week = 0
        saleperson_month = 0
        saleperson_total = 0

        lead_contact_today = 0
        lead_contact_week = 0
        lead_contact_month = 0
        lead_contact_total = 0

        for notification in self.env['crm_notifications.notification'].search([]):
            if notification.date:
                d1 = datetime.datetime.strptime(str(notification.date).split(".")[0], '%Y-%m-%d %H:%M:%S')
                d2 = datetime.datetime.now().date()

                d1 = d1.date()
                d3 = d2 - d1
                days = int(str(d3.days))

                if days < 1:
                    today +=1
                if  days < 8:
                    week +=1
                if  days < 31:
                    month +=1
                total +=1

                if notification.type == "lead_status":
                    if days < 1:
                        lead_status_today += 1
                    if days < 8:
                        lead_status_week += 1
                    if days < 31:
                        lead_status_month += 1
                    lead_status_total += 1

                if notification.type == "saleperson_assign":
                    if days < 1:
                        saleperson_today += 1
                    if days < 8:
                        saleperson_week += 1
                    if days < 31:
                        saleperson_month += 1
                    saleperson_total += 1

                if notification.type == "lead_contact":
                    if days < 1:
                        lead_contact_today += 1
                    if days < 8:
                        lead_contact_week += 1
                    if days < 31:
                        lead_contact_month += 1
                    lead_contact_total += 1

        self.count_today = int(today)
        self.count_week = int(week)
        self.count_month = int(month)
        self.count_total = int(total)

        self.lead_status_count_today = int(lead_status_today)
        self.lead_status_count_week = int(lead_status_week)
        self.lead_status_count_month = int(lead_status_month)
        self.lead_status_count_total = int(lead_status_total)

        self.saleperson_count_today = int(saleperson_today)
        self.saleperson_count_week = int(saleperson_week)
        self.saleperson_count_month = int(saleperson_month)
        self.saleperson_count_total = int(saleperson_total)

        self.lead_contact_count_today = int(lead_contact_today)
        self.lead_contact_count_week = int(lead_contact_week)
        self.lead_contact_count_month = int(lead_contact_month)
        self.lead_contact_count_total = int(lead_contact_total)

    count_today = fields.Integer(compute="_get_count")
    count_week = fields.Integer(compute="_get_count")
    count_month = fields.Integer(compute="_get_count")
    count_total = fields.Integer(compute="_get_count")

    lead_status_count_today = fields.Integer(compute="_get_count")
    lead_status_count_week = fields.Integer(compute="_get_count")
    lead_status_count_month = fields.Integer(compute="_get_count")
    lead_status_count_total = fields.Integer(compute="_get_count")

    saleperson_count_today = fields.Integer(compute="_get_count")
    saleperson_count_week = fields.Integer(compute="_get_count")
    saleperson_count_month = fields.Integer(compute="_get_count")
    saleperson_count_total = fields.Integer(compute="_get_count")

    lead_contact_count_today = fields.Integer(compute="_get_count")
    lead_contact_count_week = fields.Integer(compute="_get_count")
    lead_contact_count_month = fields.Integer(compute="_get_count")
    lead_contact_count_total = fields.Integer(compute="_get_count")

    name = fields.Char(string="Name")

    def _get_notification_count(self):
        self.notification_count = self.env['crm_notifications.notification'].\
            search_count()

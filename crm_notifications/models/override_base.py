import datetime
from odoo import api, fields, models
import logging
from odoo.exceptions import UserError, AccessError, ValidationError

_logger = logging.getLogger(__name__)


def return_channel(obj, name):

    channel = obj.env['mail.channel'].sudo(1).search([('name', '=', str(name))])
    if len(channel) <1:
        channel = obj.env['mail.channel'].sudo(1).create({
            'name': str(name),
            'public': 'private',
            'channel_partner_ids': [],
            'channel_type': 'chat',
            'group_public_id': obj.env.ref('base.group_portal').id})

    return channel


class Message(models.Model):
    """ Messages model: system notification (replacing res.log notifications),
        comments (OpenChatter discussion) and incoming emails. """
    _inherit = 'mail.message'
    link_to_object = fields.Text(default="None")


class Lead(models.Model):
    _inherit = "crm.lead"

    def lead_status_change(self, obj, message_text):

        channel = return_channel(obj, "CRM Notifications for: " + str(self.user_id.name))
        if not obj.user_id.partner_id:
            return
        channel.channel_partner_ids = obj.user_id.partner_id
        message_body = "CRM| " + str(obj.id) + "|" + message_text

        new_notification = obj.env['crm_notifications.notification'].sudo(1).create({
            'name': "Lead status change: " + str(obj.stage_id.name),
            'type': 'lead_status',
            'field_name': 'contact form',
            'model_name': 'crm.lead',
            'body': message_body,
            'date': datetime.datetime.now(),
            'sent_to': obj.user_id.id,
            'sent_by': self.env.user.partner_id.id,
            'link_lead': obj.id})

        channel.sudo(1).message_post(message_body,
                                     "notification_automatic_xx|crm.lead|" + str(self.id) + "|Header" +
                                     "Lead status change",
                                     'comment',
                                     'mail.mt_comment', False,
                                     None, 'html')
    @api.onchange('stage_id')
    def _onchange_stage_id(self):
        values = self._onchange_stage_id_values(self.stage_id.id)

        message = "Lead : " + self.name + " stage was changed to: " + self.stage_id.name
        self.lead_status_change(self._origin, message)
        self.update(values)

    @api.multi
    def action_set_lost(self):
        message = "Lead : " + self.name + " was marked as LOST  "
        self.lead_status_change(self, message)
        """ Lost semantic: probability = 0, active = False """
        return self.write({'probability': 0, 'active': False})

    @api.multi
    def action_set_active(self):
        message = "Lead : " + self.name + " was marked as ACTIVE  "
        self.lead_status_change(self, message)
        return self.write({'active': True})

    @api.multi
    def action_set_unactive(self):
        message = "Lead : " + self.name + " was marked as UNACTIVE  "
        self.lead_status_change(self, message)
        return self.write({'active': False})

    @api.multi
    def action_set_won(self):
        message = "Lead : " + self.name + " was marked as WON  "
        self.lead_status_change(self, message)

        """ Won semantic: probability = 100 (active untouched) """
        for lead in self:
            stage_id = lead._stage_find(domain=[('probability', '=', 100.0), ('on_change', '=', True)])
            lead.write({'stage_id': stage_id.id, 'probability': 100})



    @api.constrains('user_id')
    def salesperson_assign(self):

        channel = return_channel(self, "CRM Notifications for: " + str(self.user_id.name))

        channel.channel_partner_ids = self.user_id.partner_id
        message_body = "CRM| " + str(self.id) + "| You were assigned to lead : " + self.name

        new_notification = self.env['crm_notifications.notification'].sudo(1).create({
            'name': "Lead saleperson change: " + str(self.name),
            'type': 'saleperson_assign',
            'field_name': 'saleperson',
            'model_name': 'crm.lead',
            'body': message_body,
            'date': datetime.datetime.now(),
            'sent_to': self.user_id.id,
            'sent_by': self.create_uid.partner_id.id,
            'link_lead': self.id})

        channel.moderation_notify_msg = message_body
        channel.sudo(1).message_post(message_body,
                                     "notification_automatic_xx|crm.lead|"+ str(self.id)+"|Saleperson change",
                                     'comment',
                                     'mail.mt_comment', False,
                                     None, 'html')

    @api.model
    def create(self, vals):
        res = super(Lead, self).create(vals)

        if vals.get('medium_id') and vals['medium_id'] == 1:
            self.notify_contact_form(res)

        return res

    def notify_contact_form(self,lead):
        partners = self.env['res.partner'].sudo(1).search([('id', '=', 3)])
        for partner in partners:
            channel = return_channel(self, "CRM Notifications for: " + str(partner.name))

            channel.channel_partner_ids = partner
            message_body = "CRM| " + str(lead.id) + "| Contact form was filled by : " + lead.name + "  from company:  " \
                                                         + lead.partner_name + "  with question: " + lead.description

            new_notification = self.env['crm_notifications.notification'].sudo(1).create({
                'name': "Lead create from form: " + str(lead.contact_name),
                'type': 'lead_contact',
                'field_name': 'contact form',
                'model_name': 'crm.lead',
                'body': message_body,
                'contact_name': lead.contact_name,
                'link_lead': lead.id,
                'date': datetime.datetime.now(),
                'contact_email': lead.email_from,
                'sent_to': partner.user_id.id})

            new_notification.sent_by = partner
            new_notification.sent_to = self.partner_id

            channel.sudo(1).message_post(message_body,
                                            "notification_automatic_xx|crm.lead|"+str(lead.id) + "|" + str(partner.id),
                                             'comment',
                                    'mail.mt_comment', False,
                                    None, 'html')


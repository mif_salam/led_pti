
from odoo import api, fields, models


class Notification(models.Model):

    _name = 'crm_notifications.notification'

    name = fields.Char()
    type = fields.Char()
    model_name = fields.Char()
    body = fields.Text()
    note = fields.Char()
    date = fields.Datetime()
    sent_by = fields.Many2one('res.users')
    sent_to = fields.Many2one('res.users')

    link_applicant = fields.Many2one('hr.applicant')
    link_lead = fields.Many2one('crm.lead')

    contact_email = fields.Char()
    field_name = fields.Char('Field name')















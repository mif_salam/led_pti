# -*- coding: utf-8 -*-
{
    'name': "CRM Notifications",

    'summary': """
        Be in real time notified when some CRM activities are made""",

    'description': """
        Desktop notifications for CRM module. Be in real time notified when some CRM activities are made.
    """,

    'author': "Implemento",
    'website': "http://implemento.sk/",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Sales',
    'version': '11.0.0.0.1',
    'license': 'LGPL-3',
    'support': 'lagin@implemento.sk',
    'images': ['images/main_screenshot.png'],

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale', 'crm'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/dashboard.xml',
        'views/templates.xml',
        'views/notification.xml',
    ],

    'qweb': [
        'static/src/xml/override.xml',
    ],

    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
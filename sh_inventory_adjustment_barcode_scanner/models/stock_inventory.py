# -*- coding: utf-8 -*-
# Copyright (C) Softhealer Technologies.

from odoo import models,api
from odoo.exceptions import UserError

class StockInventory(models.Model):
    _inherit='stock.inventory'
 
    @api.one
    def sh_on_barcode_scanned(self, barcode):
                
        inventory_id = self.id
        stock_inventory = self.env['stock.inventory'].search([('id', '=', inventory_id)],limit=1)       
            
        if stock_inventory:
            product_id = self.env['product.product'].search([('barcode', '=', barcode)],limit=1)
    
            if product_id:                

                location_id = stock_inventory.location_id                                    
                if not stock_inventory.line_ids:    
                    inventory_line_val = {
                            'display_name': product_id.name,
                            'product_id': product_id.id,
                            'location_id':location_id,   
                            'product_qty': 1,
                            'product_uom_id': product_id.product_tmpl_id.uom_id.id,
                            'inventory_id': stock_inventory.id
                    }
                    
                    stock_inventory.update({'line_ids': [(0, 0, inventory_line_val)]})
                
                else:
                    stock_picking_line = stock_inventory.line_ids.search([('product_id', '=', product_id.id),('inventory_id','=',inventory_id)], limit=1)
                     
                    if stock_picking_line:
                        qtyProduct = stock_picking_line.product_qty
                        if qtyProduct <= 0:     
                            stock_picking_line.product_qty = 1
                        else :
                            raise UserError('Product sudah di scanned')

                    else :    
                        inventory_line_val = {
                            'display_name': product_id.name,
                            'product_id': product_id.id,
                            'location_id':location_id,   
                            'product_qty': 1,
                            'product_uom_id': product_id.product_tmpl_id.uom_id.id,
                            'inventory_id': stock_inventory.id
                        }
                        
                        stock_inventory.update({'line_ids': [(0, 0, inventory_line_val)]})
                        
            else :
                raise UserError('Product does not exist')

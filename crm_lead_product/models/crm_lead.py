# Copyright (C) 2018 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, fields, models


class CrmLead(models.Model):
    _inherit = 'crm.lead'

    @api.multi
    @api.depends('lead_line_ids.planned_revenue')
    def _get_planned_revenue(self):
        for lead in self :
            lead.planned_revenue = lead.get_planned_revenue()

    lead_line_ids = fields.One2many(
        comodel_name='crm.lead.line',
        inverse_name='lead_id',
        string='Lead Lines',
    )
    planned_revenue = fields.Float('Expected Revenue', track_visibility='always', compute='_get_planned_revenue', store=True)

    @api.multi
    def get_planned_revenue(self):
        self.ensure_one()
        planned_revenue = 0
        for lead_line in self.lead_line_ids :
            planned_revenue += lead_line.planned_revenue
        return planned_revenue

    @api.multi
    def _convert_opportunity_data(self, customer, team_id=False):
        res = super(CrmLead, self)._convert_opportunity_data(customer, team_id)

        # Update planned_revenue
        res['planned_revenue'] = self.get_planned_revenue()

        return res

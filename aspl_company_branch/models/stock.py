# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from odoo import models, fields, api, _


class stock_warehouse(models.Model):
    _inherit = 'stock.warehouse'

    branch_id = fields.Many2one('company.branch', string="Branch")

    @api.model
    def create(self, vals):
        res = super(stock_warehouse, self).create(vals)
        res.lot_stock_id.write({'branch_id':res.branch_id.id})
        res.view_location_id.write({'branch_id':res.branch_id.id})
        res.wh_input_stock_loc_id.write({'branch_id':res.branch_id.id})
        res.wh_output_stock_loc_id.write({'branch_id':res.branch_id.id})
        res.wh_pack_stock_loc_id.write({'branch_id':res.branch_id.id})
        res.wh_qc_stock_loc_id.write({'branch_id':res.branch_id.id})
        return res

    @api.multi
    def write(self, vals):
        res = super(stock_warehouse, self).write(vals)
        if vals.get('branch_id'):
            self.lot_stock_id.write({'branch_id':vals.get('branch_id')})
            self.view_location_id.write({'branch_id':vals.get('branch_id')})
            self.wh_input_stock_loc_id.write({'branch_id':vals.get('branch_id')})
            self.wh_output_stock_loc_id.write({'branch_id':vals.get('branch_id')})
            self.wh_pack_stock_loc_id.write({'branch_id':vals.get('branch_id')})
            self.wh_qc_stock_loc_id.write({'branch_id':vals.get('branch_id')})
        return res


class stock_location(models.Model):
    _inherit = 'stock.location'

    branch_id = fields.Many2one('company.branch', string="Branch")


class stock_picking(models.Model):
    _inherit = 'stock.picking'

    branch_id = fields.Many2one('company.branch', string="Branch", default=lambda self: self.env.user.branch_id,
                                states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})


class procurement_rule(models.Model):
    _inherit = 'procurement.rule'

    def _get_stock_move_values(self, product_id, product_qty, product_uom, location_id, name, origin, values, group_id):
        res = super(procurement_rule, self)._get_stock_move_values(product_id, product_qty, product_uom, location_id, name, origin, values, group_id)
        res.update({'branch_id': values.get('branch_id', False)})
        return res


class stock_move(models.Model):
    _inherit = 'stock.move'

    branch_id = fields.Many2one('company.branch', string="Branch")

    def _get_new_picking_values(self):
        res = super(stock_move, self)._get_new_picking_values()
        res.update({'branch_id': self.branch_id.id})
        return res


class stock_quant(models.Model):
    _inherit = 'stock.quant'

    branch_id = fields.Many2one(related='location_id.branch_id', string='Branch', store=True, readonly=True)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
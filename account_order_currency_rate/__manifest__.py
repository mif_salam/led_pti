# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Currency Rate - Set custom rate on PO and SO',
    'version': '0.1',
    'summary': 'Set custom currency rate on PO and SO',
    'author': 'Arkana',
    'description': """
Features :
- Set custom rate on PO and SO
    """,
    'depends': [
        'account',
        'purchase',
        'sale_management',
        'partner_credit_limit'
    ],
    'data': [
        'views/view_account.xml',
    ],
    'demo': [],
    'installable': True
}

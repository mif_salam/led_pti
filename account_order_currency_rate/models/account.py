from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    @api.multi
    @api.depends('currency_id', 'company_id')
    def _show_currency_rate(self):
        for order in self:
            if order.currency_id != order.company_id.currency_id:
                order.show_currency_rate = True

    currency_rate = fields.Float('Currency Rate', default=1)
    show_currency_rate = fields.Boolean(compute="_show_currency_rate")

    @api.onchange('currency_id')
    def onchange_currency_id(self):
        self.currency_rate = (self.company_id.currency_id.rate /
                              self.currency_id.rate) if self.currency_id.rate else 1


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    @api.depends('pricelist_id', 'company_id')
    def _show_currency_rate(self):
        for order in self:
            if order.pricelist_id.currency_id != order.company_id.currency_id:
                order.show_currency_rate = True

    currency_rate = fields.Float('Currency Rate', default=1)
    show_currency_rate = fields.Boolean(compute="_show_currency_rate")

    @api.onchange('pricelist_id')
    def onchange_pricelist_id(self):
        self.currency_rate = (self.company_id.currency_id.rate /
                              self.pricelist_id.currency_id.rate) if self.pricelist_id.currency_id.rate else 1

    @api.multi
    def _prepare_invoice(self):
        res = super(SaleOrder, self)._prepare_invoice()
        res['currency_rate'] = self.currency_rate
        return res

    @api.multi
    def action_confirm(self):
        return super(SaleOrder, self.with_context(force_currency_rate=self[0].currency_rate)).action_confirm()

    @api.multi
    def action_cancel(self):
        return super(SaleOrder, self.with_context(force_currency_rate=self[0].currency_rate)).action_cancel()


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    @api.depends('currency_id', 'company_id')
    def _show_currency_rate(self):
        for invoice in self:
            if invoice.currency_id != invoice.company_id.currency_id:
                invoice.show_currency_rate = True

    currency_rate = fields.Float('Currency Rate', default=1)
    show_currency_rate = fields.Boolean(compute="_show_currency_rate")

    @api.multi
    def action_move_create(self):
        return super(AccountInvoice, self.with_context(force_currency_rate=self[0].currency_rate)).action_move_create()

    @api.onchange('purchase_id')
    def purchase_order_change(self):
        self.currency_rate = self.purchase_id.currency_rate
        res = super(AccountInvoice, self).purchase_order_change()
        self.currency_id = self.purchase_id.currency_id.id
        return res

    @api.onchange('currency_id')
    def onchange_currency_id(self):
        if self.mapped('invoice_line_ids.purchase_line_id'):
            self.currency_rate = self.mapped(
                'invoice_line_ids.purchase_line_id.order_id.currency_rate')[0]
        elif self.mapped('invoice_line_ids.sale_line_ids.order_id'):
            self.currency_rate = self.mapped(
                'invoice_line_ids.sale_line_ids.order_id.currency_rate')[0]
        else:
            self.currency_rate = (self.company_id.currency_id.rate /
                                  self.currency_id.rate) if self.currency_id.rate else 1

    @api.onchange('currency_id')
    def _onchange_currency_id(self):
        if self.currency_id:
            for line in self.invoice_line_ids.filtered(lambda r: r.purchase_line_id):
                line.price_unit = line.purchase_id.currency_id.with_context(force_currency_rate=self.currency_rate).compute(
                    line.purchase_line_id.price_unit, self.currency_id, round=False)
            for line in self.invoice_line_ids.filtered(lambda r: r.sale_line_ids):
                line.price_unit = line.sale_line_ids.order_id.pricelist_id.currency_id.with_context(force_currency_rate=self.currency_rate).compute(
                    line.sale_line_ids[0].price_unit, self.currency_id, round=False)

    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AccountInvoice,
                    self)._prepare_invoice_line_from_po_line(line)
        res['price_unit'] = line.price_unit
        return res


class ResCurrency(models.Model):
    _inherit = 'res.currency'

    @api.model
    def _get_conversion_rate(self, from_currency, to_currency):
        if self._context.get('force_currency_rate'):
            return self._context.get('force_currency_rate')
        from_currency = from_currency.with_env(self.env)
        to_currency = to_currency.with_env(self.env)
        return to_currency.rate / from_currency.rate

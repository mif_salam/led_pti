from odoo import fields, api, models, _
from odoo.exceptions import Warning

class HrDepartment(models.Model):
    _inherit = 'hr.department'

    account_expense_id = fields.Many2one('account.account', string='Expense Account', default=lambda self: self.env['ir.property'].get('property_account_expense_categ_id', 'product.category'))
    
class HrExpense(models.Model):
    _inherit = 'hr.expense'

    percentage_line = fields.One2many('hr.expense.percentage', 'expense_id', 'Expense Percentage', copy=True)

    @api.onchange('product_id')
    def product_change(self):
        if self.product_id :
            percentage_line = []
            for line in self.product_id.department_percentage_ids :
                percentage_line.append((0,0,{
                    'department_id': line.department_id.id,
                    'percentage': line.percentage,
                    'account_expense_id': line.account_expense_id.id
                }))
            self.percentage_line = percentage_line

    @api.multi
    def _check_percentage(self):
        self.ensure_one()
        if self.percentage_line :
            total_percent = 0
            for line in self.percentage_line :
                total_percent += line.percentage
                if not line.percentage :
                    raise Warning(_("Please input percentage."))
                if self.state == 'approve_manager' and not line.account_expense_id :
                    raise Warning(_("Please input expense account for all department in Department Percentage page."))
            if total_percent != 100 :
                raise Warning(_("Total of department percentage must be 100 percent.\n\nCurrent total percentage : %s percent"%('{0:,.2f}'.format(total_percent))))

    @api.multi
    def action_move_create(self):
        res = super(HrExpense, self).action_move_create()
        for expense in self :
            expense._check_percentage()
        return res

    @api.multi
    def _move_line_get(self):
        account_move = []
        for expense in self:
            move_line = expense._prepare_move_line_value()
            #START CUSTOM CODE
            if not expense.percentage_line :
                account_move.append(move_line)
                # Calculate tax lines and adjust base line
                taxes = expense.tax_ids.with_context(round=True).compute_all(expense.unit_amount, expense.currency_id, expense.quantity, expense.product_id)
                account_move[-1]['price'] = taxes['total_excluded']
                account_move[-1]['tax_ids'] = [(6, 0, expense.tax_ids.ids)]
                for tax in taxes['taxes']:
                    account_move.append({
                        'type': 'tax',
                        'name': tax['name'],
                        'price_unit': tax['amount'],
                        'quantity': 1,
                        'price': tax['amount'],
                        'account_id': tax['account_id'] or move_line['account_id'],
                        'tax_line_id': tax['id'],
                        'expense_id': expense.id,
                    })
            else :
                for percent in expense.percentage_line :
                    account_move.append({
                        'type': 'src',
                        'name': '%s (%s)'%(move_line['name'],percent.department_id.name),
                        'price_unit': move_line['price_unit'] * (percent.percentage/100),
                        'quantity': self.quantity,
                        'price': move_line['price'] * (percent.percentage/100),
                        'account_id': percent.account_expense_id.id,
                        'product_id': self.product_id.id,
                        'uom_id': self.product_uom_id.id,
                        'analytic_account_id': self.analytic_account_id.id,
                        'expense_id': self.id,
                    })

                    # Calculate tax lines and adjust base line
                    taxes = expense.tax_ids.with_context(round=True).compute_all(move_line['price'] * (percent.percentage/100), expense.currency_id, expense.quantity, expense.product_id)
                    account_move[-1]['price'] = taxes['total_excluded']
                    account_move[-1]['tax_ids'] = [(6, 0, expense.tax_ids.ids)]
                    for tax in taxes['taxes']:
                        account_move.append({
                            'type': 'tax',
                            'name': tax['name'],
                            'price_unit': tax['amount'],
                            'quantity': 1,
                            'price': tax['amount'],
                            'account_id': tax['account_id'] or percent.account_expense_id.id,
                            'tax_line_id': tax['id'],
                            'expense_id': expense.id,
                        })
            #END OF CUSTOM CODE
        return account_move

class HrExpensePercentage(models.Model):
    _name = "hr.expense.percentage"
    _description = "Expense Percentage"

    expense_id = fields.Many2one('hr.expense', 'Expense', ondelete='cascade', index=True)
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    percentage = fields.Float(string='Percentage', required=True)
    account_expense_id = fields.Many2one('account.account', string='Expense Account', default=lambda self: self.env['ir.property'].get('property_account_expense_categ_id', 'product.category'))

    @api.onchange('department_id')
    def department_change(self):
        if self.department_id :
            self.account_expense_id = self.department_id.account_expense_id.id

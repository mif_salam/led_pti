from odoo import models, fields, api, _
from datetime import datetime, timedelta
from odoo.addons import decimal_precision as dp
from odoo.exceptions import Warning, ValidationError, UserError
import pytz
from pytz import timezone

STATE = [
    ('draft','Draft'),
    ('confirm','Waiting for Approval'),
    ('approve_manager','Approved by Manager'),
    ('approve_finance','Approved by Finance'),
    ('done','Done'),
    ('reject','Rejected'),
    ('cancel','Cancelled'),
]

class AdvanceRequest(models.Model):
    _name = "advance.request"
    _description = "Advance Request"
    _order = "id desc"
    
    @api.multi
    def get_default_date_multi(self):
        return pytz.UTC.localize(datetime.now()).astimezone(timezone('Asia/Jakarta'))

    @api.multi
    @api.depends('advance_request_line.price_subtotal','advance_request_realization.price_subtotal')
    def _compute_amount(self):
        for me_id in self :
            total = 0
            total_realization = 0
            for line in me_id.advance_request_line :
                total += line.price_subtotal
            for line in me_id.advance_request_realization :
                total_realization += line.price_subtotal
            me_id.amount_total = total
            me_id.amount_total_realization = total_realization

    def _get_employee(self):
        employee = self.env['hr.employee'].search([
            ('user_id','=',self._uid)
        ], limit=1)
        return employee and employee.id or False

    @api.multi
    @api.depends('employee_id.manager','employee_id.department_id.manager_id','state')
    def _get_approval_name(self):
        for request in self :
            if request.state == 'confirm' :
                if request.employee_id.manager :
                    approval_name = request.employee_id.name
                else :
                    approval_name = request.department_id.manager_id.name
            elif request.state == 'approve_manager' :
                approval_name = 'Accounting/Finance User'
            else :
                approval_name = False
            request.approval_name = approval_name

    name = fields.Char(string='Name', required=True, default='New', copy=False)
    date = fields.Datetime('Date', default=datetime.now(), required=True)
    state = fields.Selection(STATE, string="State", default='draft')
    employee_id = fields.Many2one('hr.employee', 'Employee', ondelete='restrict', required=True, default=_get_employee)
    currency_id = fields.Many2one('res.currency', 'Currency', ondelete='restrict', required=True, default=lambda self: self.env.user.company_id.currency_id.id)
    department_id = fields.Many2one('hr.department', 'Department', related='employee_id.department_id', store=True)
    advance_request_line = fields.One2many('advance.request.line', 'advance_request_id', 'Advance Request Line', copy=True)
    percentage_line = fields.One2many('advance.request.percentage', 'advance_request_id', 'Advance Request Percentage', copy=True)
    advance_request_realization = fields.One2many('advance.request.realization', 'advance_request_id', 'Advance Request Realization')
    approval_line = fields.One2many('advance.request.approve', 'advance_request_id', 'Approval History')
    amount_total = fields.Float(string='Total', digit=dp.get_precision('Price Unit'), compute='_compute_amount', store=True)
    amount_total_realization = fields.Float(string='Total Realization', digit=dp.get_precision('Price Unit'), compute='_compute_amount', store=True)
    note = fields.Text(string='Notes')
    account_id = fields.Many2one('account.account', string='Account', default=lambda self: self.env['ir.property'].get('property_account_expense_categ_id', 'product.category'))
    journal_id = fields.Many2one('account.journal', string='Journal', default=lambda self: self.env['account.journal'].search([('type', 'in', ['cash', 'bank'])], limit=1), domain=[('type', 'in', ['cash', 'bank'])])
    move_request_id = fields.Many2one('account.move', string='Journal Entry Request', copy=False)
    move_realization_id = fields.Many2one('account.move', string='Journal Entry Realization', copy=False)
    attachment_ids = fields.Many2many('ir.attachment', 'advance_request_attachment_rel', 'advance_request_id', 'attachment_id', 'Attachments')
    ref = fields.Char('Reference Number')
    project_id = fields.Many2one('project.project', string='Project', copy=False)
    approval_name = fields.Char('Approval Name', compute='_get_approval_name', store=True)

    @api.multi
    def action_compute_percentage(self):
        for request in self :
            request.percentage_line.unlink()
            product_ids = request.mapped('advance_request_line').mapped('product_id')
            if not product_ids :
                return False
            for product_id in product_ids :
                if product_id.department_percentage_ids :
                    for percent in product_id.department_percentage_ids :
                        request.percentage_line.create({
                            'advance_request_id': request.id,
                            'department_id': percent.department_id.id,
                            'percentage': percent.percentage,
                            'account_expense_id': percent.account_expense_id.id,
                        })
                    break

    @api.model
    def create(self, vals):
        employee_id = self.env['hr.employee'].browse(vals['employee_id'])
        user_employee_id = self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
        if not user_employee_id :
            raise Warning(_("There is no employee data for user %s, please create one."%(self.env.user.name)))
        if (not employee_id.manager and employee_id.department_id.manager_id != user_employee_id) or (employee_id.manager and employee_id != user_employee_id):
            outstanding_request = self.search([
                ('employee_id','=',vals['employee_id']),
                ('state','not in',['done','cancel','reject']),
            ])
            if outstanding_request :
                if len(outstanding_request) > 1 :
                    raise Warning(_("Can not create advance request. You have more than 1 outstanding advance request."))
                else :
                    last_approve = self.env['advance.request.approve'].search([
                        ('name','=','Approved by finance'),
                        ('advance_request_id','=',outstanding_request.id),
                    ], limit=1, order='date desc')
                    if last_approve :
                        age = datetime.now() - datetime.strptime(last_approve.date, '%Y-%m-%d %H:%M:%S')
                        if age.days >= 90 :
                            raise Warning(_("Can not create advance request. You have 1 outstanding advance request approved by finance more than 90 days ago."))
        if vals.get('New', False) or 'name' not in vals :
            vals['name'] = self.env.ref('advance_request.seq_advance_request').next_by_id()
        res = super(AdvanceRequest, self).create(vals)
        return res

    @api.multi
    def unlink(self):
        for data in self:
            if data.state != 'draft' :
                raise UserError(_('Can not delete data that is not Draft !'))
        return super(AdvanceRequest, self).unlink()

    @api.multi
    def action_confirm(self):
        for request in self :
            if not request.advance_request_line :
                raise Warning(_("Please input detail."))
            request._check_percentage()
            request.write({'state':'confirm'})

    @api.multi
    def action_draft(self):
        for request in self :
            request.write({'state':'draft'})

    @api.multi
    def action_approve_manager(self):
        for request in self :
            if not request.employee_id.manager and not request.employee_id.parent_id :
                raise Warning(_("Please input manager for employee %s."%(request.employee_id.name)))
            elif request.employee_id.manager :
                if not request.employee_id.user_id :
                    raise Warning(_("Please input related user for employee %s."%(request.employee_id.name)))
                elif request.employee_id.user_id.id != self._uid :
                    raise Warning(_("You are not allow to approve this document. Only user %s can approve."%(request.employee_id.user_id.name_get()[0][1])))
            elif request.employee_id.parent_id :
                if not request.employee_id.parent_id.user_id :
                    raise Warning(_("Please input related user for employee %s."%(request.employee_id.parent_id.name_get()[0][1])))
                elif request.employee_id.parent_id.user_id.id != self._uid :
                    raise Warning(_("You are not allow to approve this document. Only user %s can approve."%(request.employee_id.parent_id.user_id.name_get()[0][1])))
            request.approval_line.create({
                'advance_request_id': request.id,
                'date': fields.Datetime.now(),
                'user_id': self._uid,
                'name': 'Approved by manager',
            })
            request.write({'state':'approve_manager'})

    @api.multi
    def _check_percentage(self):
        self.ensure_one()
        if self.percentage_line :
            total_percent = 0
            for line in self.percentage_line :
                total_percent += line.percentage
                if not line.percentage :
                    raise Warning(_("Please input percentage."))
                if self.state == 'approve_manager' and not line.account_expense_id :
                    raise Warning(_("Please input expense account for all department in Department Percentage page."))
            if total_percent != 100 :
                raise Warning(_("Total of department percentage must be 100 percent.\n\nCurrent total percentage : %s percent"%('{0:,.2f}'.format(total_percent))))

    @api.multi
    def action_approve_finance(self):
        for request in self :
            request._check_percentage()
            user_finance = self.env.ref('account.group_account_invoice')
            if self._uid not in user_finance.users.ids :
                raise Warning(_("You are not allow to approve this document. Only finance user can approve \n\n %s."%('\n'.join(user.name_get()[0][1] for user in user_finance.users))))
            if not request.percentage_line and not request.account_id :
                raise Warning(_("Please input expense account."))
            request.approval_line.create({
                'advance_request_id': request.id,
                'date': fields.Datetime.now(),
                'user_id': self._uid,
                'name': 'Approved by finance',
            })
            if self.env.user.company_id.currency_id != request.currency_id :
                currency_id = request.currency_id.id
                amount = request.currency_id.with_context(date=request.date or fields.Date.context_today(self)).compute(request.amount_total, self.env.user.company_id.currency_id)
                amount_currency = request.amount_total
            else :
                currency_id = False
                amount = request.amount_total
                amount_currency = 0
            move_line_vals = [
                (0,0,{
                    'partner_id': request.employee_id.user_id.partner_id.id,
                    'name': request.name,
                    'debit': 0,
                    'credit': amount,
                    'account_id': request.journal_id.default_credit_account_id.id,
                    'date_maturity': request.date,
                    'amount_currency': -amount_currency,
                    'currency_id': currency_id,
                })
            ]
            if not request.percentage_line :
                move_line_vals.append((0,0,{
                    'partner_id': request.employee_id.user_id.partner_id.id,
                    'name': request.name,
                    'debit': amount,
                    'credit': 0,
                    'account_id': request.account_id.id,
                    'date_maturity': request.date,
                    'amount_currency': amount_currency,
                    'currency_id': currency_id,
                }))
            else :
                for pl in request.percentage_line :
                    move_line_vals.append((0,0,{
                        'partner_id': request.employee_id.user_id.partner_id.id,
                        'name': '%s (%s)'%(request.name, pl.department_id.name),
                        'debit': amount * (pl.percentage/100),
                        'credit': 0,
                        'account_id': pl.account_expense_id.id,
                        'date_maturity': request.date,
                        'amount_currency': amount_currency * (pl.percentage/100),
                        'currency_id': currency_id,
                    }))
            move = self.env['account.move'].create({
                'journal_id': request.journal_id.id,
                'company_id': self.env.user.company_id.id,
                'date': request.date,
                'ref': request.name,
                'name': '/',
                'line_ids': move_line_vals,
            })
            move.post()
            request.advance_request_realization.unlink()
            for line in request.advance_request_line :
                request.advance_request_realization.create({
                    'advance_request_id': line.advance_request_id.id,
                    'product_id': line.product_id.id,
                    'name': line.name,
                    'qty': line.qty,
                    'price_unit': 0,
                })
            request.write({
                'move_request_id': move.id,
                'state':'approve_finance',
            })

    @api.multi
    def action_validate(self):
        for request in self :
            user_finance = self.env.ref('account.group_account_invoice')
            if self._uid not in user_finance.users.ids :
                raise Warning(_("You are not allow to approve this document. Only finance user can approve \n\n %s."%('\n'.join(user.name_get()[0][1] for user in user_finance.users))))
            to_write = {'state':'done'}
            for real in request.advance_request_realization :
                if not real.price_subtotal :
                    raise Warning(_("Please input realization amount."))
            if request.amount_total != request.amount_total_realization :
                diff_amount = request.amount_total - request.amount_total_realization
                if diff_amount > 0 :
                    if self.env.user.company_id.currency_id != request.currency_id :
                        amount = request.currency_id.with_context(date=request.date or fields.Date.context_today(self)).compute(diff_amount, self.env.user.company_id.currency_id)
                        currency_id = request.currency_id.id
                        amount_currency = diff_amount
                    else :
                        currency_id = False
                        amount_currency = 0
                        amount = diff_amount
                    debit_account_id = request.journal_id.default_debit_account_id.id
                    credit_account_id = request.account_id.id
                else :
                    if self.env.user.company_id.currency_id != request.currency_id :
                        amount = request.currency_id.with_context(date=request.date or fields.Date.context_today(self)).compute(abs(diff_amount), self.env.user.company_id.currency_id)
                        currency_id = request.currency_id.id
                        amount_currency = abs(diff_amount)
                    else :
                        currency_id = False
                        amount_currency = 0
                        amount = abs(diff_amount)
                    debit_account_id = request.account_id.id
                    credit_account_id = request.journal_id.default_credit_account_id.id

                if not request.percentage_line :
                    move_line_vals = [
                        (0,0,{
                            'partner_id': request.employee_id.user_id.partner_id.id,
                            'name': request.name,
                            'debit': amount,
                            'credit': 0,
                            'account_id': debit_account_id,
                            'date_maturity': request.date,
                            'amount_currency': amount_currency,
                            'currency_id': currency_id,
                        }),
                        (0,0,{
                            'partner_id': request.employee_id.user_id.partner_id.id,
                            'name': request.name,
                            'debit': 0,
                            'credit': amount,
                            'account_id': credit_account_id,
                            'date_maturity': request.date,
                            'amount_currency': -amount_currency,
                            'currency_id': currency_id,
                        }),
                    ]
                else :
                    move_line_vals = []
                    if diff_amount > 0 :
                        move_line_vals.append((0,0,{
                            'partner_id': request.employee_id.user_id.partner_id.id,
                            'name': request.name,
                            'debit': amount,
                            'credit': 0,
                            'account_id': debit_account_id,
                            'date_maturity': request.date,
                            'amount_currency': amount_currency,
                            'currency_id': currency_id,
                        }))
                        for pl in request.percentage_line :
                            move_line_vals.append((0,0,{
                                'partner_id': request.employee_id.user_id.partner_id.id,
                                'name': '%s (%s)'%(request.name, pl.department_id.name),
                                'debit': 0,
                                'credit': amount * (pl.percentage/100),
                                'account_id': credit_account_id,
                                'date_maturity': request.date,
                                'amount_currency': -amount_currency * (pl.percentage/100),
                                'currency_id': currency_id,
                            }))
                    else :
                        move_line_vals.append((0,0,{
                            'partner_id': request.employee_id.user_id.partner_id.id,
                            'name': request.name,
                            'debit': 0,
                            'credit': amount,
                            'account_id': credit_account_id,
                            'date_maturity': request.date,
                            'amount_currency': -amount_currency,
                            'currency_id': currency_id,
                        }))
                        for pl in request.percentage_line :
                            move_line_vals.append((0,0,{
                                'partner_id': request.employee_id.user_id.partner_id.id,
                                'name': '%s (%s)'%(request.name, pl.department_id.name),
                                'debit': amount * (pl.percentage/100),
                                'credit': 0,
                                'account_id': debit_account_id,
                                'date_maturity': request.date,
                                'amount_currency': amount_currency * (pl.percentage/100),
                                'currency_id': currency_id,
                            }))

                move = self.env['account.move'].create({
                    'journal_id': request.journal_id.id,
                    'company_id': self.env.user.company_id.id,
                    'date': request.date,
                    'ref': 'Realization %s'%(request.name),
                    'name': '/',
                    'line_ids': move_line_vals,
                })
                move.post()
                to_write['move_realization_id'] = move.id
            request.write(to_write)

    @api.multi
    def action_reject(self):
        return {
            'name': 'Reason',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'advance.request.wizard',
            'target': 'new',
            'context': {},
        }

class AdvanceRequestLine(models.Model):
    _name = "advance.request.line"
    _description = "Advance Request Line"

    @api.multi
    @api.depends('qty','price_unit')
    def _compute_amount(self):
        for line in self :
            line.price_subtotal = line.qty * line.price_unit

    advance_request_id = fields.Many2one('advance.request', 'Advance Request', ondelete='cascade', index=True)
    product_id = fields.Many2one('product.product', 'Product', index=True, domain=[('can_be_expensed','=',True)])
    name = fields.Text(string='Description', required=True)
    qty = fields.Float('Qty', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1)
    price_unit = fields.Float(string='Price Unit', digit=dp.get_precision('Price Unit'))
    price_subtotal = fields.Float(string='Subtotal', digit=dp.get_precision('Price Unit'), compute='_compute_amount', store=True)

    @api.onchange('product_id')
    def product_change(self):
        if self.product_id :
            self.name = self.product_id.name_get()[0][1]

class AdvanceRequestRealization(models.Model):
    _name = "advance.request.realization"
    _description = "Advance Request Realization"

    @api.multi
    @api.depends('qty','price_unit')
    def _compute_amount(self):
        for line in self :
            line.price_subtotal = line.qty * line.price_unit

    advance_request_id = fields.Many2one('advance.request', 'Advance Request', ondelete='cascade', index=True)
    product_id = fields.Many2one('product.product', 'Product', index=True, domain=[('can_be_expensed','=',True)])
    name = fields.Text(string='Description', required=True)
    qty = fields.Float('Qty', digits=dp.get_precision('Product Unit of Measure'), required=True, default=1)
    price_unit = fields.Float(string='Price Unit', digit=dp.get_precision('Price Unit'))
    price_subtotal = fields.Float(string='Subtotal', digit=dp.get_precision('Price Unit'), compute='_compute_amount', store=True)
    remark = fields.Text('Remarks')

    @api.onchange('product_id')
    def product_change(self):
        if self.product_id :
            self.name = self.product_id.name_get()[0][1]

class AdvanceRequestApprove(models.Model):
    _name = "advance.request.approve"
    _description = "Approval History"

    advance_request_id = fields.Many2one('advance.request', 'Advance Request', ondelete='cascade', index=True)
    date = fields.Datetime('Date', default=datetime.now())
    user_id = fields.Many2one('res.users', 'User')
    name = fields.Text(string='Description')

class AdvanceRequestPercentage(models.Model):
    _name = "advance.request.percentage"
    _description = "Advance Request Percentage"

    advance_request_id = fields.Many2one('advance.request', 'Advance Request', ondelete='cascade', index=True)
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    percentage = fields.Float(string='Percentage', required=True)
    account_expense_id = fields.Many2one('account.account', string='Expense Account', default=lambda self: self.env['ir.property'].get('property_account_expense_categ_id', 'product.category'))

    @api.onchange('department_id')
    def department_change(self):
        if self.department_id :
            self.account_expense_id = self.department_id.account_expense_id.id

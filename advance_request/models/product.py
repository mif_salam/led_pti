from odoo import fields, api, models, _

class ProductTemplate(models.Model):
    _inherit = "product.template"

    department_percentage_ids = fields.One2many('product.template.department', 'product_tmpl_id', 'Department Percentage')

    @api.onchange('can_be_expensed')
    def expense_change(self):
        self.department_ids = False

class ProductTemplateDepartment(models.Model):
    _name = "product.template.department"
    _description = "Product Template Department"

    product_tmpl_id = fields.Many2one('product.template', string='Product', ondelete='cascade')
    department_id = fields.Many2one('hr.department', string='Department', required=True)
    percentage = fields.Float(string='Percentage', required=True)
    account_expense_id = fields.Many2one('account.account', string='Expense Account', default=lambda self: self.env['ir.property'].get('property_account_expense_categ_id', 'product.category'))

    @api.onchange('department_id')
    def department_change(self):
        if self.department_id :
            self.account_expense_id = self.department_id.account_expense_id.id

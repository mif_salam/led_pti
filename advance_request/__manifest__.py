# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Advance Request',
    'version'       : '0.1',
    'summary'       : 'Expense for accomodation',
    'author'        : 'Arkana',
    'description'   : """
Features :

- 
    """,
    'depends'       : [
        'account',
        'hr',
        'hr_expense',
    ],
    'data': [
        'views/advance_request_view.xml',
        'views/hr_view.xml',
        'views/product_view.xml',
        'wizard/advance_request_wizard.xml',
        'data/ir_sequence.xml',
        'security/ir.model.access.csv',
    ],
    'demo': [],
    'installable': True
}
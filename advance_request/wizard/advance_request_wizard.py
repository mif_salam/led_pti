from odoo import fields, api, models, _
from odoo.exceptions import Warning, UserError
from odoo.addons import decimal_precision as dp

class AdvanceRequestWizard(models.TransientModel):
    _name = "advance.request.wizard"
    _description = "Reason"

    name = fields.Text(string='Reason', required=True)

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        request_id = self.env[self._context['active_model']].browse(self._context['active_id'])
        if request_id.state == 'confirm' :
            user_position = 'manager'
        else :
            user_position = 'finance'
        request_id.approval_line.create({
            'advance_request_id': request_id.id,
            'date': fields.Datetime.now(),
            'user_id': self._uid,
            'name': 'Rejected by %s. Reason : %s'%(user_position,self.name)
        })
        request_id.write({
            'state': 'reject',
        })

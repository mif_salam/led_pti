# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name'          : 'Sale Order',
    'version'       : '0.1',
    'summary'       : 'Custom Sale Module for PT Indoturbine',
    'author'        : 'Arkana',
    'description'   : """
Features :

- 
    """,
    'depends'       : [
        'sale',
        'sale_order_dates',
        'crm_dev',
        'stock_dev',
        'sale_stock_picking_blocking',
        'document_checklist',
    ],
    'data': [
        'views/sale_order_view.xml',
        'views/stock_view.xml',
        'wizard/sale_picking_wizard.xml',
        'data/ir_sequence.xml',
    ],
    'demo': [],
    'installable': True
}
from odoo import fields, api, models, _
from odoo.exceptions import Warning, UserError
from odoo.addons import decimal_precision as dp

class SalePickingWizard(models.TransientModel):
    _name = "sale.picking.wizard"
    _description = "Delivery Order"

    name = fields.Char(string='Name', default='Create receive based on selected Sale Order line.')
    sale_picking_line = fields.One2many('sale.picking.line.wizard', 'wizard_id', string='Sale Picking Line')
    carrier_id = fields.Many2one("delivery.carrier", string="Carrier")
    carrier_tracking_ref = fields.Char(string='Tracking Reference')
    carrier_price = fields.Monetary(string="Shipping Cost")
    currency_id = fields.Many2one('res.currency', 'Currency', default=lambda self: self.env.user.company_id.currency_id.id)

    @api.model
    def default_get(self, fields_list):
        result = super(SalePickingWizard, self).default_get(fields_list)

        line_vals = []
        sale_line_ids = self.env[self._context['active_model']].browse(self._context['active_ids'])
        for line in sale_line_ids :
            line_vals.append((0, 0, {
                'order_id': line.order_id.id,
                'order_line_id': line.id,
                'rel_order_line_id': line.id,
                'outstanding_qty': line.outstanding_qty,
                'qty': line.outstanding_qty,
            }))
        result['sale_picking_line'] = line_vals

        return result

    @api.model
    def _prepare_picking(self, order, orders):
        if not order.procurement_group_id:
            order.procurement_group_id = order.procurement_group_id.create({
                'name': order.name,
                'partner_id': order.partner_id.id,
            })
        if not order.partner_id.property_stock_customer.id :
            raise UserError(_("You must set a Customer Location for this partner %s") % order.partner_id.name)
        return {
            'name': self.env['ir.sequence'].next_by_code('stock.picking.delivery.order'),
            'picking_type_id': order.warehouse_id.out_type_id.id,
            'partner_id': order.partner_id.id,
            'date': order.date_order,
            'origin': ', '.join([order.name for order in orders]),
            'location_dest_id': order._get_destination_location(),
            'location_id': order.warehouse_id.out_type_id.default_location_src_id.id,
            'company_id': order.company_id.id,
            'carrier_id': self.carrier_id and self.carrier_id.id or False,
            'carrier_tracking_ref': self.carrier_tracking_ref,
            'carrier_price': self.carrier_price,
            'currency_id': self.currency_id.id,
        }

    @api.multi
    def _create_picking(self):
        sale_line_ids = self.env[self._context['active_model']].browse(self._context['active_ids'])
        orders = sale_line_ids.mapped('order_id')
        order = orders[0]
        partner_id = sale_line_ids.mapped('order_id').mapped('partner_id')
        if len(partner_id) > 1 :
            raise Warning(_("Can not create DO from multiple customer."))
        warehouse_id = sale_line_ids.mapped('order_id').mapped('warehouse_id')
        if len(warehouse_id) > 1 :
            raise Warning(_("Can not create DO for multiple warehouse."))
        picking_type_id = warehouse_id.out_type_id
        StockPicking = self.env['stock.picking']
        picking = StockPicking.create(self._prepare_picking(order,orders))
        moves = self.sale_picking_line._create_stock_moves(picking)
        moves = moves.filtered(lambda x: x.state not in ('done', 'cancel'))._action_confirm()
        seq = 0
        for move in sorted(moves, key=lambda move: move.date_expected):
            seq += 5
            move.sequence = seq
        moves._action_assign()
        picking.message_post_with_view('mail.message_origin_link',
            values={'self': picking, 'origin': order},
            subtype_id=self.env.ref('mail.mt_note').id)
        for order in orders :
            if order.state == 'sale' :
                order.write({'state':'delivery'})
        return picking
    
    @api.multi
    def action_create(self):
        picking_id = self._create_picking()
        action = self.env.ref('stock.action_picking_tree_all')
        action = action.read()[0]
        view_id = self.env.ref('stock.view_picking_form')
        action.update({
            'res_id': picking_id.id,
            'views': [(view_id.id, 'form')],
            'view_id': view_id.id,
        })
        return action

class SalePickingLineWizard(models.TransientModel):
    _name = "sale.picking.line.wizard"
    _description = "Delivery Order Line"
    _rec_name = 'order_line_id'

    wizard_id = fields.Many2one('sale.picking.wizard', string='Wizard', ondelete='cascade')
    order_id = fields.Many2one('sale.order', string='Sale Order', ondelete='cascade')
    order_line_id = fields.Many2one('sale.order.line', string='Sale Order Line', ondelete='cascade')
    rel_order_line_id = fields.Many2one('sale.order.line', string='Sale Order Line', related='order_line_id')
    outstanding_qty = fields.Float(string="Outstanding Qty", digits=dp.get_precision('Product Unit of Measure'))
    qty = fields.Float(string="Qty", digits=dp.get_precision('Product Unit of Measure'), required=True)

    @api.multi
    def _create_stock_moves(self, picking):
        moves = self.env['stock.move']
        done = self.env['stock.move'].browse()
        with self.env.norecompute():
            for line in self:
                for val in line._prepare_stock_moves(picking):
                    done += moves.create(val)
        self.recompute()
        return done

    @api.multi
    def _prepare_stock_moves(self, picking):
        """ Prepare the stock moves data for one order line. This function returns a list of
        dictionary ready to be used in stock.move's create()
        """
        self.ensure_one()
        res = []
        if self.order_line_id.product_id.type not in ['product', 'consu']:
            return res
        qty = self.qty
        template = {
            'name': self.order_line_id.name or '',
            'product_id': self.order_line_id.product_id.id,
            'product_uom': self.order_line_id.product_uom.id,
            'date': self.order_line_id.order_id.date_order,
            'date_expected': self.order_line_id.commitment_date,
            'location_id': self.order_line_id.order_id.warehouse_id.out_type_id.default_location_src_id.id,
            'location_dest_id': self.order_line_id.order_id._get_destination_location(),
            'picking_id': picking.id,
            'partner_id': self.order_line_id.order_id.partner_id.id,
            # 'move_dest_ids': [(4, x) for x in self.order_line_id.move_dest_ids.ids],
            'state': 'draft',
            'sale_line_id': self.order_line_id.id,
            'company_id': self.order_line_id.order_id.company_id.id,
            'price_unit': 0, #??? price unit dihitung saat booking
            'picking_type_id': self.order_line_id.order_id.warehouse_id.out_type_id.id,
            'group_id': self.order_line_id.order_id.procurement_group_id.id,
            'origin': self.order_line_id.order_id.name,
            'route_ids': self.order_line_id.order_id.warehouse_id and [(6, 0, [x.id for x in self.order_line_id.order_id.warehouse_id.route_ids])] or [],
            'warehouse_id': self.order_line_id.order_id.warehouse_id.id,
            'product_uom_qty': qty,
        }
        res.append(template)
        return res

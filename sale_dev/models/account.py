from odoo import fields, api, models, _
from odoo.exceptions import Warning

class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    @api.multi
    def action_invoice_open(self):
        res = super(AccountInvoice, self).action_invoice_open()
        for invoice in self :
            order_ids = invoice.invoice_line_ids.mapped('sale_line_ids').mapped('order_id')
            order_ids.trigger_state()
        return res

    @api.multi
    def action_invoice_paid(self):
        res = super(AccountInvoice, self).action_invoice_paid()
        for invoice in self :
            order_ids = invoice.invoice_line_ids.mapped('sale_line_ids').mapped('order_id')
            order_ids.trigger_state()
        return res
        
from odoo import fields, api, models, _
from odoo.exceptions import Warning
from odoo.addons import decimal_precision as dp
from odoo.osv import expression
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def _get_destination_location(self):
        self.ensure_one()
        if self.partner_shipping_id :
            return self.partner_shipping_id.property_stock_customer.id
        elif self.partner_id.property_stock_customer :
            return self.partner_id.property_stock_customer.id
        return self.picking_type_id.default_location_dest_id.id

    @api.depends('order_line.move_ids','order_line.qty_delivered')
    def _compute_picking_ids2(self):
        for order in self:
            order.picking_ids = order.order_line.mapped('move_ids').mapped('picking_id')
            order.delivery_count = len(order.picking_ids)

    @api.depends('state', 'order_line.invoice_status')
    def _get_invoiced(self):
        """
        Compute the invoice status of a SO. Possible statuses:
        - no: if the SO is not in status 'sale' or 'done', we consider that there is nothing to
          invoice. This is also hte default value if the conditions of no other status is met.
        - to invoice: if any SO line is 'to invoice', the whole SO is 'to invoice'
        - invoiced: if all SO lines are invoiced, the SO is invoiced.
        - upselling: if all SO lines are invoiced or upselling, the status is upselling.

        The invoice_ids are obtained thanks to the invoice lines of the SO lines, and we also search
        for possible refunds created directly from existing invoices. This is necessary since such a
        refund is not directly linked to the SO.
        """
        # Ignore the status of the deposit product
        deposit_product_id = self.env['sale.advance.payment.inv']._default_product_id()
        line_invoice_status_all = [(d['order_id'][0], d['invoice_status']) for d in self.env['sale.order.line'].read_group([('order_id', 'in', self.ids), ('product_id', '!=', deposit_product_id.id)], ['order_id', 'invoice_status'], ['order_id', 'invoice_status'], lazy=False)]
        for order in self:
            invoice_ids = order.order_line.mapped('invoice_lines').mapped('invoice_id').filtered(lambda r: r.type in ['out_invoice', 'out_refund'])
            # Search for invoices which have been 'cancelled' (filter_refund = 'modify' in
            # 'account.invoice.refund')
            # use like as origin may contains multiple references (e.g. 'SO01, SO02')
            refunds = invoice_ids.search([('origin', 'like', order.name), ('company_id', '=', order.company_id.id), ('type', 'in', ('out_invoice', 'out_refund'))])
            invoice_ids |= refunds.filtered(lambda r: order.name in [origin.strip() for origin in r.origin.split(',')])

            # Search for refunds as well
            domain_inv = expression.OR([
                ['&', ('origin', '=', inv.number), ('journal_id', '=', inv.journal_id.id)]
                for inv in invoice_ids if inv.number
            ])
            if domain_inv:
                refund_ids = self.env['account.invoice'].search(expression.AND([
                    ['&', ('type', '=', 'out_refund'), ('origin', '!=', False)], 
                    domain_inv
                ]))
            else:
                refund_ids = self.env['account.invoice'].browse()

            line_invoice_status = [d[1] for d in line_invoice_status_all if d[0] == order.id]

            if order.state not in ('sale', 'delivery', 'waiting_invoice', 'waiting_payment', 'done'):
                invoice_status = 'no'
            elif any(invoice_status == 'to invoice' for invoice_status in line_invoice_status):
                invoice_status = 'to invoice'
            elif all(invoice_status == 'invoiced' for invoice_status in line_invoice_status):
                invoice_status = 'invoiced'
            elif all(invoice_status in ['invoiced', 'upselling'] for invoice_status in line_invoice_status):
                invoice_status = 'upselling'
            else:
                invoice_status = 'no'

            order.update({
                'invoice_count': len(set(invoice_ids.ids + refund_ids.ids)),
                'invoice_ids': invoice_ids.ids + refund_ids.ids,
                'invoice_status': invoice_status
            })

    picking_ids = fields.Many2many('stock.picking', compute='_compute_picking_ids2', string='Pickings', store=True)
    delivery_count = fields.Integer(string='Delivery Orders', compute='_compute_picking_ids2')
    invoice_count = fields.Integer(string='# of Invoices', compute='_get_invoiced', readonly=True)
    invoice_ids = fields.Many2many("account.invoice", string='Invoices', compute="_get_invoiced", readonly=True, copy=False)
    invoice_status = fields.Selection([
        ('upselling', 'Upselling Opportunity'),
        ('invoiced', 'Fully Invoiced'),
        ('to invoice', 'To Invoice'),
        ('no', 'Nothing to Invoice')
        ], string='Invoice Status', compute='_get_invoiced', store=True, readonly=True)
    doc_ids = fields.One2many('document.checklist', 'sale_order_id', string='Document Checklist')

    @api.model
    def _set_auto_done_false(self):
        self.env['ir.default'].set('res.config.settings', 'auto_done_setting', 0)

    @api.multi
    def trigger_state(self):
        for sale in self :
            if sale.state == 'delivery' :
                if sale.order_line and all(line.qty_delivered >= line.product_uom_qty for line in sale.order_line):
                    sale.write({'state':'waiting_invoice'})
            elif sale.state == 'waiting_invoice' :
                if sale.order_line and all(line.qty_invoiced >= line.product_uom_qty for line in sale.order_line):
                    sale.write({'state':'waiting_payment'})
            elif sale.state == 'waiting_payment' :
                if sale.invoice_ids and all(inv.state in ['paid','cancel'] for inv in sale.invoice_ids):
                    sale.write({'state':'done'})

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    @api.depends('move_ids.state','qty_delivered')
    @api.multi
    def _get_picking_qty(self):
        for line in self :
            move_ids = line.move_ids.filtered(lambda move: move.state not in ('draft','cancel','done'))
            line.picking_qty = sum(move.product_uom_qty for move in move_ids)
            line.outstanding_qty = line.product_uom_qty - line.qty_delivered - line.picking_qty

    @api.depends('state', 'product_uom_qty', 'qty_delivered', 'qty_to_invoice', 'qty_invoiced')
    def _compute_invoice_status(self):
        """
        Compute the invoice status of a SO line. Possible statuses:
        - no: if the SO is not in status 'sale' or 'done', we consider that there is nothing to
          invoice. This is also hte default value if the conditions of no other status is met.
        - to invoice: we refer to the quantity to invoice of the line. Refer to method
          `_get_to_invoice_qty()` for more information on how this quantity is calculated.
        - upselling: this is possible only for a product invoiced on ordered quantities for which
          we delivered more than expected. The could arise if, for example, a project took more
          time than expected but we decided not to invoice the extra cost to the client. This
          occurs onyl in state 'sale', so that when a SO is set to done, the upselling opportunity
          is removed from the list.
        - invoiced: the quantity invoiced is larger or equal to the quantity ordered.
        """
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for line in self:
            if line.state not in ('sale', 'delivery', 'waiting_invoice', 'waiting_payment', 'done'):
                line.invoice_status = 'no'
            elif not float_is_zero(line.qty_to_invoice, precision_digits=precision):
                line.invoice_status = 'to invoice'
            elif line.state == 'sale' and line.product_id.invoice_policy == 'order' and\
                    float_compare(line.qty_delivered, line.product_uom_qty, precision_digits=precision) == 1:
                line.invoice_status = 'upselling'
            elif float_compare(line.qty_invoiced, line.product_uom_qty, precision_digits=precision) >= 0:
                line.invoice_status = 'invoiced'
            else:
                line.invoice_status = 'no'

    @api.depends('qty_invoiced', 'qty_delivered', 'product_uom_qty', 'order_id.state')
    def _get_to_invoice_qty(self):
        """
        Compute the quantity to invoice. If the invoice policy is order, the quantity to invoice is
        calculated from the ordered quantity. Otherwise, the quantity delivered is used.
        """
        for line in self:
            if line.order_id.state in ['sale', 'delivery', 'waiting_invoice', 'waiting_payment', 'done']:
                if line.product_id.invoice_policy == 'order':
                    line.qty_to_invoice = line.product_uom_qty - line.qty_invoiced
                else:
                    line.qty_to_invoice = line.qty_delivered - line.qty_invoiced
            else:
                line.qty_to_invoice = 0

    picking_qty = fields.Float(compute='_get_picking_qty', string="Picking Qty", digits=dp.get_precision('Product Unit of Measure'), store=True)
    outstanding_qty = fields.Float(compute='_get_picking_qty', string="Outstanding Qty", digits=dp.get_precision('Product Unit of Measure'), store=True)
    commitment_date = fields.Datetime(string='Commitment Date', related='order_id.commitment_date', store=True)
    invoice_status = fields.Selection([
        ('upselling', 'Upselling Opportunity'),
        ('invoiced', 'Fully Invoiced'),
        ('to invoice', 'To Invoice'),
        ('no', 'Nothing to Invoice')
        ], string='Invoice Status', compute='_compute_invoice_status', store=True, readonly=True, default='no')
    qty_to_invoice = fields.Float(
        compute='_get_to_invoice_qty', string='To Invoice', store=True, readonly=True,
        digits=dp.get_precision('Product Unit of Measure'))
    partner_id = fields.Many2one('res.partner', string='Customer', related='order_id.partner_id', store=True)

    @api.multi
    def _action_launch_procurement_rule(self):
        if 'force_create' in self._context :
            return super(SaleOrderLine, self)._action_launch_procurement_rule()
        else :
            return False

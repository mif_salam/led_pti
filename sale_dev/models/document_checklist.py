from odoo import fields, api, models, _
from odoo.exceptions import Warning

class DocumentChecklist(models.Model):
    _inherit = 'document.checklist'

    sale_order_id = fields.Many2one('sale.order')

    @api.model
    def create(self, vals):
        if 'sale_order_id' in vals :
            sale_order_id = self.env['sale.order'].browse(vals['sale_order_id'])
            vals['name'] = sale_order_id.name
        res = super(DocumentChecklist, self).create(vals)
        return res

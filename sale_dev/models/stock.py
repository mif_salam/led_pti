from odoo import fields, api, models, _
from odoo.exceptions import Warning, UserError

class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        for picking in self :
            order_ids = picking.move_lines.mapped('sale_line_id').mapped('order_id')
            order_ids.trigger_state()
        return res
